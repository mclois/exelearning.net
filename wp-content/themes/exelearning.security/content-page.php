<?php global $my_i18n; ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<div class="entry-content">
		<?php 
			$content = get_the_content(); 
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);		
			$pieces = explode("<p>|</p>", $content);
			$size = sizeof($pieces);
			if ($size>1) {
				echo "<div id='tabs'></div>";
				echo "<div id='tabs-desc'>";
					for ($i = 0; $i < $size; $i++) {
						echo "<div id='tab".($i+1)."' class='tab'>";
							echo $pieces[$i];
							//echo $i;
						echo "</div>";
					}
				echo "</div><!-- #tabs-desc -->";			
			} else {
				if (is_page(221) || is_page(223) || is_page(601)) {
					echo "<p>".$my_i18n['chooseYourOS'].":</p>";
					$page_id = 221;
					$page_data = get_page( $page_id );
					$content = str_replace('Versión instalable', $my_i18n['installVersion'], $page_data->post_content);		
					$content = str_replace('Versión portable', $my_i18n['portableVersion'], $content);	
					echo $content;
					//echo apply_filters('the_content', $page_data->post_content);
					echo "<p class='previous-versions'><a href='https://forja.cenatic.es/frs/?group_id=197' rel='external'>".$my_i18n['previousVersions']."</a></p>";
				} else {
					echo $content;
				}
			}
		?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div>
	<?php //edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
</div>