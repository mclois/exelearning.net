<?php global $lang_link, $lang, $my_i18n; ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php 
			$t = get_the_title(); 
			$my_t = str_replace("(plantilla para eXeLearning)", "<span class='scr-av'>(plantilla para eXeLearning)</span>", $t);
		?>
		<?php if ( 'post' == get_post_type() ) : ?>
			<div class="with-data">
				<?php echo '<h1 class="entry-title">'.$my_t.'</h1>'; ?>
				<div class="entry-meta autoclear" id="add-this">
					<?php twentyeleven_posted_on(); ?>
				</div><!-- .entry-meta -->
			</div>
		<?php else : ?>
			<?php echo '<h1 class="entry-title">'.$my_t.'</h1>'; ?>
			<div id="add-this"></div>
		<?php endif; ?>
		<?php
			$posttags = get_the_tags();
			if ($posttags) {
				echo "<div class='post-tags'>";
					echo $my_i18n["tags"].": ";
					foreach($posttags as $tag) {
						$tag_link = get_tag_link($tag->term_id).$lang_link;
						echo "<a href='".$tag_link."'>".$tag->name."</a> ";
					}
				echo "</div>";
			}
		?>		
	</div><!-- .entry-header -->
	<div class="entry-content">
		<!--<p><a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false, '' ); echo $src[0]; ?>	" rel="lightbox"><?php the_post_thumbnail("medium"); ?></a></p>-->
		<?php 
			$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false, '' ); 
			if ($src[0]!='') {
		?>
			<p class="entry-main-img"><?php the_post_thumbnail(); ?></p>
		<?php } ?>
		<?php the_content(); ?>
		<?php
			$attachments = "";
			$zip = get_post_custom_values("zip");
			if (sizeof($zip)>0 ) {
				foreach ( $zip as $key => $value ) $attachments.="<li><a href='".$value."' class='zip'>".$my_i18n['download']."</a></li>"; 
			} else {
				$args = array(
					'post_type' => 'attachment',
					'numberposts' => null,
					'post_status' => null,
					'post_parent' => $post->ID
				); 
				$att = get_posts($args);
					if ($att) {
					foreach ($att as $attachment) {
						$ext = pathinfo($attachment->guid, PATHINFO_EXTENSION);
						if ($ext=="zip") $attachments.="<li><a href='".$attachment->guid."' class='zip'>".$my_i18n['download']."</a></li>"; 
					}
				}						
			}
			$theme_author = get_post_custom_values("autor");
			if (sizeof($theme_author)>0 ) {
				foreach ( $theme_author as $key => $value ) {
					$author_name=$value;
				}
			}
			$authorURL = get_post_custom_values("web_autor");
			if (sizeof($authorURL)>0 ) {
				foreach ( $authorURL as $key => $value ) {
					$author_url=$value;
				}
			}
			if ($author_name!='') {
				$attachments.="<li>".$my_i18n["author"].": ";
				if ($author_url!='') $attachments.="<a href='".$author_url."'>";
				$attachments.=$author_name;
				if ($author_url!='') $attachments.="</a>";
				$attachments.="</li>";
			}
			if($attachments!="") echo "<ul id='attachments'>".$attachments."</ul>";
		?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	
</div>