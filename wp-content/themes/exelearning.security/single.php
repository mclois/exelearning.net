<?php get_header(); ?>
<?php global $blogURL, $my_i18n, $isDocumentation, $isDocumentacion, $isDocumentacionEU, $isDescargas, $isDescargasEU, $isDownloads; ?>
		<div id="primary">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
					global $lang;
					
					$prevPost = get_previous_post(true);
					$prevURL = get_permalink($prevPost->ID);
					$prevTitle = $prevPost->post_title;
					
					$nextPost = get_next_post(true);
					$nextURL = get_permalink($nextPost->ID);
					$nextTitle = $nextPost->post_title;					
					
					$showInternalNav = true;
					if (($prevPost=='') && ($nextPost=='')) $showInternalNav = false;
					if ($showInternalNav) echo "<div id='nav-single' class='serial-nav autoclear'>";
					previous_post_link('<span class="nav-previous"><a href="'.$prevURL.'" title="'.$prevTitle.'"><strong>← '.$my_i18n["previous"].'</strong></a></span>','Anterior',true);
					next_post_link('<span class="nav-next"><a href="'.$nextURL.'" title="'.$nextTitle.'"><strong>'.$my_i18n["next"].' →</strong></a></span>','Siguiente',true) ;
					if ($showInternalNav) echo "</div>";					
				?>				
				<?php get_template_part( 'content', 'single' ); ?>
				<?php //comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>			
		</div><!-- #primary -->
		<?php if ($isDocumentation || $isDocumentacion || $isDocumentacionEU || $isDescargas || $isDescargasEU || $isDownloads) { ?>
			
			<?php
				$default = 9;
				if ($isDocumentation) $default = 10;
				else if ($isDocumentacionEU) $default = 43;
				else if ($isDescargas) $default = 16;			
				else if ($isDescargasEU) $default = 42;	
				else if ($isDownloads) $default = 17;			
				$category = get_the_category();
				$cur_cat_id =  $category[0]->term_id;			
				$side_title=$my_i18n["index"];
				$cN = "";
				if ($isDescargas || $isDescargasEU || $isDownloads) {
					$side_title=$my_i18n["downloads"];
					$cN = ' class="other-downloads"';
				}
				echo '<div id="secondary"'.$cN.'><div class="sidebar-block"><h2 class="widget-title">'.$side_title.'</h2>';
					echo "<ul>";
					wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&current_category='.$cur_cat_id);					
				echo "</ul></div></div>";			
			?>
					
		<?php } ?>		

<?php get_footer(); ?>