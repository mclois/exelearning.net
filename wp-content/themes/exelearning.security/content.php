<?php global $lang_link, $my_i18n; ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php 
		$t = get_the_title(); 			
		$my_t = str_replace("(plantilla para eXeLearning)", "<span class='scr-av'>(plantilla para eXeLearning)</span>", $t);
		$tag = "h2";
		if (is_single()) $tag = "h1";
	?>
	<?php if ( 'post' == get_post_type() ) : ?>
		<div class="with-data">
			<<?php echo $tag; ?> class="entry-title"><a href="<?php the_permalink(); echo $lang_link; ?>"><?php echo $my_t; ?></a></<?php echo $tag; ?>>			
			<div class="entry-meta">
				<?php twentyeleven_posted_on(); ?>
			</div><!-- .entry-meta -->
		</div>
	<?php else : ?>
		<<?php echo $tag; ?> class="entry-title"><a href="<?php the_permalink(); echo $lang_link; ?>"><?php echo $my_t; ?></a></<?php echo $tag; ?>>			
	<?php endif; ?>
	<?php if ( is_search() ) : ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	<?php else : ?>
		<div class="entry-summary autoclear">		
			<?php the_post_thumbnail("thumbnail"); ?>
			<?php echo get_the_excerpt(); ?>
			<?php
				$posttags = get_the_tags();
				if ($posttags) {
					echo "<p class='post-tags'>".$my_i18n["tags"].": ";
					foreach($posttags as $tag) {
						$tag_link = get_tag_link($tag->term_id).$lang_link;
						echo "<a href='".$tag_link."'>".$tag->name."</a> ";
					}
					echo "</p>";
				}
			?>			
			<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>			
		</div><!-- .entry-content -->
	<?php endif; ?>
</div>