﻿<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
global $blogURL, $blogDesc, $blogTitle, $lang, $parent, $searchInSiteTitle, $searchInForumsTitle;
$blogURL = get_bloginfo("wpurl");
$blogDesc = "La evolución de eXeLearning";
$blogTitle = "El nuevo eXeLearning";
$searchInSiteTitle = "Buscar en la web";
$searchInForumsTitle = "Buscar en los foros";
//$current_page = echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; 
$lang = 'es';
if ($_GET['lang'] && $_GET['lang']==='en') {
	$lang = 'en';
} 
else if (is_page()) {
	$mykey_value = get_post_custom_values('lang');
	if ($mykey_value[0]=="en") $lang = 'en';
	if ( $post->post_parent ) {
		$ancestors=get_post_ancestors($post->ID);
		$root=count($ancestors)-1;
		$parent = $ancestors[$root];		
	}
}
if ($lang=='en') {
	$blogTitle = "The new eXeLearning";
	$blogDesc = "The evolution of eXeLearning";
	$searchInSiteTitle = "Website search";
	$searchInForumsTitle = "Forum search";
}
//language_attributes();
?><!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div id="branding" role="banner" class="autoclear">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo $blogTitle; ?></a>
					</span>
				</<?php echo $heading_tag; ?>>
				<div id="site-description"><?php echo $blogDesc; ?></div>
				<div id="lang-switcher">
				<?php 
					//Language switcher
					if ($lang=='es') {
						echo "<strong>Español</strong> | <a href='".$blogURL."/?lang=en'>English</a>";
					} else {
						echo "<a href='".$blogURL."'>Español</a> | <strong>English</strong>";
					}
				?>
				</div>
				<?php
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() && current_theme_supports( 'post-thumbnails' ) &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID );
					elseif ( get_header_image() ) : ?>
						<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
					<?php endif; ?>
			</div><!-- #branding -->

			<div id="access" role="navigation">
			  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
				<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
				<?php //wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
				<div class="menu">
				  <ul>
					<?php if ($lang=="es") { ?>
						<li<?php if (is_home()) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/">Inicio</a></li>
						<li<?php if (is_page(11)) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/descarga/">Descarga</a></li>
						<li<?php if (is_page(38) || $parent==38) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/documentacion/">Documentación</a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/">Foros</a></li>
						<li<?php if (is_page(50)) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/desarrolladores/">Desarrolladores</a></li>
					<?php } else { ?>
						<li<?php if (is_home()) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/?lang=en">Home</a></li>
						<li<?php if (is_page(16)) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/download/">Download</a></li>
						<li<?php if (is_page(42) || $parent==42) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/documentation/">Documentation</a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/">Forums</a></li>
						<li<?php if (is_page(52)) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/developers/">Developers</a></li>
					<?php } ?>
				  </ul>
				</div>				
			</div><!-- #access -->
		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main">
		<?php if(function_exists('bcn_display') && !is_home() && $post->post_type!="forum" && $post->post_type!="topic"){
			echo "<p style='margin-left:15px'><a href='".$blogURL;
			if ($lang=='en') echo "/?lang=en";
			echo "'>".$blogTitle."</a> &raquo; ";
			bcn_display();
			echo "</p>";
		}
		?>