<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<div id="extras">
<?php 
	global $blogURL;
	if ( is_user_logged_in() ) { 
		$user = wp_get_current_user(); 
		$logoutReferer=$_SERVER['REQUEST_URI'];
		echo 'Usuario: <a href="'.$blogURL.'/login/?action=profile" id="vs-user">'.$user->display_name.'</a>';
		//if (current_user_can('manage_options')) echo ' | <a href="/wp-admin/">Administrar</a> (<a href="/wp-admin/post.php?action=edit&amp;post='.currentPost().'">'.currentPost().'</a>)';
		echo ' | <a href="'.wp_logout_url($logoutReferer).'">Cerrar sesi&oacute;n</a>';
	} else echo '<a href="'.$blogURL.'/?pagename=login&amp;action=register" class="new-account">Crea tu cuenta</a> | <a href="'.wp_login_url($_SERVER['REQUEST_URI']).'">Iniciar sesi&oacute;n</a>';
?>
<?php
	//echo "<ul class='my-login'>"; 
	//dynamic_sidebar( 'primary-widget-area' );
	//echo "</ul>";
	global $searchInSiteTitle, $searchInForumsTitle;
	echo '<h2 class="widgettitle">'.$searchInSiteTitle.'</h2>';	
	echo get_search_form();
	//bbpress_search();
	the_widget("bbPress_Forum_Plugin_Search","title=".$searchInForumsTitle);
	if (is_page(38) || $parent==38 || is_page(42) || $parent==42) { //Documentation pages.
		$childPages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );
		$childPagesMenu = '';
		foreach( $childPages as $page ) {
			$childPagesMenu.='<li><a href="'.get_page_link( $page->ID ).'">'.$page->post_title.'</a></li>';
		}
		if ($childPagesMenu!='') {
			$childPagesTitle = "En este apartado";
			if ($lang=="en") $childPagesTitle = "In this section";
			echo "<h2>".$childPagesTitle."</h2><ul>".$childPagesMenu."</ul>";
		}
	} 
?>
</div>