﻿<?php global $blogURL; ?>
<div id="secondary">
	<div id="rss-feeds" class="sidebar-block">
		<h2 class='widget-title'>RSS</h2>
		<?php
			global $lang;
			if ($lang=="es") { 
		?>
		<ul>
			<li><a href="<?php echo $blogURL; ?>/feed">El nuevo eXeLearning</a>
				<ul>
					<li><a href="<?php echo $blogURL; ?>/category/descargas/feed">Descargas</a>
						<ul>
							<li><a href="<?php echo $blogURL; ?>/category/descargas/descargar-idevices/feed">iDevices</a></li>
							<li><a href="<?php echo $blogURL; ?>/category/descargas/descargar-plantillas/feed">Plantillas</a></li>
						</ul>
					</li>
					<li><a href="<?php echo $blogURL; ?>/category/documentacion/feed">Documentación</a></li>
				</ul>
			</li>			
			<li><a href="<?php echo $blogURL; ?>/forums/feed">Foros</a>
				<ul>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/feed">Ayuda</a></li>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/feed">Discusión abierta</a></li>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/feed">Desarrolladores</a></li>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/feed">Grupos de trabajo</a></li>
				</ul>
			</li>
		</ul>
		<?php } else { ?>
		<ul>
			<li><a href="<?php echo $blogURL; ?>/feed">The new eXeLearning</a>
				<ul>
					<li><a href="<?php echo $blogURL; ?>/category/downloads/feed">Descargas</a>
						<ul>
							<li><a href="<?php echo $blogURL; ?>/category/descargas/download-idevices/feed">iDevices</a></li>
							<li><a href="<?php echo $blogURL; ?>/category/descargas/download-themes/feed">Themes</a></li>
						</ul>
					</li>
					<li><a href="<?php echo $blogURL; ?>/category/documentation/feed">Documentation</a></li>
				</ul>
			</li>		
			<li><a href="<?php echo $blogURL; ?>/forums/feed">Forums</a>
				<ul>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/feed">Help</a></li>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/feed">Open discussion</a></li>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/feed">Developers</a></li>
					<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/feed">Work groups</a></li>
				</ul>
			</li>
		</ul>		
		<?php } ?>
	</div>
</div>