<?php get_header(); ?>
<?php global $blogURL, $isDocumentation, $isDocumentacion, $isDescargas, $isDownloads; ?>
		<div id="primary">
			<div id="content">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php
						global $lang;
						$next_text = "Siguiente";
						$prev_text = "Anterior";
						if ($lang=="en") {
							$next_text = "Next";
							$prev_text = "Previous";
						}
						
						$prevPost = get_previous_post(true);
						$prevURL = get_permalink($prevPost->ID);
						$prevTitle = $prevPost->post_title;
						
						$nextPost = get_next_post(true);
						$nextURL = get_permalink($nextPost->ID);
						$nextTitle = $nextPost->post_title;					
						
						$showInternalNav = true;
						if (($prevPost=='') && ($nextPost=='')) $showInternalNav = false;
						if ($showInternalNav) echo "<div id='nav-single'>";
						previous_post_link('<span class="nav-previous"><a href="'.$prevURL.'" title="'.$prevTitle.'"><strong>← '.$prev_text.'</strong></a></span>','Anterior',true);
						next_post_link('<span class="nav-next"><a href="'.$nextURL.'" title="'.$nextTitle.'"><strong>'.$next_text.' →</strong></a></span>','Siguiente',true) ;
						if ($showInternalNav) echo "</div>";					
					?>				
					<?php get_template_part( 'content', 'single' ); ?>
					<?php comments_template( '', true ); ?>
				<?php endwhile; // end of the loop. ?>
			</div><!-- #content -->
		</div><!-- #primary -->
		<?php if ($isDocumentation || $isDocumentacion || $isDescargas || $isDownloads) { ?>
			<div id="secondary">
			<?php
				$default = 9;
				if ($isDocumentation) $default = 10;
				if ($isDescargas) $default = 16;			
				if ($isDownloads) $default = 17;			
				$category = get_the_category();
				$cur_cat_id =  $category[0]->term_id;			
				$side_title=$my_i18n["index"];
				if ($isDescargas || $isDownloads) $side_title=$my_i18n["downloads"];
				echo '<div class="sidebar-block"><h2 class="widget-title">'.$side_title.'</h2>';
				echo "<ul>";
				wp_list_categories('child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&current_category='.$cur_cat_id);
				echo "</ul></div>";			
			?>
			</div>		
		<?php } ?>		

<?php get_footer(); ?>