<?php global $blogURL, $themePath, $isForum; ?>

	</div><!-- #main -->

	<div id="footer">
		<ul>
			<li><a href="http://www.ite.educacion.es/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/intef.png" alt="INTEF" width="125" height="40" /></a></li>
			<li><a href="http://cedec.ite.educacion.es/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/cedec.png" alt="CEDEC" width="109" height="40" /></a></li>
			<li><a href="http://todofp.es/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/todoFP.png" alt="TodoFP.es" width="95" height="40" /></a></li>
			<li><a href="http://www.ulhi.hezkuntza.net/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/ulhi.png" alt="ULHI" width="164" height="40" /></a></li>
			<li><a href="http://www.ulhi.hezkuntza.net/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/tknika.png" alt="TKNIKA" width="121" height="40" /></a></li>
			<li class="epa"><a href="http://www.juntadeandalucia.es/educacion/permanente/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/epa.png" alt="Educación Permanente (Andalucía)" width="173" height="50" /></a></li>
			<li class="op"><a href="http://www.open-phoenix.com/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/open_phoenix.png" alt="Open Phoenix" width="153" height="30" /></a></li>
		</ul>
	</div><!-- #footer -->
</div><!-- #page -->
<?php if ($isForum && is_user_logged_in()) { ?>
<script type="text/javascript" src="<?php echo $themePath; ?>/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "fullscreen",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,bullist,numlist,|,outdent,indent,blockquote,|,sub,sup,|,undo,redo,|,link,unlink,code,|,removeformat,|,fullscreen",
        theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo $themePath; ?>/css/tiny_mce.css"        
});
</script>
<?php } ?><?php wp_footer(); ?><script type="text/javascript">domIsLoaded('<?php echo $blogURL; ?>')</script></body>
</html>