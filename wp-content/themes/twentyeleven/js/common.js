var _bodyClassName;
function domIsLoaded(blogURL) {	
	_bodyClassName = document.body.className;
	
	var twitter = document.getElementById("twitter-link");
	if (twitter) {
		var h = twitter.innerHTML;
		var n = '<a href="https://twitter.com/exelearning_sp" class="twitter-follow-button" data-show-count="false" data-lang="es" data-show-screen-name="false">Seguir a @exelearning_sp</a>';
		if (_bodyClassName.indexOf(" lang-en")!=-1) n = '<a href="https://twitter.com/exelearning_sp" class="twitter-follow-button" data-show-count="false" data-lang="en" data-show-screen-name="false">Follow @exelearning_sp</a>';
		twitter.innerHTML=n+h;
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	}
	
	var s = document.getElementById("s");
	if (s) {
		s.placeholder = s.title;
		s.title="";
	}
	
	if (_bodyClassName.indexOf(" exe-forum")!=-1) {
		var c = document.getElementById("content");
		var as = c.getElementsByTagName("A");
		var i = as.length;
		while (i--) {
			if (as[i].href.indexOf(blogURL)!=-1 && as[i].href.indexOf('lang=en')==-1) {
				if (as[i].href.indexOf("?")==-1) as[i].href+="?lang=en";
				else as[i].href+="&lang=en";
			}
		}
	}
}