<?php
global $blogURL, $themePath, $lang, $lang_link, $isDocumentacion, $isDocumentation, $isDescargas, $isDownloads, $my_i18n, $isForum;
$blogURL = get_bloginfo("wpurl");
$themePath = get_bloginfo("template_url");
$lang_link="";

$my_i18n = array(
	"blogDesc" => "La evolución de eXeLearning",
	"blogTitle" => "El nuevo eXeLearning",
	"tags" => "Etiquetas",
	"index" => "Índice",
	"download" => "Descargar",
	"downloads" => "Descargas",
	"author" => "Autor",
	"subcategories" => "Sub-apartados",
	"searchIn" => "Buscar en",
	"chooseOne" => "Selecciona uno",
	"forums" => "Foros",
	"forumsTopics" => "Foros (Temas)",
	"forumsReplies" => "Foros (Respuestas)",
	"search" => "Buscar",
	"postedOn" => "Publicado el",
	"searchResultsFor" => "Resultados de la búsqueda para",
	"noSearchResults" => "No hay resultados para esa búsqueda. Por favor, inténtalo con otras palabras.",
	"noArticles" => "Todavía no hay ningún en este apartado. Disculpa las molestias."
);

//$current_page = echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; 
$lang = 'es';
if ($_GET['lang'] && $_GET['lang']==='en') {
	$lang = 'en';
} 
if (is_page()) {
	if (is_page(52)) $lang = 'en'; //Developers
}
else if (is_category()){
	$cat = get_query_var('cat');
	if (is_category(9) || get_top_parent($cat)==9) { //Documentación
		$isDocumentacion = true;		
	}
	else if (is_category(10) || get_top_parent($cat)==10) { //Documentation
		$lang = 'en';
		$isDocumentation = true;
	}
	else if (is_category(16) || get_top_parent($cat)==16) { //Descargas
		$isDescargas = true;
	}
	else if (is_category(17) || get_top_parent($cat)==17) { //Downloads
		$lang = 'en';
		$isDownloads = true;
	}
}
else if (is_single()) {
	if (is_parent_category(9)) { //Documentación
		$isDocumentacion = true;
	}
	else if (is_parent_category(10)) { //Documentation
		$lang = 'en';
		$isDocumentation = true;
	}
	else if (is_parent_category(16)) { //Descargas
		$isDescargas = true;
	}
	else if (is_parent_category(17)) { //Downloads
		$lang = 'en';
		$isDownloads = true;		
	}
}
if ($post->post_type=="forum" || $post->post_type=="topic" || $post->post_type=="reply") $isForum = true;
if ($lang=='en') {	
	$lang_link = '?lang=en';
	$my_i18n = array(
		"blogDesc" => "The evolution of eXeLearning",
		"blogTitle" => "The new eXeLearning",
		"tags" => "Tags",
		"index" => "Índice",
		"download" => "Download",
		"downloads" => "Downloads",
		"author" => "Author",
		"subcategories" => "Subcategories",
		"searchIn" => "Search in",
		"chooseOne" => "Choose one",
		"forums" => "Forums",
		"forumsTopics" => "Topics",
		"forumsReplies" => "Replies in Topics",
		"search" => "Buscar",
		"postedOn" => "Posted on",
		"searchResultsFor" => "Search results for",
		"noSearchResults" => "Sorry, but nothing matched your search criteria. Please try again with some different keywords.",
		"noArticles" => "There are currently no articles in this section."
	);
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang; ?>" lang="<?php echo $lang; ?>">
<?php /*
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="<?php echo $lang; ?>">
<!--<![endif]-->
*/
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo $themePath; ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $themePath; ?>/js/common.js"></script>
<?php
	if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div class="skip-link"><a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to primary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to primary content', 'twentyeleven' ); ?></a></div>
<div class="skip-link"><a class="assistive-text" href="#secondary" title="<?php esc_attr_e( 'Skip to secondary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to secondary content', 'twentyeleven' ); ?></a></div>
<div id="page" class="hfeed">
	<div id="header" class="autoclear">
			<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
			<<?php echo $heading_tag; ?> id="site-title">
				<span>
					<a href="<?php echo home_url( '/' ).$lang_link; ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo $my_i18n["blogTitle"]; ?></a>
				</span>
			</<?php echo $heading_tag; ?>>			
			<p id="site-description"><?php bloginfo( 'description' ); ?></p>			
			<div id="user-options">
				<p id="twitter-link">
					<?php if ($lang=="es") { ?>
						<a href="https://twitter.com/exelearning_sp">@exelearning_sp en Twitter</a>
					<?php } else { ?>
						<a href="https://twitter.com/exelearning_sp">@exelearning_sp on Twitter</a>
					<?php } ?>					
				</p>			
				<p id="user-login">
				<?php 
					global $blogURL;
					if ( is_user_logged_in() ) { 
						$user = wp_get_current_user(); 
						$logoutReferer=$_SERVER['REQUEST_URI'];
						echo 'Usuario: <a href="'.$blogURL.'/login/?action=profile" id="vs-user">'.$user->display_name.'</a>';
						//if (current_user_can('manage_options')) echo ' | <a href="/wp-admin/">Administrar</a> (<a href="/wp-admin/post.php?action=edit&amp;post='.currentPost().'">'.currentPost().'</a>)';
						echo ' | <a href="'.wp_logout_url($logoutReferer).'">Cerrar sesi&oacute;n</a>';
					} else echo '<a href="'.$blogURL.'/?pagename=login&amp;action=register" class="new-account">Crea tu cuenta</a> | <a href="'.wp_login_url($_SERVER['REQUEST_URI']).'">Iniciar sesi&oacute;n</a>';
				?>
				</p>			
				<p id="lang-switcher">
				<?php 
					//Language switcher
					if ($lang=='es') {
						echo "<strong>Español</strong> | <a href='".$blogURL."/?lang=en'>English</a>";
					} else {
						echo "<a href='".$blogURL."'>Español</a> | <strong>English</strong>";
					}
				?>
				</p>			
				<?php /*<div id="search-forms" class="autoclear">
					<div id="forum-search">
						<?php 
							//bbpress_search();
							the_widget("bbPress_Forum_Plugin_Search","title=".$my_i18n["searchInForumsTitle"]);				
						?>					
					</div>
				</div> */ ?>
				<form method="get" id="searchform" action="<?php echo $blogURL; ?>">
					<div>
						<label for="cat"><?php echo $my_i18n["searchIn"]; ?>:</label>
						<input type="hidden" name="lang" id="lang" value="<?php echo $lang; ?>" />
						<select id="cat" name="post_type">
							<option value="" selected="selected">- <?php echo $my_i18n["chooseOne"]; ?> -</option>
							<option value="forum"><?php echo $my_i18n["forums"]; ?></option>
							<option value="topic"><?php echo $my_i18n["forumsTopics"]; ?></option>
							<option value="reply"><?php echo $my_i18n["forumsReplies"]; ?></option>
						</select>
						<label for="s" class="scr-av"><?php echo $my_i18n["search"]; ?>:</label>
						<input type="text" value="" name="s" id="s" class="s" title="<?php echo $my_i18n["search"]; ?>" />
						<input type="submit" id="searchsubmit" class="js-scr-av" value="<?php echo $my_i18n["search"]; ?>" />
					</div>
				</form>
			</div>			
	</div><!-- #header -->
	
	<div id="nav">
		<h3 class="scr-av"><?php _e( 'Main menu', 'twentyeleven' ); ?></h3>
		<div class="menu">
		  <ul>
			<?php if ($lang=="es") { ?>
				<li<?php if (is_home()) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/">Inicio</a></li>
				<li<?php if ($isDescargas) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/category/descargas/">Descargas</a></li>
				<li<?php if ($isDocumentacion) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/category/documentacion/">Documentación</a></li>
				<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/">Foros</a></li>
				<li<?php if (is_page(50)) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/desarrolladores/">Desarrolladores</a></li>
			<?php } else { ?>
				<li<?php if (is_home()) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/?lang=en">Home</a></li>
				<li<?php if ($isDownloads) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/category/downloads/">Downloads</a></li>						
				<li<?php if ($isDocumentation) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/category/documentation/">Documentation</a></li>
				<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/?lang=en">Forums</a></li>
				<li<?php if (is_page(52)) { ?> class="current_page_item"<?php } ?>><a href="<?php echo $blogURL; ?>/developers/">Developers</a></li>
			<?php } ?>
		  </ul>				  
		</div>				
	</div><!-- #nav -->		

	<div id="main" class="autoclear">
		<?php if(function_exists('bcn_display') && !is_home() && !$isForum){
			echo "<p id='breadcrumbs'><a href='".$blogURL;
			if ($lang=='en') echo "/?lang=en";
			echo "'>".$my_i18n["blogTitle"]."</a> &raquo; ";
			if (is_search()) echo $my_i18n["searchResultsFor"].": '".get_search_query()."'";
			else bcn_display();
			echo "</p>";			
		} else if ($isForum) {
			$my_breadcrumbs = bbp_get_breadcrumb(); 
			$my_breadcrumbs = str_replace("&rsaquo;", "&raquo;", $my_breadcrumbs);
			$my_breadcrumbs = str_replace(' class="bbp-breadcrumb-home"','', $my_breadcrumbs);
			$my_breadcrumbs = str_replace(' class="bbp-breadcrumb-root"','', $my_breadcrumbs);
			$my_breadcrumbs = str_replace('>Home<', '>'.$my_i18n ['blogTitle'].'<', $my_breadcrumbs);
			if ($lang=='en') $my_breadcrumbs = str_replace('">', '?lang=en">', $my_breadcrumbs);
			else $my_breadcrumbs = str_replace('>Forums<', '>Foros<', $my_breadcrumbs);
			echo $my_breadcrumbs;		
		}
		?>