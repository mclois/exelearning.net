<?php get_header(); ?>
	<div id="primary">
		<div id="content">
			<?php if ($lang=="en") { ?>
			<p style="margin-top:30px">Welcome to the new <strong>eXeLearning</strong>.</p>
			<p>eXeLearning Community:</p>
			<ul>
				<li>Information</li>
				<li>Downloads</li>
				<li>Documentation</li>
				<li>Forums</li>
				<li>Development environment</li>
			</ul>
			<p style="margin-bottom:30px">The final version will be available very soon. </p>
			<?php } else { ?>
			<p style="margin-top:30px">Bienvenidos al nuevo <strong>eXeLearning</strong>.</p>
			<p>Una comunidad dedicada a eXeLearning.</p>
			<ul>
				<li>Información</li>
				<li>Descargas</li>
				<li>Documentación</li>
				<li>Foros</li>
				<li>Entorno de desarrollo</li>
			</ul>
			<p style="margin-bottom:30px">La versión definitiva estará disponible muy pronto. </p>				
			<?php } ?>
		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>