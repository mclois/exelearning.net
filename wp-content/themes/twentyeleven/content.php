<?php global $lang_link, $my_i18n; ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php /*
			<?php if ( is_sticky() ) : ?>
				<hgroup>
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<h3 class="entry-format"><?php _e( 'Featured', 'twentyeleven' ); ?></h3>
				</hgroup>
			<?php else : ?>
		*/ ?>			
		<?php 
			$t = get_the_title(); 			
			$my_t = str_replace("(plantilla para eXeLearning)", "<span class='scr-av'>(plantilla para eXeLearning)</span>", $t);
		?>			
		<h1 class="entry-title"><a href="<?php the_permalink(); echo $lang_link; ?>"><?php echo $my_t; ?></a></h1>			
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php twentyeleven_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		<?php /*
			<?php if ( comments_open() && ! post_password_required() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Reply', 'twentyeleven' ) . '</span>', _x( '1', 'comments number', 'twentyeleven' ), _x( '%', 'comments number', 'twentyeleven' ) ); ?>
			</div>
			<?php endif; ?>
		*/ ?>
		<?php
			$posttags = get_the_tags();
			if ($posttags) {
				echo "<p class='post-tags'>".$my_i18n["tags"].": ";
				foreach($posttags as $tag) {
					$tag_link = get_tag_link($tag->term_id).$lang_link;
					echo "<a href='".$tag_link."'>".$tag->name."</a> ";
				}
				echo "</p>";
			}
		?>			
	</div><!-- .entry-header -->
	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<div style="float:left;margin:0 20px 20px 0"><?php the_post_thumbnail("thumbnail"); ?></div>
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>			
	</div><!-- .entry-content -->
	<?php endif; ?>

</div>