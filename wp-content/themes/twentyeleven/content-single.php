<?php global $lang_link, $lang, $my_i18n; ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php 
			$t = get_the_title(); 
			$my_t = str_replace("(plantilla para eXeLearning)", "<span class='scr-av'>(plantilla para eXeLearning)</span>", $t);
			echo '<h1 class="entry-title">'.$my_t.'</h1>'
		?>
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php twentyeleven_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</div><!-- .entry-header -->
	<div class="entry-content">
		<?php
			$posttags = get_the_tags();
			if ($posttags) {
				echo $my_i18n["tags"].": ";
				foreach($posttags as $tag) {
					$tag_link = get_tag_link($tag->term_id).$lang_link;
					echo "<a href='".$tag_link."'>".$tag->name."</a> ";
				}
			}
		?>		
		<p><a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false, '' ); echo $src[0]; ?>	" rel="lightbox"><?php the_post_thumbnail("medium"); ?></a></p>
		<?php the_content(); ?>
		<ul>
			<?php
				$zip = get_post_custom_values("zip");
				if (sizeof($zip)>0 ) {
					foreach ( $zip as $key => $value ) echo "<li><a href='".$value."'>".$my_i18n['download']."</a></li>"; 
				}
				$theme_author = get_post_custom_values("autor");
				if (sizeof($theme_author)>0 ) {
					foreach ( $theme_author as $key => $value ) {
						$author_name=$value;
					}
				}
				$authorURL = get_post_custom_values("web_autor");
				if (sizeof($authorURL)>0 ) {
					foreach ( $authorURL as $key => $value ) {
						$author_url=$value;
					}
				}
				if ($author_name!='') {
					echo "<li>".$my_i18n["author"].": ";
					if ($author_url!='') echo "<a href='".$author_url."'>";
					echo $author_name;
					if ($author_url!='') echo "</a>";
					echo "</li>";
				}
			?>
		</ul>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	
</div>