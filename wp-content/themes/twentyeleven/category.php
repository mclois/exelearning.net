<?php
	get_header(); 
	global $isDocumentation, $isDocumentacion, $isDownloads, $isDescargas, $my_i18n;
?>
<?php if ($isDocumentation || $isDocumentacion) { ?>
	<div id="primary">
		<div id="content">
			<h1 class="page-title"><?php echo single_cat_title( '', false ); ?></h1>			
			<?php query_posts(array('category__in' => array($cat))); ?>
			<?php if ( have_posts() ) { ?>
			<ul>
				<?php while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
			</ul>
			<?php } else { ?>
				<p><?php echo $my_i18n['noArticles']; ?></p>
			<?php } ?>
			<?php
				$this_category = get_category($cat);
				if (get_category_children($this_category->cat_ID) != "") {
					echo '<h2>'.$my_i18n["subcategories"].'</h2>';
					echo '<ul>';
						wp_list_categories('hide_empty=0&title_li=&orderby=term_order&child_of='.$this_category->cat_ID);
					echo '</ul>';
				} 
			?>
		</div><!-- #content -->
	</div><!-- #primary -->	
	<div id="secondary">
		<?php
			$default = 9;
			if ($isDocumentation) $default = 10;
			$cur_cat_id= get_cat_id(single_cat_title( '', false ));		
			echo '<div class="sidebar-block"><h2 class="widget-title">'.$my_i18n["index"].'</h2>';
			echo "<ul>";
			wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&current_category='.$cur_cat_id);
			echo "</ul></div>";			
		?>
	</div>	
<?php } else if ($isDownloads || $isDescargas) { ?>
	<div id="primary">
		<div id="content">
			<h1 class="page-title"><?php echo single_cat_title( '', false ); ?></h1>
			<?php if ( have_posts() ) { ?>
				<?php twentyeleven_content_nav( 'nav-above' ); ?>
					<?php if (is_category(17)) { ?>
					<p>Choose your OS:</p>
					<ul>
						<li><strong>GNU/Linux</strong>
							<ul>
								<li><a href="https://forja.cenatic.es/frs/download.php/1444/python-exe_1.04.1.3605intef6_i386.deb">Debian/Ubuntu</a></li>
								<li><a href="https://forja.cenatic.es/frs/download.php/1446/exe-1.04.1.3605intef6-1.fc15.i386.rpm">Fedora/Redhat</a></li>
							</ul>
						</li>
						<li><strong>Apple</strong>
							<ul>
								<li><a href="https://forja.cenatic.es/frs/download.php/1447/iteexe-1.04.1.3605intef6.dmg">OS X Lion / OS X Leopard</a></li>
							</ul>
						</li>
						<li><strong>Microsoft Windows: </strong>
							<ul>
								<li><a href="https://forja.cenatic.es/frs/download.php/1448/eXe-install-1.04.1.3605intef6.exe">Installer</a></li>
								<li><a href="https://forja.cenatic.es/frs/download.php/1449/exe-ready2run-1.04.1.3605intef6.exe">Ready to run</a></li>
							</ul>
						</li>
					</ul>					
					<?php } else if (is_category(16)) { ?>
					<p>Descarga directa de las &uacute;ltimas versiones:</p>
					<ul>
						<li><strong>GNU/Linux</strong>
							<ul>
								<li><a href="https://forja.cenatic.es/frs/download.php/1444/python-exe_1.04.1.3605intef6_i386.deb">Debian/Ubuntu</a></li>
								<li><a href="https://forja.cenatic.es/frs/download.php/1446/exe-1.04.1.3605intef6-1.fc15.i386.rpm">Fedora/Redhat</a></li>
							</ul>
						</li>
						<li><strong>Apple</strong>
							<ul>
								<li><a href="https://forja.cenatic.es/frs/download.php/1447/iteexe-1.04.1.3605intef6.dmg">OS X Lion / OS X Leopard</a></li>
							</ul>
						</li>
						<li><strong>Microsoft Windows: </strong>
							<ul>
								<li><a href="https://forja.cenatic.es/frs/download.php/1448/eXe-install-1.04.1.3605intef6.exe">Instalador</a></li>
								<li><a href="https://forja.cenatic.es/frs/download.php/1449/exe-ready2run-1.04.1.3605intef6.exe">Auto-ejecutable</a></li>
							</ul>
						</li>
					</ul>					
					<?php } else { ?>
						<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
						<?php endwhile; ?>
						<?php twentyeleven_content_nav( 'nav-below' ); ?>
					<?php } ?>
			<?php } else { ?>
				<p><?php echo $my_i18n['noArticles']; ?></p>
			<?php } ?>
		</div><!-- #content -->
	</div><!-- #primary -->
	<div id="secondary">
		<?php
			$default = 16;
			if ($isDownloads) $default = 17;
			$cur_cat_id= get_cat_id(single_cat_title( '', false ));		
			echo '<div class="sidebar-block"><h2 class="widget-title">'.$my_i18n["downloads"].'</h2>';
			echo "<ul>";
			wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&current_category='.$cur_cat_id);
			echo "</ul></div>";			
		?>
	</div>	
<?php } else { ?>
	<?php /*
	<div id="primary">
		<div id="content">
			<?php if ( have_posts() ) : ?>
				<h1 class="page-title"><?php
					printf( __( 'Category Archives: %s', 'twentyeleven' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) ) echo apply_filters( 'category_archive_meta', '<div class="category-archive-meta">' . $category_description . '</div>' );
				?>
				<?php twentyeleven_content_nav( 'nav-above' ); ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
			<?php else : ?>
				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->
					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->
			<?php endif; ?>
		</div><!-- #content -->
	</div><!-- #primary -->
	*/ ?>
<?php } ?>
<?php get_footer(); ?>