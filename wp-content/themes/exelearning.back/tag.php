<?php 
	global $my_i18n;
	get_header();
?>
<div id="primary">
	<?php if ( have_posts() ) : ?>
		<h1><?php
			echo $my_i18n['tag'];
			printf( __( ': %s', 'twentyeleven' ), '<span>' . single_tag_title( '', false ) . '</span>' );
		?></h1>
		<?php
			$tag_description = tag_description();
			if ( ! empty( $tag_description ) )
				echo apply_filters( 'tag_archive_meta', '<div class="tag-archive-meta">' . $tag_description . '</div>' );
		?>
		<?php twentyeleven_content_nav( 'nav-above' ); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
		<?php twentyeleven_content_nav( 'nav-below' ); ?>
	<?php else : ?>
		<div id="post-0" class="post no-results not-found">
			<h1><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
			<p><?php echo $my_i18n['noArticles']; ?></p>
		</div>
	<?php endif; ?>
</div><!-- #primary -->
<?php 
	$tag = wp_tag_cloud('format=array' );
	if (sizeof($tag)==0) {
		get_sidebar();
	} else { 
?>
<div id="secondary">
	<h2><?php echo $my_i18n['tags']; ?></h2>
	<div id="tag-cloud" class="tag-<?php echo get_query_var('tag_id'); ?>"><?php wp_tag_cloud('format=list' ); ?></div>
</div>
<?php } ?>
<?php get_footer(); ?>