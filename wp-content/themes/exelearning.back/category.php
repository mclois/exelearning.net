<?php
	get_header(); 
	global $isDocumentation, $isDocumentacion, $isDocumentacionEU, $isDownloads, $isDescargas, $isDescargasEU, $my_i18n;
	if (is_category(16) || is_category(17) || is_category(42)) $isDownloadsPage = true;
?>
<?php if ($isDocumentation || $isDocumentacion || $isDocumentacionEU) { ?>
	<div id="primary"<?php if ($isDownloadsPage) echo " class='downloads-page'"; ?>>
		<h1 class="page-title"><?php echo single_cat_title( '', false ); ?></h1>			
		<?php query_posts(array('category__in' => array($cat))); ?>
		<?php if ( have_posts() ) { ?>
		<ul>
			<?php while ( have_posts() ) : the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
		</ul>
		<?php } else { ?>
			<p><?php echo $my_i18n['noArticles']; ?></p>
		<?php } ?>
		<?php
			$this_category = get_category($cat);
			if (get_category_children($this_category->cat_ID) != "") {
				echo '<h2>'.$my_i18n["subcategories"].'</h2>';
				echo '<ul>';
					wp_list_categories('hide_empty=0&title_li=&orderby=term_order&child_of='.$this_category->cat_ID);
				echo '</ul>';
			} 
		?>
	</div><!-- #primary -->	
	<div id="secondary">
		<?php
			$default = 9;
			if ($isDocumentation) $default = 10;
			else if ($isDocumentacionEU) $default = 43;
			$cur_cat_id= get_cat_id(single_cat_title( '', false ));		
			echo '<div class="sidebar-block"><h2 class="widget-title">'.$my_i18n["index"].'</h2>';
			echo "<ul>";
			wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&current_category='.$cur_cat_id);
			echo "</ul></div>";			
		?>
	</div>	
<?php } else if ($isDownloads || $isDescargas || $isDescargasEU) { ?>
	<div id="primary"<?php if (is_category(16) || is_category(17) || is_category(42)) { ?> class="downloads-page"<?php } ?>>
		<h1 class="page-title"><?php echo single_cat_title( '', false ); ?></h1>
		<?php if ( have_posts() ) { ?>
			<?php twentyeleven_content_nav( 'nav-above' ); ?>
				<?php /*
				<?php if (is_category(17)) { ?>
					<?php 
						$page_id = 223;
						$page_data = get_page( $page_id );
						echo apply_filters('the_content', $page_data->post_content);
					?>
				<?php } else if (is_category(16)) { ?>
					<?php 
						$page_id = 221;
						$page_data = get_page( $page_id );
						echo apply_filters('the_content', $page_data->post_content);
					?>
				<?php } else if (is_category(42)) { ?>
					<?php 
						$page_id = 601;
						$page_data = get_page( $page_id );
						echo apply_filters('the_content', $page_data->post_content);
					?>
				*/ ?>
				<?php if ($isDownloadsPage) { ?>
					<div style="position:relative">
						<p><?php echo $my_i18n['chooseYourOS']; ?>:</p>
						<?php 
							$page_id = 221;
							$page_data = get_page( $page_id );
							echo apply_filters('the_content', $page_data->post_content);
							echo "<p class='previous-versions'><a href='https://forja.cenatic.es/frs/?group_id=197' rel='external'>".$my_i18n['previousVersions']."</a></p>";
						?>
					</div>					
				<?php } else { ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
					<?php endwhile; ?>
					<?php twentyeleven_content_nav( 'nav-below' ); ?>
				<?php } ?>
		<?php } else { ?>
			<p><?php echo $my_i18n['noArticles']; ?></p>
		<?php } ?>		
	</div><!-- #primary -->
	<div id="secondary" class="other-downloads">
		<?php
			$default = 16;
			if ($isDownloads) $default = 17;
			else if ($isDescargasEU) $default = 42;
			$cur_cat_id= get_cat_id(single_cat_title( '', false ));		
			echo '<div class="sidebar-block"><h2 class="widget-title" id="d-t">'.$my_i18n["downloads"].'</h2>';
			echo "<ul>";
			wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&current_category='.$cur_cat_id);
			echo "</ul></div>";			
		?>
	</div>	
<?php } else { ?>
	<div id="primary">
		<h1 class="page-title"><?php echo single_cat_title( '', false ); ?></h1>
		<?php if ( have_posts() ) : ?>
			<?php twentyeleven_content_nav( 'nav-above' ); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			<?php twentyeleven_content_nav( 'nav-below' ); ?>
		<?php else : ?>
			<p><?php echo $my_i18n['noArticles']; ?></p>
		<?php endif; ?>		
	</div><!-- #primary -->
	<?php get_sidebar(); ?>	
<?php } ?>
<?php get_footer(); ?>