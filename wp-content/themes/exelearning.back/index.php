<?php global $blogURL, $themePath; ?>
<?php get_header(); ?>
	<div id="primary" class="autoclear">
		<?php if ($lang=="en") { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> is an Open Source authoring application to assist teachers and academics in the publishing of web content.</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> Resources authored in <strong>eXe</strong> can be exported in IMS Content Package, SCORM 1.2, or IMS Common Cartridge formats or as simple self-contained web pages.</li>
			</ul>
		</div>
		<div id="col-b">
			<p class="welcome">Welcome to the new <strong>eXeLearning</strong>.</p>
			<p class="logo"><img src="<?php echo $themePath; ?>/images/exelearning/home/exelenarning.jpg" width="150" height="150" alt="Logotipo de eXeLearning" /></p>
			<p>eXeLearning Community:</p>
			<ul>
				<li>Information</li>
				<li>Downloads</li>
				<li>Documentation</li>
				<li>Forums</li>
				<li>Development environment</li>
			</ul>		
		</div>
		<?php } else if ($lang=="eu") { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> Irakasleentzat web edukiak sortu eta argitaratzeko laguntza handiko kode irekian garatutako egiletasun tresna da.</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> <strong>eXe</strong>rekin garatutako edukiak hainbat formatutan gorde daitezke: IMS, SCORM 1.2 … Baita web orri nabigagarri moduan ere.</li>
			</ul>
		</div>
		<div id="col-b">
			<p>Orain dela hilabete batzuk,<strong>eXeLearning</strong>aren bertsio berriaren berri eman genuen.</p>
			<p>Gaur, <strong>eXeLearning berriari</strong> buruz dihardun gune honen martxa jartzearen berri ematen dugu.</p>
			<p class="logo"><img src="<?php echo $themePath; ?>/images/exelearning/home/exelenarning.jpg" width="150" height="150" alt="eXeLearningaren logoa" /></p>
			<p><a href="<?php echo $blogURL; ?>/garatzaileak/">Garapen</a> honetan, erakunde desberdinek kolaboratzen dute, baita elkarlanarekiko joera duten hainbat pertsonek, <a href="<?php echo $blogURL; ?>/jaitsi/">tresna probatu</a>, <a href="<?php echo $blogURL; ?>/forums/?lang=eu">eztabaidatu</a> eta partekatzen dute.</p>
			<p><strong>eXeLearning</strong> software askea da. Parte hartzea, hezkuntza komunitate osoari irekia dago.</p>
			<p>Denak zaudete gonbidatuak <a href="<?php echo $blogURL; ?>/login/?lang=eu">gurekin bidea egiten</a>.</p>			
		</div>
		<?php } else { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> es una herramienta de autor de código abierto para ayudar a los docentes en la creación y publicación de contenidos web.
</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> Los recursos elaborados con <strong>eXe</strong> pueden exportarse en diferentes formatos: IMS, SCORM 1.2&hellip; También como páginas web navegables.</li>
			</ul>
		</div>
		<div id="col-b">
			<p>Hace unos meses anunciamos el lanzamiento de la nueva versi&oacute;n de <strong>eXeLearning</strong>.</p>
			<p>Hoy, anunciamos la puesta en marcha de este espacio dedicado al <strong>nuevo eXeLearning</strong>.</p>
			<p class="logo"><img src="<?php echo $themePath; ?>/images/exelearning/home/exelenarning.jpg" width="150" height="150" alt="Logotipo de eXeLearning" /></p>
			<p>En su <a href="<?php echo $blogURL; ?>/desarrolladores/">desarrollo</a> colaboran diferentes instituciones, y tambi&eacute;n personas que amablemente y siguiendo un esp&iacute;ritu colaborativo <a href="<?php echo $blogURL; ?>/descargas/">prueban la herramienta</a>, <a href="<?php echo $blogURL; ?>/forums/">debaten</a> y comparten.</p>
			<p><strong>eXeLearning</strong> es software libre. La participaci&oacute;n est&aacute; abierta a toda la comunidad educativa.</p>
			<p>Est&aacute;is tod@s <a href="<?php echo $blogURL; ?>/login/">invitados a acompa&ntilde;arnos</a>.</p>
		</div>		
		<?php } ?>		
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>