<div class="lang en">
	<h2>English</h2>
	<dl>
		<dt><a href="<?php echo $blogURL; ?>/?lang=en">Home</a></dt>
		<dd>Home page &amp; last news.</dd>
		<dt><a href="<?php echo $blogURL; ?>/descargas/">Descargas</a></dt>
		<dd>
			Last versions and:
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=17&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1'); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/forums/?lang=en">Forums</a></dt>
		<dd>The forums are common to all languages:
			<ul>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/?lang=en">Open discussion</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/?lang=en">Developers</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/?lang=en">Work groups</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/?lang=en">Help</a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/news/">News</a></dt>
		<dd>All about the new eXeLearning.</dd>
		<dt><a href="<?php echo $blogURL; ?>/features/">Features</a></dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/features/#tab1">Main features</a></li>
				<li><a href="<?php echo $blogURL; ?>/features/#tab2">Road map</a></li>
				<li><a href="<?php echo $blogURL; ?>/features/#tab3">Changelog</a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/documentation/">Documentation</a></dt>
		<dd>
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=10&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1'); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/developers/">Developers</a></dt>
		<dd>Be informed and participate in the project!</dd>
		<dt>Other sections</dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/contacto/?lang=en">Contact</a></li>
				<li><a href="<?php echo $blogURL; ?>/rss-en/">RSS</a></li>
			</ul>
		</dd>
	</dl>				
</div><!-- /en -->