<?php 
	global $my_i18n, $themePath, $lang_link; 
	$destination = "ismail.ali@ite.educacion.es,antonio.monje@ite.educacion.es";
?>
<div id="primary">
	<h1><?php echo $my_i18n['contact']; ?></h1>
	<?php if (!$_POST) { ?>
	<div id="contact">
		<form id="contactForm" action="<?php echo $blogURL; ?>/contacto/<?php echo $lang_link; ?>" method="post" class="autoclear">
		<div class="personalData">
			<div class="formField autoclear">
				<label for="your-name"><span><?php echo $my_i18n['cf_name']; ?><b>*</b>: </span>
				<input type="text" name="your-name" id="your-name" /></label>
			</div>
			<div class="formField autoclear">
				<label for="your-email"><span><?php echo $my_i18n['cf_eMail']; ?><b>*</b>: </span>
				<input type="text" name="your-email" id="your-email" /></label>
			</div>
			<div class="formField autoclear">
				<label for="asunto"><span><?php echo $my_i18n['cf_subject']; ?>: </span>
				<input type="text" name="asunto" id="asunto" /></label>
			</div>
		</div>																	
		<div class="comments">
			<div class="formField autoclear textarea">
			<label for="coment"><span><?php echo $my_i18n['cf_comments']; ?>: </span>
			<textarea name="coment" id="coment" cols="32" rows="6"></textarea></label>
			</div>
			<div class="formField captcha"><?php echo $my_i18n['cf_captchaText']; ?><b>*</b>:<br />
				<img src="<?php echo $themePath; ?>/images/exelearning/contact/check.php" alt="<?php echo $my_i18n['cf_captchaImgAlt']; ?>" width="120" height="30" /><br />
				<input type="text" name="imagen" id="imagen" /><br />
				<span class="info"><?php echo $my_i18n['cf_captchaWarning']; ?></span>
			</div>			
			<div class="formField scr-not-av">
				<label for="anti-s"><span><?php echo $my_i18n['cf_leaveThisFieldEmpty']; ?> </span>
				<input type="text" name="anti-s" id="anti-s" title="<?php echo $my_i18n['cf_emptyFieldTitle']; ?>" /></label>
			</div>
			<div class="formButtons autoclear">
				<input type="submit" value="<?php echo $my_i18n['cf_send']; ?>" name="submit-form" id="submit-form" class="form-submit" /> 
				<span id="errors"><strong><b>*</b><?php echo $my_i18n['cf_requiredFields']; ?>:</strong> <?php echo $my_i18n['cf_requiredFieldsList']; ?>.</span>
			</div>    
		</div>
		</form>
	</div>	
	<script type="text/javascript">contacto.init();</script>
	<?php 
		} else {
		
			//SETTINGS.
			$nombre =  $_POST["your-name"];
			$email =  $_POST["your-email"];
			$asunto =  $_POST["asunto"];
			$comment = $_POST['coment'];
			$image = $_POST['imagen'];
			$antiS = $_POST["anti-s"];
			
			if (empty($nombre)) $error .= "<li>".$my_i18n['cf_e_tellUsYourName']."</li>";
			if (empty($email)) $error .= "<li>".$my_i18n['cf_e_giveUsAnEmail']."</li>";
			
			//Captcha
			session_start();
			$session = $_SESSION['captcha']; //Define the session we set in image.php
			$image = md5($image); //MD5 encrypt the post
			if ($session != $image) $error .= "<li>".$my_i18n['cf_e_mismatch']."</li>";
			
			//Anti-SPAM
			if (!empty($antiS)) $error .= "<li>".$my_i18n['cf_e_controlFieldWarning']."</li>";
				
			//Email
			if($email) {
				if(isset($_POST['email'])) {
					if (preg_match('/^[-!#$%&\'*+\\.\/0-9=?A-Z^_`{|}~]+@([-0-9A-Z]+\.)+([0-9A-Z]){2,4}$/i', trim($email))) {
						//Do nothing
					} else {
						$error .= "<li>".$my_i18n['cf_e_checkEmailSpell']."</li>";
					}
					//Check for valid domain name
					$ok = true;
					$ok = eregi( "^[_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3}$", $email,$check);
					$ok = getmxrr(substr(strstr($check[0], '@'), 1), $dummy);
					if($ok === false) {
						$host = substr($email, strpos($email, '@') + 1);
						if(gethostbyname($host) != $host) $ok = true;
						if ($ok != true) $error .= "<li>".$my_i18n['cf_e_wrongEmail']."</li>";
					}
				}
			}
			//Error
			if($error) {
				echo "<div class='errors'><p>".$my_i18n['cf_e_errorIntro']."</p><ul>";
				echo $error;
				$path='#URL_SITE_SPIP';
				echo "</ul><p>&laquo; <a href='".$blogURL."/contacto/".$lang_link."'>".$my_i18n['cf_e_back']."</a></p></div>";
			}
			else {
				$destinationName = "eXeLearning.net";
				$subject = "Formulario desde eXeLearning.net";
				$contentType = "text/plain"; //text/html or text/plain
				$charset = "utf-8"; //iso-8859-1 or UTF-8
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: '.$contentType.'; charset='.$charset.'' . "\r\n";
				$headers .= 'From: '.$nombre.' <'.$email.'>' . "\r\n";
				$headers .= 'To: '.$destinationName.' <'.$destination.'>' . "\r\n";
				
				$body .= "Nombre: ".$nombre."\n";
				$body .= "E-mail: ".$email."\n";
				$body .= "Asunto: ".$asunto."\n";
				$body .= "Comentarios:\n".$comment."\n";
				if ($contentType=="text/html") $body = str_replace("\n", "<br />", $body);
				
				if (function_exists('mail')) {
					try {
						mail($destination,$subject,$body,$headers);
						echo "<p>".$my_i18n['cf_s_message']."</p>";
					} catch(Exception $e) {
						echo "<p>".$my_i18n['cf_s_error_message']."</p>";
					}
				} else {
					echo "<p>".$my_i18n['cf_s_error_message']."</p>";
				}
				
			}			
		} 
	?>	
</div>
<div id="secondary">
<?php get_sidebar(); ?>
</div>