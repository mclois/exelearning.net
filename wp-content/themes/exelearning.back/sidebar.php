<?php global $blogURL, $my_i18n, $lang; ?>
<div id="secondary">
	<?php if (is_page(215) || is_page(217) || is_page(614)) { ?>
		<p id="rss-alternate"><?php echo $my_i18n['youCanAlsoFollowUsOnTwitter']; ?>&hellip; 
			<a href='https://twitter.com/exelearning_sp' class="js-scr-av">@exelearning_sp</a>
		</p>
	<?php } else { ?>
	<div id="rss-feeds" class="sidebar-block">
		<h2 class='widget-title'>RSS</h2>
		<p><a href="<?php echo $blogURL; ?>/rss-<?php echo $lang; ?>/"><?php echo $my_i18n['followUsViaRSS']; ?></a> &raquo;</p>		
	</div>
	<?php } ?>
	<script  type="text/javascript" charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>
	<script type="text/javascript">
	new TWTR.Widget({
	  version: 2,
	  type: 'profile',
	  rpp: 4,
	  interval: 30000,
	  width: 260,
	  height: 300,
	  theme: {
		shell: {
		  background: '#666666',
		  color: '#ffffff'
		},
		tweets: {
		  background: '#ffffff',
		  color: '#333333',
		  links: '#bb0000'
		}
	  },
	  features: {
		scrollbar: false,
		loop: false,
		live: true,
		behavior: 'all'
	  }
	}).render().setUser('exelearning_sp').start();
	</script>	
</div>