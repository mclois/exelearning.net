<?php get_header(); ?>
<?php global $blogURL, $my_i18n, $isDocumentation, $isDocumentacion, $isDocumentacionEU, $isDocumentacionCA, $isDocumentacionGL, $isDescargas, $isDescargasEU, $isDescargasCA, $isDescargasGL, $isDownloads; ?>
		<div id="primary">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
					global $lang;
					
					$prevPost = get_previous_post(true);
					$prevURL = get_permalink($prevPost->ID);
					$prevTitle = $prevPost->post_title;
					
					$nextPost = get_next_post(true);
					$nextURL = get_permalink($nextPost->ID);
					$nextTitle = $nextPost->post_title;					
					
					$showInternalNav = true;
					if (($prevPost=='') && ($nextPost=='')) $showInternalNav = false;
					if ($showInternalNav) echo "<div id='nav-single' class='serial-nav autoclear'>";
					previous_post_link('<span class="nav-previous"><a href="'.$prevURL.'" title="'.$prevTitle.'"><strong>← '.$my_i18n["previous"].'</strong></a></span>','Anterior',true);
					next_post_link('<span class="nav-next"><a href="'.$nextURL.'" title="'.$nextTitle.'"><strong>'.$my_i18n["next"].' →</strong></a></span>','Siguiente',true) ;
					if ($showInternalNav) echo "</div>";					
				?>
				<?php get_template_part( 'content', 'exe_style' ); ?>
				<?php //comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>			
		</div><!-- #primary --><?php
        $default = 16;
        $send_style_path = 'enviar-estilo';
        if ($isDownloads) {
          $default = 17;
          $send_style_path = 'send-style';
        }
  			else if ($isDescargasEU) {
          $default = 42;
          $send_style_path = 'bidali-estilo';
        }
        else if ($isDescargasCA) {
          $default = 260;
          $send_style_path = 'enviar-estil';
        }
        else if ($isDescargasGL) {
          $default = 263;
          $send_style_path = 'enviar-estilo-ga';
        }
				
				$category = get_the_category();
				$cur_cat_id =  $category[0]->term_id;			
				$side_title=$my_i18n["index"];
				$cN = "";
				if ($isDescargas || $isDescargasEU || $isDescargasCA || $isDescargasGL || $isDownloads) {
					$side_title=$my_i18n["downloads"];
					$cN = ' class="other-downloads"';
				}
		?>
  		<div id="secondary"<?php  echo $cN; ?>><div class="sidebar-block">
        <div id="contribute-links">
          <a class="send-style" href="/<?php echo $send_style_path ?>"><?php echo $my_i18n['send_your_style']; ?></a>
        </div>
        
  		  <h2 class="widget-title"><?php  echo $side_title; ?></h2>
  		  <ul>
          <?php  wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&show_option_none='.$my_i18n['noArticles'].'&current_category='.$cur_cat_id); ?>					
  			</ul>
  	  </div>
	  </div>
	  
<?php get_footer(); ?>