<?php
//error_reporting(0);
global $blogURL, $themePath, $lang, $lang_link, $my_i18n, $isDocumentacion, $isDocumentacionEU, $isDocumentacionCA, $isDocumentacionGL, $isDocumentation, $isDescargas, $isDescargasEU, $isDescargasCA, $isDescargasGL, $isDownloads, $isForum, $isContact, $isSiteMap, $isActualidad, $isActualidadEU, $isActualidadCA, $isActualidadGL, $isNews;
$blogURL = get_bloginfo("wpurl");
$themePath = get_bloginfo("template_url");
$lang_link="";

//echo "<pre>";
//print_r($post);
//echo "</pre>";
$isExeStyleArchive = is_tax('exe_style_tag') || is_tax('exe_style_colour') || is_tax('exe_style_compatibility') || is_tax('exe_style_license');

//$current_page = echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; 
$lang = 'es';
if ($_GET['lang']) {
    $l = $_GET['lang'];
	if ($l==='en' || $l==='eu' || $l==='ca' || $l==='gl') $lang = $l;
} 
if (is_page()) {
	if (is_page(221)) $isDescargas = true;
	else if (is_page(322)) $isContact = true;
	else if (is_page(324)) $isSiteMap = true;
	else if (is_page(223)) {
		$isDownloads = true;
		$lang = 'en';
	}
	else if (is_page(601)) {
		$isDescargasEU = true;
		$lang = 'eu';
	}
	else if (is_page(1616)) {
		$isDescargasCA = true;
		$lang = 'ca';
	}
	else if (is_page(1618)) {
		$isDescargasGL = true;
		$lang = 'gl';
	}    
	else if (is_page(52) || is_page(202) || is_page(217)) $lang = 'en'; //Developers, Features & RSS
	else if (is_page(603) || is_page(471) || is_page(614)) $lang = 'eu'; //Developers, Features & RSS (eu)
	else if (is_page(1626) || is_page(1620) || is_page(1641)) $lang = 'ca'; //Developers, Features & RSS (ca)  
    else if (is_page(1628) || is_page(1624) || is_page(1643)) $lang = 'gl'; //Developers, Features & RSS (gl)
}
else if (is_category()){
	$cat = get_query_var('cat');
	if (is_category(9) || get_top_parent($cat)==9) { //Documentación
		$isDocumentacion = true;		
	}
	else if (is_category(10) || get_top_parent($cat)==10) { //Documentation
		$lang = 'en';
		$isDocumentation = true;
	}
	else if (is_category(43) || get_top_parent($cat)==43) { //Documentación (eu)
		$lang = 'eu';
		$isDocumentacionEU = true;
	}
	else if (is_category(257) || get_top_parent($cat)==257) { //Documentación (ca)
		$lang = 'ca';
		$isDocumentacionCA = true;
	}
	else if (is_category(258) || get_top_parent($cat)==258) { //Documentación (gl)
		$lang = 'gl';
		$isDocumentacionGL = true;
	}    
	else if (is_category(16) || get_top_parent($cat)==16) { //Descargas
		$isDescargas = true;
	}
	else if (is_category(17) || get_top_parent($cat)==17) { //Downloads
		$lang = 'en';
		$isDownloads = true;
	}
	else if (is_category(42) || get_top_parent($cat)==42) { //Descargas (eu)
		$lang = 'eu';
		$isDescargasEU = true;
	}
	else if (is_category(260) || get_top_parent($cat)==260) { //Descargas (ca)
		$lang = 'ca';
		$isDescargasCA = true;
	}
	else if (is_category(263) || get_top_parent($cat)==263) { //Descargas (gl)
		$lang = 'gl';
		$isDescargasGL = true;
	}    
	if (is_category(22) || get_top_parent($cat)==22) { //Actualidad
		$isActualidad = true;		
	}
	else if (is_category(23) || get_top_parent($cat)==23) { //News
		$lang = 'en';
		$isNews = true;
	}
	else if (is_category(41) || get_top_parent($cat)==41) { //Actualidad (eu)
		$lang = 'eu';
		$isActualidadEU = true;
	}
	else if (is_category(255) || get_top_parent($cat)==255) { //Actualidad (ca)
		$lang = 'ca';
		$isActualidadCA = true;
	}
	else if (is_category(256) || get_top_parent($cat)==256) { //Actualidad (gl)
		$lang = 'gl';
		$isActualidadGL = true;
	}	
}
else if (is_single()) {
	if (is_parent_category(9)) { //Documentación
		$isDocumentacion = true;
	}
	else if (is_parent_category(10)) { //Documentation
		$lang = 'en';
		$isDocumentation = true;
	}
	else if (is_parent_category(43)) { //Documentation (eu)
		$lang = 'eu';
		$isDocumentacionEU = true;
	}
	else if (is_parent_category(257)) { //Documentation (ca)
		$lang = 'ca';
		$isDocumentacionCA = true;
	}
	else if (is_parent_category(258)) { //Documentation (gl)
		$lang = 'gl';
		$isDocumentacionGL = true;
	}    
	else if (is_parent_category(16)) { //Descargas
		$isDescargas = true;
	}
	else if (is_parent_category(17)) { //Downloads
		$lang = 'en';
		$isDownloads = true;		
	}
	else if (is_parent_category(42)) { //Descargas (eu)
		$lang = 'eu';
		$isDescargasEU = true;		
	}
	else {
		$post_cat = get_the_category();
		if ($post_cat[0]->cat_ID==22) { //Actualidad
			$isActualidad = true;
		}
		else if ($post_cat[0]->cat_ID==23) { //News
			$lang = 'en';
			$isNews = true;			
		}
		else if ($post_cat[0]->cat_ID==41) { //Actualidad (eu)
			$lang = 'eu';
			$isActualidadEU = true;			
		}
	}
}
if ($post->post_type=="forum" || $post->post_type=="topic" || $post->post_type=="reply") $isForum = true;
if ($lang!='es') {	
	$lang_link = '?lang='.$lang;
}
include("languages/exelearning/".$lang.".php");
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang; ?>" lang="<?php echo $lang; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?rev=20130923" />
<!--[if lt IE 8]><link rel="stylesheet" type="text/css" media="all" href="<?php echo $themePath; ?>/css/style_ie_lt_8.css?rev=20130118" /><![endif]-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!--<script type="text/javascript" src="<?php echo $themePath; ?>/js/jquery.min.js"></script>-->
<?php
	if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
<?php if ($isDescargas || $isDownloads || $isDescargasEU || $isDescargasCA || $isDescargasGL || $isExeStyleArchive)  { ?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $themePath; ?>/exe-styles.css" />
<?php  } ?>
<script type="text/javascript" src="<?php echo $themePath; ?>/js/lang/lang_<?php echo $lang; ?>.js"></script>
<script type="text/javascript" src="<?php echo $themePath; ?>/js/common.js?rev=20120705"></script>
</head>

<body <?php body_class(); ?>><script type="text/javascript">document.body.className+=" js";</script>
<div class="skip-link"><a class="assistive-text" href="#content"><?php echo $my_i18n["skipToContent"]; ?></a></div>
<div id="page">
	<div id="header-wrapper">
		<div id="header" class="autoclear">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="logo"><a href="<?php echo home_url( '/' ).$lang_link; ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><span><?php echo $my_i18n["blogTitle"]; ?></span></a></<?php echo $heading_tag; ?>>			
				<p id="site-desc"><strong><b>eXe</b>Learning<span>.net</span></strong> <?php echo $my_i18n["blogDesc"]; ?>&hellip;</p>
				<div id="user-options">
					<p id="twitter-link"><a href="https://twitter.com/exelearning_sp"><?php echo $my_i18n["exeLearningOnTwitter"]; ?></a></p>			
					<p id="user-login">
					<?php 
						global $blogURL;
						if ( is_user_logged_in() ) { 
							$user = wp_get_current_user(); 
							$logoutReferer=$_SERVER['REQUEST_URI'];
							echo 'Usuario: <a href="'.$blogURL.'/login/?action=profile" id="vs-user">'.$user->display_name.'</a>';
							if (!current_user_can('subscriber')) echo ' - <a href="'.$blogURL.'/wp-admin/">'.$my_i18n["administrationLink"].'</a>';
							if (currentPost()!='' && currentPost()!='0') echo ' (<a href="'.$blogURL.'/wp-admin/post.php?action=edit&amp;post='.currentPost().'">'.currentPost().'</a>)';
							echo ' - <a href="'.wp_logout_url($logoutReferer).'">'.$my_i18n["closeSessionLink"].'</a>';
						} else echo '<a href="'.$blogURL.'/?pagename=login&amp;action=register" class="new-account">'.$my_i18n["createAccountLink"].'</a> - <a href="'.wp_login_url($_SERVER['REQUEST_URI']).'">'.$my_i18n["loginLink"].'</a>';
					?>
					</p>			
					<p id="lang-switcher">
						<a href="<?php echo $blogURL; ?>"<?php if ($lang=="es") echo ' class="current"'; ?> hreflang="es">Español</a> - 
                        <a href="<?php echo $blogURL; ?>/?lang=ca"<?php if ($lang=="ca") echo ' class="current"'; ?> hreflang="ca">Català</a> - 
                        <a href="<?php echo $blogURL; ?>/?lang=eu"<?php if ($lang=="eu") echo ' class="current"'; ?> hreflang="eu">Euskara</a> - 
                        <a href="<?php echo $blogURL; ?>/?lang=gl"<?php if ($lang=="gl") echo ' class="current"'; ?> hreflang="gl">Galego</a> - 
                        <a href="<?php echo $blogURL; ?>/?lang=en"<?php if ($lang=="en") echo ' class="current"'; ?> hreflang="en">English</a>						
					</p>			
					<?php 
						//bbpress_search();
						//the_widget("bbPress_Forum_Plugin_Search","title=".$my_i18n["searchInForumsTitle"]);
					?>					
					<form method="get" id="searchform" action="<?php echo $blogURL; ?>">
						<div>
							<label for="cat"><?php echo $my_i18n["searchIn"]; ?>:</label>
							<input type="hidden" name="lang" id="lang" value="<?php echo $lang; ?>" />
							<select id="cat" name="post_type">
								<option value="" selected="selected">- <?php echo $my_i18n["chooseOne"]; ?> -</option>
								<option value="forum"><?php echo $my_i18n["forums"]; ?></option>
								<option value="topic"><?php echo $my_i18n["forumsTopics"]; ?></option>
								<option value="reply"><?php echo $my_i18n["forumsReplies"]; ?></option>
							</select>
							<label for="s" class="scr-av"><?php echo $my_i18n["search"]; ?>:</label>
							<input type="text" value="" name="s" id="s" class="s" title="<?php echo $my_i18n["search"]; ?>" />
							<input type="submit" id="searchsubmit" class="js-scr-av" value="<?php echo $my_i18n["search"]; ?>" />
						</div>
					</form>
				</div>
				<div id="nav">
					<ul>
					<?php if ($lang=="es") { ?>
						<li<?php if (is_home()) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/"><?php echo $my_i18n["navHome"]; ?></a></li>
						<li<?php if ($isDescargas) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/descargas/"><?php echo $my_i18n["navDownloads"]; ?></a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/"><?php echo $my_i18n["navForums"]; ?></a></li>
						<li<?php if ($isActualidad) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/actualidad/"><?php echo $my_i18n["navNews"]; ?></a></li>
						<li<?php if (is_page(200)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/caracteristicas/"><?php echo $my_i18n["navFeatures"]; ?></a></li>
						<li<?php if ($isDocumentacion) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/documentacion/"><?php echo $my_i18n["navDocumentation"]; ?></a></li>
						<li<?php if (is_page(50)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/desarrolladores/"><?php echo $my_i18n["navDevelopers"]; ?></a></li>
					<?php } else if ($lang=="eu") { ?>
						<li<?php if (is_home()) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/?lang=eu"><?php echo $my_i18n["navHome"]; ?></a></li>
						<li<?php if ($isDescargasEU) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/jaitsi/"><?php echo $my_i18n["navDownloads"]; ?></a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/?lang=eu"><?php echo $my_i18n["navForums"]; ?></a></li>
						<li<?php if ($isActualidadEU) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/berriak/"><?php echo $my_i18n["navNews"]; ?></a></li>
						<li<?php if (is_page(471)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/ezaugarriak/"><?php echo $my_i18n["navFeatures"]; ?></a></li>
						<li<?php if ($isDocumentacionEU) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/dokumentazioa/"><?php echo $my_i18n["navDocumentation"]; ?></a></li>
						<li<?php if (is_page(603)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/garatzaileak/"><?php echo $my_i18n["navDevelopers"]; ?></a></li>					
					<?php } else if ($lang=="ca") { ?>
						<li<?php if (is_home()) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/?lang=ca"><?php echo $my_i18n["navHome"]; ?></a></li>
						<li<?php if ($isDescargasCA) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/descarregues/"><?php echo $my_i18n["navDownloads"]; ?></a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/?lang=ca"><?php echo $my_i18n["navForums"]; ?></a></li>
						<li<?php if ($isActualidadCA) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/actualitat/"><?php echo $my_i18n["navNews"]; ?></a></li>
						<li<?php if (is_page(1620)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/caracteristiques/"><?php echo $my_i18n["navFeatures"]; ?></a></li>
						<li<?php if ($isDocumentacionCA) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/documentacio/"><?php echo $my_i18n["navDocumentation"]; ?></a></li>
						<li<?php if (is_page(1626)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/desenvolupadors/"><?php echo $my_i18n["navDevelopers"]; ?></a></li>					
					<?php } else if ($lang=="gl") { ?>
						<li<?php if (is_home()) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/?lang=gl"><?php echo $my_i18n["navHome"]; ?></a></li>
						<li<?php if ($isDescargasGL) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/descargas-gl/"><?php echo $my_i18n["navDownloads"]; ?></a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/?lang=gl"><?php echo $my_i18n["navForums"]; ?></a></li>
						<li<?php if ($isActualidadGL) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/actualidade/"><?php echo $my_i18n["navNews"]; ?></a></li>
						<li<?php if (is_page(1624)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/caracteristicas-gl/"><?php echo $my_i18n["navFeatures"]; ?></a></li>
						<li<?php if ($isDocumentacionGL) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/documentacion-gl/"><?php echo $my_i18n["navDocumentation"]; ?></a></li>
						<li<?php if (is_page(1628)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/desenvolvedores/"><?php echo $my_i18n["navDevelopers"]; ?></a></li>					
					<?php } else { ?>
						<li<?php if (is_home()) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/?lang=en"><?php echo $my_i18n["navHome"]; ?></a></li>
						<li<?php if ($isDownloads) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/downloads/"><?php echo $my_i18n["navDownloads"]; ?></a></li>
						<li<?php if ($post->post_type=="forum" || $post->post_type=="topic") { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/forums/?lang=en"><?php echo $my_i18n["navForums"]; ?></a></li>
						<li<?php if ($isNews) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/news/"><?php echo $my_i18n["navNews"]; ?></a></li>
						<li<?php if (is_page(202)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/features/"><?php echo $my_i18n["navFeatures"]; ?></a></li>
						<li<?php if ($isDocumentation) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/category/documentation/"><?php echo $my_i18n["navDocumentation"]; ?></a></li>
						<li<?php if (is_page(52)) { ?> class="current"<?php } ?>><a href="<?php echo $blogURL; ?>/developers/"><?php echo $my_i18n["navDevelopers"]; ?></a></li>
					<?php } ?>
					</ul>				  			
				</div><!-- #nav -->
		</div><!-- #header -->
	</div><!-- #header-wrapper -->
	<div id="content-wrapper">
		<div id="content" class="autoclear">
		<?php if(function_exists('bcn_display') && !is_home() && !$isForum){
			echo "<p id='breadcrumbs'><a href='".$blogURL;
			if ($lang!='es') echo "/?lang=".$lang;
			echo "'>".$my_i18n["blogTitle"]."</a> &raquo; ";
			if (is_search()) echo $my_i18n["searchResultsFor"].": '".get_search_query()."'";
			$breadcrumbs = bcn_display(true);
			if ($isDescargas) $breadcrumbs = str_replace('/category/descargas/"', '/descargas/"', $breadcrumbs);
			else if ($isDownloads) $breadcrumbs = str_replace('/category/downloads/"', '/downloads/"', $breadcrumbs);
			else if ($isDescargasEU) $breadcrumbs = str_replace('/category/jaitsi/"', '/jaitsi/"', $breadcrumbs);
            else if ($isDescargasCA) $breadcrumbs = str_replace('/category/descarregues/"', '/descarregues/"', $breadcrumbs);
            else if ($isDescargasGL) $breadcrumbs = str_replace('/category/descargas-gl/"', '/descargas-gl/"', $breadcrumbs);
			else if ($isSiteMap) $breadcrumbs = $my_i18n['siteMap'];
			else if ($isContact) $breadcrumbs = $my_i18n['contact'];
			echo $breadcrumbs;
			echo "</p>";			
		} else if ($isForum) {
			$my_breadcrumbs = bbp_get_breadcrumb(); 
			$my_breadcrumbs = str_replace("&rsaquo;", "&raquo;", $my_breadcrumbs);
			$my_breadcrumbs = str_replace(' class="bbp-breadcrumb-home"','', $my_breadcrumbs);
			$my_breadcrumbs = str_replace(' class="bbp-breadcrumb-root"','', $my_breadcrumbs);
			$my_breadcrumbs = str_replace('>Home<', '>'.$my_i18n ['blogTitle'].'<', $my_breadcrumbs);
			if ($lang!='es') $my_breadcrumbs = str_replace('">', '?lang='.$lang.'">', $my_breadcrumbs);
			$my_breadcrumbs = str_replace('>Forums<', '>'.$my_i18n["navForums"].'<', $my_breadcrumbs);
			echo "<div id='breadcrumbs'>".$my_breadcrumbs."</div>";
		}
		?>