<?php global $lang_link, $lang, $my_i18n;  ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php 
			$t = get_the_title(); 
			$my_t = str_replace("(plantilla para eXeLearning)", "<span class='scr-av'>(plantilla para eXeLearning)</span>", $t);
		?>
			<div class="with-data">
				<?php echo '<h1 class="entry-title">'.$my_t.'</h1>'; ?>
			</div>
			<div id="add-this"></div>
	</div><!-- .entry-header -->
	<div class="entry-content">
    <?php 
		$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', false, '' ); 
		if ($src[0]!='') { ?>
	  <p class="entry-main-img"><?php the_post_thumbnail(); ?></p>
	  <?php
    } 
    
    $style_demo_url = get_post_meta(get_the_ID(), '_exe_style_demo_url_meta_key', true );
	  if (!empty($style_demo_url)) {
    ?>
	    <p class="exe_style-demo-link"><a target="_blank" href="<?php echo $style_demo_url . $lang_link;?>">Demo >></a></p>
	  <?php
    } ?>
		
		<?php the_content(); ?>
    
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>	
	</div><!-- .entry-content -->
	
	<div class="entry-meta autoclear" id="add-this">
	  <div class="exe-style exe-style-version">
	    <span class="label"><?php echo $my_i18n['exe_style_labels']['version'] . ': '; ?></span>
	    <span class="value"><?php echo get_post_meta(get_the_ID(), '_exe_style_version_meta_key', true ); ?></span>
		</div>
		
	  <div class="exe-style exe-style-version">
	    <span class="label"><?php echo $my_i18n['exe_style_labels']['author'] . ': '; ?></span>
	    <span class="value"><?php 
			  $style_author = get_post_meta(get_the_ID(), '_exe_style_author_meta_key', true );
			  $style_author_url = get_post_meta(get_the_ID(), '_exe_style_author_url_meta_key', true );

			  if (!empty($style_author_url)) {
          echo '<a href="' . $style_author_url . $lang_link . '" target="_blank">' . $style_author . '</a>';
        }
        else {
          echo $style_author;
        }
			?></span>
		</div>
		
	  <div class="exe-style exe-style-license">
	    <span class="label"><?php echo $my_i18n['exe_style_labels']['license'] . ': '; ?></span>
	    <span class="value"><?php 
        $license = wp_get_object_terms($post->ID, 'exe_style_license');
        $license = array_shift($license);
        
        echo '<a href="' . get_term_link($license) . $lang_link . '">' . $license->name . '</a>';
      ?></span>
		</div>
		
	  <div class="exe-style exe-style-compatibility">
	    <span class="label"><?php echo $my_i18n['exe_style_labels']['compatibility'] . ': '; ?></span>
	    <span class="value"><?php 
        $compatibility = wp_get_object_terms($post->ID, 'exe_style_compatibility');
        $compatibility = array_shift($compatibility);
        
        echo '<a href="' . get_term_link($compatibility) . $lang_link . '">' . $compatibility->name . '</a>';
		  ?></span>
		</div>
    
    <?php // Optional
		$download_zip_stored_meta = (int) get_post_meta($post->ID, '_exe_style_download_zip_meta_key', TRUE);
		$download = get_post($download_zip_stored_meta);
		if (!empty($download)) { ?>
		<div class="exe-style exe-style-download-zip">
		  <span class="label"><?php echo $my_i18n['exe_style_labels']['download'] . ': ' ;?></span>
			<span class="value"><a href="<?php echo $download->guid; ?>" class="zip"><?php echo $download->post_title; ?></a></span>
		</div>
    <?php } ?>
		
	  <?php // Optional
    $tags = wp_get_object_terms($post->ID, 'exe_style_tag');
    if (!empty($tags)) {?>
	  <div class="exe-style exe-style-tags">
	    <span class="label"><?php echo $my_i18n['exe_style_labels']['tags'] . ': '; ?></span>
	    <ul class="value"><?php
		    foreach ($tags as $tag) {
          // Dynamic content, it is posible to add new terms that do not exist
          // on the language file. If a translated value exists, use it, otherwise
          // use term name.
          $tag_value = (!empty($my_i18n["exe_style_tags"][$tag->slug]))
          ? $my_i18n["exe_style_tags"][$tag->slug] : $tag->name;
          
		      echo '<li><a href="' . get_term_link($tag) . $lang_link . '">' . $tag_value . '</a></li>';
		    }
		  ?></ul>
	  </div>
	  <?php } ?>
	  
	  <?php // Optional
    $colours = wp_get_object_terms($post->ID, 'exe_style_colour');
    if (!empty($colours)) {?>
	  <div class="exe-style exe-style-colours">
	    <span class="label"><?php echo $my_i18n['exe_style_labels']['colours'] . ': '; ?></span>
	    <ul class="value"><?php
		    foreach ($colours as $colour) {
          // Dynamic content, it is posible to add new terms that do not exist
          // on the language file. If a translated value exists, use it, otherwise
          // use term name.
          $colour_value = (!empty($my_i18n["exe_style_colours"][$colour->slug]))
          ? $my_i18n["exe_style_colours"][$colour->slug] : $colour->name;
          
		      echo '<li><a href="' . get_term_link($colour) . $lang_link . '">' . $colour_value . '</a></li>';
		    }
			?></ul>
		</div>
    <?php } ?>
	</div><!-- .entry-meta -->
</div>