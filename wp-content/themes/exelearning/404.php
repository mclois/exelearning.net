<?php
	get_header(); 
	global $my_i18n;
?>
	<div id="primary">
			<div id="post-0" class="post error404 not-found">
				<div class="entry-header">
					<h1 class="entry-title"><?php echo $my_i18n['pageNotFound']; ?></h1>
				</div>
				<div class="entry-content">
					<p><?php echo $my_i18n['pageNotFoundText']; ?></p>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>