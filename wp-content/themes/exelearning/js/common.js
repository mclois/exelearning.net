var _bodyClassName;

function loadScript(url, callback){

    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                if (callback) callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            if (callback) callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function externalLink(e,themePath){
	if ((e.getElementsByTagName("img").length==0) && (e.className!='text') && (e.className!='twitter-link')) {
		e.innerHTML+="<img src='"+themePath+"/images/exelearning/icons/external_link.gif' alt='"+my_i18n.newWindow+"' width='13' height='10' class='external-icon' />";
	}
	e.className+=' external-link';
	e.rel="external";
	e.onclick=function(){ window.open(this.href);return false; };
	var t = e.title;
	if (t) e.title+=" ("+my_i18n.newWindow+")";
	else e.title=my_i18n.newWindow;
}

function displayTab(tabId,td){
	if (!td) var td = document.getElementById("tabs-desc");
	var divs = td.getElementsByTagName("DIV");
	for (i=0;i<divs.length;i++) {
		if (divs[i].className=="tab") {
			divs[i].style.display="none";
		}
	}
	var lis = document.getElementById("tabs").getElementsByTagName("LI");
	var i = lis.length;
	while (i--) lis[i].className="";
	
	document.getElementById(tabId).style.display="block";
	document.getElementById(tabId.replace("tab","trigger-")).className="current-tab";
	window.location.hash=tabId;
}

var contacto = {
	init : function() {
		fsaF = document.getElementById('contactForm');
		if(fsaF){
			if (typeof document.body.style.maxHeight == "undefined") var oldBrowser=true;
			fsaF.onsubmit = function(){
				contacto.validar(this);
				return false;
			}
			a = document.getElementById('your-name');
			if (!oldBrowser) a.onfocus=function(){ contacto.resetStatus(this); }
			b = document.getElementById('your-email');
			if (!oldBrowser) b.onfocus=function(){ contacto.resetStatus(this); }		
			c = document.getElementById('imagen');
			if (!oldBrowser) c.onfocus=function(){ contacto.resetStatus(this); }
		}	

        // Testers
        var osF = document.getElementById('os');
        if (osF) {
            var t = osF.defaultValue;
            contacto.placeholder = t;
            if (!jQuery.browser.msie) {
                osF.value = "";
                osF.placeholder = t;
            } else {
                osF.onclick = function(){
                    if (this.value==contacto.placeholder) this.value = "";
                }
                osF.onblur = function(){
                    if (this.value=="") this.value = contacto.placeholder;
                }
            }
        }
        
	},
	validar : function(e){
		var error = false;
		if (a.value=='') {
			a.className='error';
			error = true;
		} else a.className='';
		if (b.value=='') {
			b.className='error';
			error = true;
		} else b.className='';
		if (c.value=='') {
			c.className='error';
			error = true;
		} else c.className='';	
		if (error) jQuery("#errors").fadeIn();
		else fsaF.submit();
	},
	resetStatus : function(e) {
		e.className = '';
		jQuery("#errors").fadeOut();
	}
}

function setupTabs(tabs,td,tabId) { //#tabs, #tabs-description, selected-id
	var divs = td.getElementsByTagName("DIV");
	var c = 1;
	var nav = "";
	for (i=0;i<divs.length;i++) {
		if (divs[i].id.indexOf("tab")!=-1) {
			divs[i].style.display="none";
			var t = divs[i].getElementsByTagName("H2")[0].innerHTML;
			nav+="<li id='trigger-"+c+"'><a href='#tab"+c+"' class='tab"+c+"' onclick='displayTab(this.className);return false'>"+t+"</a></li>";
			c=c+1;
		}
	}
	if (nav!="") {
		tabs.innerHTML = "<ul>"+nav+"</ul>";
	}
	displayTab(tabId,td);	
}

//bbpress-threaded-replies.js
//We replace the function to remove and add TinyMCE to keep it working when moving the editor.
addReply = {
	moveForm : function(commId, parentId, respondId, postId) {
	
		alert("This is common");
	
		tinyMCE.execCommand('mceRemoveControl',false,'bbp_reply_content');
		
		var t = this, div, comm = t.I(commId), respond = t.I(respondId), cancel = t.I('cancel-in-reply-to-link'), parent = t.I('inreplyto'), post = t.I('bbp_topic_id');

		t.respondId = respondId;
		postId = postId || false;

		if ( ! t.I('wp-temp-form-div') ) {
			div = document.createElement('div');
			div.id = 'wp-temp-form-div';
			div.style.display = 'none';
			respond.parentNode.insertBefore(div, respond);
		}

		comm.parentNode.insertBefore(respond, comm.nextSibling);
		if ( post && postId )
			post.value = postId;
		parent.value = parentId;
		cancel.style.display = '';

		cancel.onclick = function() {
			var t = addReply, temp = t.I('wp-temp-form-div'), respond = t.I(t.respondId);

			if ( ! temp || ! respond )
				return;

			t.I('inreplyto').value = '0';
			temp.parentNode.insertBefore(respond, temp);
			temp.parentNode.removeChild(temp);
			this.style.display = 'none';
			this.onclick = null;
			return false;
		}
		
		tinyMCE.execCommand('mceAddControl',false,'bbp_reply_content');

		try { t.I('reply').focus(); }
		catch(e) {}

		return false;
	},

	I : function(e) {
		return document.getElementById(e);
	}
}

function domIsLoaded(blogURL,themePath,lang) {	

	_bodyClassName = document.body.className;
	
	var twitter = document.getElementById("twitter-link");
	if (twitter) {
		var h = twitter.innerHTML;
		var n = '<a href="https://twitter.com/exelearning_sp" class="twitter-follow-button" data-show-count="false" data-lang="es" data-show-screen-name="false">Seguir a @exelearning_sp</a>';
		if (_bodyClassName.indexOf(" lang-en")!=-1) n = '<a href="https://twitter.com/exelearning_sp" class="twitter-follow-button" data-show-count="false" data-lang="en" data-show-screen-name="false">Follow @exelearning_sp</a>';
		twitter.innerHTML=n+h;
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	}
	
	var s = document.getElementById("s");
	if (s) {
		s.placeholder = s.title;
		s.title="";
	}
	
	var c = document.getElementById("content");
	var as = c.getElementsByTagName("A");
	var i = as.length;
	while (i--) {
		var server = "exelearning.net";
		if (window.location.host=="localhost") server = "localhost";
		if (as[i].href.indexOf("http://"+server+"/")!=0 && as[i].href.indexOf("https://"+server+"/")!=0) externalLink(as[i],themePath);	
		else if (lang!="es" && _bodyClassName.indexOf(" bbPress ")!=-1) {
			if (as[i].href.indexOf("?")==-1) as[i].href+="?lang="+lang;
			else if (as[i].href.indexOf("lang="+lang)==-1) as[i].href+="&lang="+lang;
		}
	}

	var t = document.getElementById("tabs");
	if (t) {
		var td = document.getElementById("tabs-desc");
		if (td) {
			var tabId = "tab1";
			if (window.location.href.indexOf("#")!=-1) {
				var id = window.location.href.split("#")[1];
				if (document.getElementById(id)) tabId = id;
			}
			setupTabs(t,td, tabId);
		}
	}
	
	var tC = document.getElementById("tag-cloud");
	if (tC) {
		var c = tC.className.replace("tag-","tag-link-");
		var lis = tC.getElementsByTagName("A");
		var i = lis.length;
		while (i--) {
			if (lis[i].className==c) {
				lis[i].className+=" current-tag";
			}
		}
	}	
	
    if (_bodyClassName.indexOf("single-topic ")!=-1) {
        var iT = jQuery("#site-desc").text();
            if (iT && iT!='') {
            iT = iT.replace("eXeLearning.net ","");
            var iTL = iT.length;
            iT = iT.substr(0,iTL-1);
        
            var fT = jQuery("A","#nav").eq(2).text();
            if(fT && fT!="") {
                var as = jQuery("A",".bbp-breadcrumb");
                as.eq(0).text(iT);
                as.eq(1).text(fT);
            }
            
        }
    }  
    
	var a = document.getElementById("add-this");
	if (a) {
		var nH = '<span class="addthis_toolbox addthis_default_style "><a class="addthis_button_preferred_1"></a> <a class="addthis_button_preferred_2"></a> <a class="addthis_button_preferred_3"></a> <a class="addthis_button_preferred_4"></a> <a class="addthis_button_compact"></a> <a class="addthis_counter addthis_bubble_style"></a></span>';
		a.innerHTML += nH;
	}
	loadScript("http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f91220819af1143");

}