<?php
	$my_i18n = array(
		//"blogDesc" => "La evolución de eXeLearning",
		"blogDesc" => "El nou eXeLearning",
		"blogTitle" => "El nou eXeLearning",
		"tag" => "Etiqueta",
		"tags" => "Etiquetes",
		"index" => "Índex",
		"contact" => "Contacte",
		"siteMap" => "Mapa web",
		"download" => "Descarrega",
		"downloads" => "Descarregues",
		"otherDownloads" => "Altres descarregues",
		"author" => "Autor",
		"subcategories" => "Subapartats",
		"searchIn" => "Cerca en",
		"chooseOne" => "Escull un",
		"forums" => "Fòrums",
		"forumsTopics" => "Fòrums (Temes)",
		"forumsReplies" => "Fòrums (Respostes)",
		"search" => "Cerca",
		"skipToContent" => "Ves al contingut",
		"readMore" => "Més",
		"postedOn" => "Publicat el",
		"newerPosts" => "Entrades més noves",
		"olderPosts" => "Entrades més velles",
		"pageNotFound" => "Pàgina no trobada",
		"pageNotFoundText" => "Ho sentim, però no podem trobar el que esteu cercant. Si us plau, utilitza el menú o el cercador.",
		"searchResultsFor" => "Resultats de la cerca per",
		"noSearchResults" => "No hi ha resultats per a aquesta cerca, Si us plau intenteu-ho amb altres paraules.",
		"noArticles" => "No hi ha cap contingut en aquest apartat. Disculpeu les molèsties.",
		"followUsViaRSS" => "Segueix-nos per RSS ",
		"youCanAlsoFollowUsOnTwitter" => "També pots seguir-nos per Twitter",
		"exeLearningOnTwitter" => "@exelearning_sp en Twitter",
		"chooseYourOS" => "Descarrega directa de les darreres versions",
        "previousVersions" => "Versions anteriors",
        "sourceCode" => "Codi font",
		"installVersion" => "Versió instal·lable ",
		"portableVersion" => "Versió portable",
		//Navigation menu:
		"navHome" => "Inici",
		"navDownloads" => "Descarregues",
		"navForums" => "Fòrums",
		"navNews" => "Actualitat",
		"navFeatures" => "Característiques",
		"navDocumentation" => "Documentació",
		"navDevelopers" => "Desenvolupadors",		
		//Login (header):
		"administrationLink" => "Administració",
		"closeSessionLink" => "Tanca sessió",
		"createAccountLink" => "Crea el teu compte",
		"loginLink" => "Inicia sessió",
		//Pagination:
		"previous" => "Enrere",
		"next" => "Següent",
		//Contact form:
		"cf_name" => "Nom",
		"cf_eMail" => "Correu electrònic",
		"cf_subject" => "Assumpte",
		"cf_comments" => "Comentaris",
		"cf_captchaText" => "Introdueix el text de la imatge",
		"cf_captchaImgAlt" => "Imatge per al camp formulari",
		"cf_captchaWarning" => "Si us plau, si no podeu veure la imatge bé, contactau amb nosaltres mitjançant el fòrum.",
		"cf_leaveThisFieldEmpty" => "Deixa aquest camp buit",
		"cf_emptyFieldTitle" => "No escriviu res en aquest camp",
		"cf_send" => "Envia",
		"cf_requiredFields" => "Camps obligatoris",
		"cf_requiredFieldsList" => "Nom, correu electrònic, text de la imatge",
		//Contact form errors:
		"cf_e_tellUsYourName" => "Escriu el teu nom.",
		"cf_e_giveUsAnEmail" => "Deseu-nos  un correu electrònic per si ens cal posar-nos en contacte amb vostè",
		"cf_e_mismatch" => "El text introduït no és correspon a la imatge.",
		"cf_e_controlFieldWarning" => "Deixa buit el darrer camp (es sols un camp de control).",
		"cf_e_checkEmailSpell" => "Si us plau, comproveu com heu escrit el vostre correu electrònic.",
		"cf_e_wrongEmail" => "L'adreça de correu electrònic no sembla correcta.",
		"cf_e_errorIntro" => "Si us plau, empleneu bé el vostre formulari:",
		"cf_e_back" => "Torna al formulari",
		//Contact form sent:
		"cf_s_message" => "Moltes gràcies per contactar amb nosaltres.",
		"cf_s_error_message" => "No s'ha pogut enviar el missatge. Si us plau, intenteu-ho més tard.",
        //Developers:
        "theTeam" => "L’equip",
        "developersIntro" => "El nou eXeLearning es manté gràcies a un equip de treballadors i voluntaris que creuen en esta ferramenta d’autor de codi obert i volen ajudar els docents en la creació i publicació de continguts educatius.",
        "testersIntro" => "Persones que, voluntària i desinteressadament, proven la ferramenta i ajuden a millorar-la i detectar fallades.",
        "testers" => "Provadors",
        "doYouWantToTest" => "Vols ser provador?",
        "doYouWantToTestIntro" => "La vostra ajuda és fonamental per a detectar fallades i millorar la ferramenta. T’animes a participar?",
        "institutionOrCompany" => "Organisme/Empresa",
        "operativeSystems" => "S.O./Navegadors",
        "operativeSystemsExplanation" => "Dis-nos en quin sistema o sistemes operatius podries provar, incloent-hi la seua versió, i quin navegador o navegadors usaries en cadascun.",
        "thankYou" => "Gràcies!",
        "testers_s_message" => "Abans de llançar la pròxima versió ens posarem en contacte amb tu perquè pugues fer proves.",
        "participate" => "Participa",
        "participateIntro" => "Pots participar provant, desenvolupant, traduint o simplement aportant idees.",
        "coordinator" => "Coordinador",
        "oldCoordinator" => "Antic coordinador",
        "developers" => "Desenvolupadors",
        "collaborators" => "Col·laboradors",
        "translationTeam" => "Equip de traducció",
        "developerSpace" => "Espais per a desenvolupadors",
        "developmentForum" => "Fòrum de desenvolupament",
        "eXeForge" => "Forja de desenvolupament del nou eXeLearning",
        "workGroups" => "Grups de treball",
        "workGroupsForum" => "Fòrum de grups de treball",
	    // eXe Styles
	    "exe_style_labels" => array(
	        "version" => "Versió",
	        "author" => "Autor",
	        "license" => "Llicència",
	        "compatibility" => "Compatible amb eXe",
	        "download" => "Descarregar arxiu ZIP",
	        "tags" => "Etiquetes",
	        "colours" => "Colors predominants",
	        "taxonomy_list_license" => "Altres estils amb llicència %s",
	        "taxonomy_list_compatibility" => "Altres estils compatibles amb %s",
	        "taxonomy_list_tag" => "Altres estils etiquetats amb %s",
	        "taxonomy_list_colour" => "Altres estils amb color %s",
	    ),
	    "exe_style_tags" => array(
	        "adaptive" => "Adaptatiu",
	        "mobile" => "Mòbil",
	        "fluid-width" => "Ample fluid",
	        "fixed-width" => "Ample fix",
	        "high-contrast" => "Alt contrast",
	    ),
	    "exe_style_colours" => array(
	        "red" => "Vermell",
	        "orange" => "Taronja",
	        "yellow" => "Groc",
	        "green" => "Verd",
	        "blue" => "Blau",
	        "violet" => "Violeta",
	        "black" => "Negre",
	        "grey" => "Gris",
	        "white" => "Blanc",
	        "brown" => "Marró",
	    ),
	    // Send your style contact form
	    "send_your_style" => "Envia el teu estil",
	);
?>