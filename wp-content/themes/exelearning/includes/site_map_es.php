<div class="lang es">
	<h2>Español</h2>
	<dl>
		<dt><a href="<?php echo $blogURL; ?>/">Inicio</a></dt>
		<dd>Portada y últimas novedades.</dd>
		<dt><a href="<?php echo $blogURL; ?>/descargas/">Descargas</a></dt>
		<dd>
			Últimas versiones. También:
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=16&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1'); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/forums/">Foros</a></dt>
		<dd>Los foros son comunes a todos los idiomas.
			<ul>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/">Abierto</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/">Desarrolladores</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/">Grupos de trabajo</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/">Ayuda</a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/actualidad/">Actualidad</a></dt>
		<dd>Todo sobre el nuevo eXeLearning.</dd>
		<dt><a href="<?php echo $blogURL; ?>/caracteristicas/">Características</a></dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/caracteristicas/#tab1">Características</a></li>
				<li><a href="<?php echo $blogURL; ?>/caracteristicas/#tab2">Hoja de ruta</a></li>
				<li><a href="<?php echo $blogURL; ?>/caracteristicas/#tab3"><em>Changelog</em></a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/documentacion/">Documentación</a></dt>
		<dd>
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=9&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1&show_option_none='.$my_i18n['noArticles']); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/desarrolladores/">Desarrolladores</a></dt>
		<dd>¡Infórmate y participa en el proyecto!</dd>
		<dt>Otros apartados</dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/contacto/">Contacto</a></li>
				<li><a href="<?php echo $blogURL; ?>/rss-es/">RSS</a></li>
			</ul>
		</dd>		
	</dl>				
</div><!-- /es -->