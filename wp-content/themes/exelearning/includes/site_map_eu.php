<div class="lang eu">
	<h2>Euskara</h2>
	<dl>
		<dt><a href="<?php echo $blogURL; ?>/?lang=eu">Hasiera</a></dt>
		<dd>Lehen orrialdea eta azken berriak.</dd>
		<dt><a href="<?php echo $blogURL; ?>/jaitsi/">Jaitsi</a></dt>
		<dd>
			Azken bertsioak eta gainera:
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=42&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1'); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/forums/?lang=eu">Foroak</a></dt>
		<dd>Hizkuntza guztietarako foro berak erabiliko dira.
			<ul>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/?lang=eu">Eztabaida</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/?lang=eu">Garatzaileak</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/?lang=eu">Lan taldeak</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/?lang=eu">Laguntza</a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/berriak/">Berriak</a></dt>
		<dd>Dena eXeLearning berriaren inguruan.</dd>
		<dt><a href="<?php echo $blogURL; ?>/ezaugarriak/">Ezaugarriak</a></dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/ezaugarriak/#tab1">eXeLearning-aren ezaugarriak</a></li>
				<li><a href="<?php echo $blogURL; ?>/ezaugarriak/#tab2">Bidai orria</a></li>
				<li><a href="<?php echo $blogURL; ?>/ezaugarriak/#tab3"><em>Changelog</em></a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/dokumentazioa/">Dokumentazioa</a></dt>
		<dd>
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=43&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1&show_option_none='.$my_i18n['noArticles']); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/garatzaileak/">Garatzaileak</a></dt>
		<dd>Informatu eta sar zaitez proiektuan!</dd>
		<dt>Bestelako atalak</dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/contacto/?lang=eu">Harremanetarako</a></li>
				<li><a href="<?php echo $blogURL; ?>/rss-eu/">RSS</a></li>
			</ul>
		</dd>		
	</dl>				
</div><!-- /eu -->