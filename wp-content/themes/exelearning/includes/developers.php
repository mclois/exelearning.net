<?php global $my_i18n; ?>
<div id="primary" class='developers-page'>
    <h1 class="entry-title"><?php the_title(); ?></h1>
    <?php while ( have_posts() ) : the_post(); ?>
        <div id='tabs'></div>
        <div id='tabs-desc'>
            <div id='tab1' class='tab'>
                <h2><?php echo $my_i18n['theTeam']; ?></h2>
                <p class="intro"><?php echo $my_i18n['developersIntro']; ?></p>
                <?php 
                    //the_content(); 
                    //$content = get_the_content(); 
                    $page_id = 50;
                    $page_data = get_page( $page_id );
                    $content = $page_data->post_content;
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);		
                    $pieces = explode("<p>|</p>", $content);
                    $size = sizeof($pieces);
                    if ($size>1) {
                        function getPieceHTML($piece) {
                            global $lang, $my_i18n;
                            //$content = str_replace('Versión instalable', $my_i18n['installVersion'], $page_data->post_content);
                            if ($lang!="es") {
                                $piece = str_replace('">', '?lang='.$lang.'">', $piece);
                                $piece = str_replace('Antiguo coordinador', $my_i18n['oldCoordinator'], $piece);
                                $piece = str_replace('Coordinador', $my_i18n['coordinator'], $piece);
                                $piece = str_replace('Desarrolladores', $my_i18n['developers'], $piece);
                                $piece = str_replace('Equipo de traducción', $my_i18n['translationTeam'], $piece);
                                $piece = str_replace('Colaboradores', $my_i18n['collaborators'], $piece);
                                $piece = str_replace('Espacios para desarrolladores', $my_i18n['developerSpace'], $piece);
                                $piece = str_replace('Foro de desarrollo', $my_i18n['developmentForum'], $piece);
                                $piece = str_replace('Forja de desarrollo del nuevo eXeLearning', $my_i18n['eXeForge'], $piece);
                                $piece = str_replace('Grupos de trabajo', $my_i18n['workGroups'], $piece);
                                $piece = str_replace('Foro de grupos de trabajo', $my_i18n['workGroupsForum'], $piece);
                            }
                            return "<div class='participants'>".$piece."</div>";
                        }
                        echo getPieceHTML($pieces[0]);
                    } else {
                       the_content(); 
                    }                
                ?>
                
            </div>
            <div id='tab2' class='tab'>
                <h2><?php echo $my_i18n['testers']; ?></h2>
                <?php if ($size>1) { ?>
                    <p class="intro"><?php echo $my_i18n['testersIntro']; ?></p>
                    <?php echo getPieceHTML($pieces[1]); ?>
                <?php } else { ?>
                    <p><?php echo $my_i18n['noArticles']; ?></p>
                <?php } ?>
            </div>
            <div id='tab3' class='tab'>
                <h2><?php echo $my_i18n['doYouWantToTest']; ?></h2>
                <?php if (!$_POST) { ?>
                <p class="intro"><?php echo $my_i18n['doYouWantToTestIntro']; ?></p>
                <div id="contact">
                    <form id="contactForm" action="<?php echo $_SERVER['REQUEST_URI']; ?>#tab3" method="post" class="autoclear">
                        <div class="personalData">
                            <div class="formField autoclear">
                                <label for="your-name"><span><?php echo $my_i18n['cf_name']; ?><b>*</b>: </span>
                                <input type="text" name="your-name" id="your-name" /></label>
                            </div>
                            <div class="formField autoclear">
                                <label for="your-email"><span><?php echo $my_i18n['cf_eMail']; ?><b>*</b>: </span>
                                <input type="text" name="your-email" id="your-email" /></label>
                            </div>
                            <div class="formField autoclear">
                                <label for="company"><span><?php echo $my_i18n['institutionOrCompany']; ?>: </span>
                                <input type="text" name="company" id="company" /></label>
                            </div>
                        </div>																	
                        <div class="comments">
                            <div class="formField autoclear textarea">
                            <label for="os"><span><?php echo $my_i18n['operativeSystems']; ?>: </span>
                            <textarea name="os" id="os" cols="32" rows="6"><?php echo $my_i18n['operativeSystemsExplanation']; ?></textarea></label>                        
                            </div>
                            <div class="formField autoclear textarea">
                            <label for="coment"><span><?php echo $my_i18n['cf_comments']; ?>: </span>
                            <textarea name="coment" id="coment" cols="32" rows="6"></textarea></label>
                            </div>
                            <div class="formField captcha"><?php echo $my_i18n['cf_captchaText']; ?><b>*</b>:<br />
                                <img src="<?php echo $themePath; ?>/images/exelearning/contact/check.php" alt="<?php echo $my_i18n['cf_captchaImgAlt']; ?>" width="120" height="30" /><br />
                                <input type="text" name="imagen" id="imagen" /><br />
                                <span class="info"><?php echo $my_i18n['cf_captchaWarning']; ?></span>
                            </div>			
                            <div class="formField scr-not-av">
                                <label for="anti-s"><span><?php echo $my_i18n['cf_leaveThisFieldEmpty']; ?> </span>
                                <input type="text" name="anti-s" id="anti-s" title="<?php echo $my_i18n['cf_emptyFieldTitle']; ?>" /></label>
                            </div>
                            <div class="formButtons autoclear">
                                <input type="submit" value="<?php echo $my_i18n['cf_send']; ?>" name="submit-form" id="submit-form" class="form-submit" /> 
                                <span id="errors"><strong><b>*</b><?php echo $my_i18n['cf_requiredFields']; ?>:</strong> <?php echo $my_i18n['cf_requiredFieldsList']; ?>.</span>
                            </div>    
                        </div>
                    </form>
                </div>
                <script type="text/javascript">contacto.init();</script>
                <?php 
                    } else {
                    
                        //SETTINGS.
                        $nombre =  $_POST["your-name"];
                        $email =  $_POST["your-email"];
                        $company =  $_POST["company"];
                        $os = $_POST['os'];
                        $comment = $_POST['coment'];
                        $image = $_POST['imagen'];
                        $antiS = $_POST["anti-s"];
                        
                        if (empty($nombre)) $error .= "<li>".$my_i18n['cf_e_tellUsYourName']."</li>";
                        if (empty($email)) $error .= "<li>".$my_i18n['cf_e_giveUsAnEmail']."</li>";
                        
                        //Captcha
                        session_start();
                        $session = $_SESSION['captcha']; //Define the session we set in image.php
                        $image = md5($image); //MD5 encrypt the post
                        if ($session != $image) $error .= "<li>".$my_i18n['cf_e_mismatch']."</li>";
                        
                        //Anti-SPAM
                        if (!empty($antiS)) $error .= "<li>".$my_i18n['cf_e_controlFieldWarning']."</li>";
                            
                        //Email
                        if($email) {
                            if(isset($_POST['email'])) {
                                if (preg_match('/^[-!#$%&\'*+\\.\/0-9=?A-Z^_`{|}~]+@([-0-9A-Z]+\.)+([0-9A-Z]){2,4}$/i', trim($email))) {
                                    //Do nothing
                                } else {
                                    $error .= "<li>".$my_i18n['cf_e_checkEmailSpell']."</li>";
                                }
                                //Check for valid domain name
                                $ok = true;
                                $ok = eregi( "^[_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3}$", $email,$check);
                                $ok = getmxrr(substr(strstr($check[0], '@'), 1), $dummy);
                                if($ok === false) {
                                    $host = substr($email, strpos($email, '@') + 1);
                                    if(gethostbyname($host) != $host) $ok = true;
                                    if ($ok != true) $error .= "<li>".$my_i18n['cf_e_wrongEmail']."</li>";
                                }
                            }
                        }
                        //Error
                        if($error) {
                            echo "<div class='errors'><p>".$my_i18n['cf_e_errorIntro']."</p><ul>";
                            echo $error;
                            $path='#URL_SITE_SPIP';
                            echo "</ul><p>&laquo; <a href='".$blogURL."/contacto/".$lang_link."'>".$my_i18n['cf_e_back']."</a></p></div>";
                        }
                        else {
                            $destination = "ismail.ali@ite.educacion.es,antonio.monje@ite.educacion.es,antoniomonjef@gmail.com";
                            $destinationName = "eXeLearning.net";
                            $subject = "Formulario desde eXeLearning.net (probadores)";
                            $contentType = "text/plain"; //text/html or text/plain
                            $charset = "utf-8"; //iso-8859-1 or UTF-8
                            
                            $headers  = 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: '.$contentType.'; charset='.$charset.'' . "\r\n";
                            $headers .= 'From: '.$nombre.' <'.$email.'>' . "\r\n";
                            $headers .= 'To: '.$destinationName.' <'.$destination.'>' . "\r\n";
                            
                            $body .= "Nombre: ".$nombre."\n";
                            $body .= "E-mail: ".$email."\n";
                            $body .= "Organismo/Empresa: ".$company."\n";
                            $body .= "Sistema(s) operativo(s) / Navegador(es):\n\n".$os."\n\n";
                            $body .= "Comentarios:\n\n".$comment."\n\n";
                            if ($contentType=="text/html") $body = str_replace("\n", "<br />", $body);
                            
                            if (function_exists('mail')) {
                                try {
                                    mail($destination,$subject,$body,$headers);
                                    echo "<p><strong>".$my_i18n['thankYou']."</strong></p>";
                                    echo "<p>".$my_i18n['testers_s_message']."</p>";
                                } catch(Exception $e) {
                                    echo "<p>".$my_i18n['cf_s_error_message']."</p>";
                                }
                            } else {
                                echo "<p>".$my_i18n['cf_s_error_message']."</p>";
                            }
                            
                        }			
                    } 
                ?>                
            </div>
            <?php if ($size>2) { ?>
            <div id='tab4' class='tab'>
                <h2><?php echo $my_i18n['participate']; ?></h2>
                <p class="intro"><?php echo $my_i18n['participateIntro']; ?></p>
                <?php echo getPieceHTML($pieces[2]); ?>            
            </div>
            <?php } ?>
        </div><!-- #tabs-desc -->        
        
    <?php endwhile; ?>
</div><!-- #primary -->
<?php get_sidebar(); ?>