<?php global $lang; ?>
<div id="primary" class="site-map">
	<h1><?php echo $my_i18n['siteMap']; ?></h1>
	<div id="site-map" class="autoclear">
		<div class="autoclear">
			<?php 
				if ($lang=='eu') {
					include("site_map_eu.php");
					include("site_map_es.php");
                    include("site_map_ca.php");
                    include("site_map_gl.php");
					include("site_map_en.php");
				} else if ($lang=='ca') {
					include("site_map_ca.php");
                    include("site_map_es.php");
                    include("site_map_eu.php");
                    include("site_map_gl.php");                    					
                    include("site_map_en.php");
				} else if ($lang=='gl') {
					include("site_map_gl.php");                    					
                    include("site_map_es.php");
                    include("site_map_ca.php");
                    include("site_map_eu.php");
                    include("site_map_en.php");
				} else if ($lang=='en') {
					include("site_map_en.php");
					include("site_map_es.php");
                    include("site_map_ca.php");
                    include("site_map_eu.php");
                    include("site_map_gl.php");                    					
				} else {
					include("site_map_es.php");
                    include("site_map_ca.php");
                    include("site_map_eu.php");
                    include("site_map_gl.php");
					include("site_map_en.php");
				}
			?>
		</div>
	</div>	
</div>