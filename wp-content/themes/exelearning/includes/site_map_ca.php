<div class="lang ca">
	<h2>Català</h2>
	<dl>
		<dt><a href="<?php echo $blogURL; ?>/?lang=ca">Inici</a></dt>
		<dd>Portada i últimes novetats.</dd>
		<dt><a href="<?php echo $blogURL; ?>/descarregues/">Descàrregues</a></dt>
		<dd>
			Últimes versions. També:
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=260&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1'); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/forums/?lang=ca">Fòrums</a></dt>
		<dd>Els fòrums són comuns a tots els idiomes.
			<ul>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/?lang=ca">Discussió</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/?lang=ca">Desenvolupadors</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/?lang=ca">Grups de treball</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/?lang=ca">Ajuda</a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/actualitat/">Actualitat</a></dt>
		<dd>Tot sobre el nou eXeLearning.</dd>
		<dt><a href="<?php echo $blogURL; ?>/caracteristiques/">Característiques</a></dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/caracteristiques/#tab1">Característiques d'eXeLearning</a></li>
				<li><a href="<?php echo $blogURL; ?>/caracteristiques/#tab2">Full de ruta</a></li>
				<li><a href="<?php echo $blogURL; ?>/caracteristiques/#tab3"><em>Changelog</em></a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/documentacio/">Documentació</a></dt>
		<dd>
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=257&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1&show_option_none='.$my_i18n['noArticles']); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/desenvolupadors/">Desenvolupadors</a></dt>
		<dd>Informa't i participa en el projecte!</dd>
		<dt>Altres apartats</dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/contacto/?lang=ca">Contacte</a></li>
				<li><a href="<?php echo $blogURL; ?>/rss-ca/">RSS</a></li>
			</ul>
		</dd>		
	</dl>				
</div><!-- /ca -->