<div class="lang gl">
	<h2>Galego</h2>
	<dl>
		<dt><a href="<?php echo $blogURL; ?>/?lang=gl">Inicio</a></dt>
		<dd>Portada e última novidades.</dd>
		<dt><a href="<?php echo $blogURL; ?>/descargas-gl/">Descargas</a></dt>
		<dd>
			Últimas versións. Tamén:
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=263&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1'); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/forums/?lang=gl">Foros</a></dt>
		<dd>Os foros son comúns a tódolos idiomas.
			<ul>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/abierto/?lang=gl">Discusión</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/desarrollo/?lang=gl">Desenvolvedores</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/grupos/?lang=gl">Grupos de traballo</a></li>
				<li><a href="<?php echo $blogURL; ?>/forums/forum/ayuda/?lang=gl">Axuda</a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/actualidade/">Actualidade</a></dt>
		<dd>Todo sobre o novo eXeLearning.</dd>
		<dt><a href="<?php echo $blogURL; ?>/caracteristicas-gl/">Características</a></dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/caracteristicas-gl/#tab1">Características de eXeLearning</a></li>
				<li><a href="<?php echo $blogURL; ?>/caracteristicas-gl/#tab2">Folla de ruta</a></li>
				<li><a href="<?php echo $blogURL; ?>/caracteristicas-gl/#tab3"><em>Changelog</em></a></li>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/documentacion-gl/">Documentación</a></dt>
		<dd>
			<ul>
				<?php wp_list_categories('hide_empty=0&child_of=258&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=1&show_option_none='.$my_i18n['noArticles']); ?>
			</ul>
		</dd>
		<dt><a href="<?php echo $blogURL; ?>/desenvolvedores/">Desenvolvedores</a></dt>
		<dd>Infórmate e participa no proxecto!</dd>
		<dt>Outros apartados</dt>
		<dd>
			<ul>
				<li><a href="<?php echo $blogURL; ?>/contacto/?lang=gl">Contacto</a></li>
				<li><a href="<?php echo $blogURL; ?>/rss-gl/">RSS</a></li>
			</ul>
		</dd>		
	</dl>				
</div><!-- /gl -->