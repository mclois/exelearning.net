<?php global $blogURL, $themePath, $isForum, $isContact, $isSiteMap, $my_i18n, $lang, $lang_link; ?>

		</div><!-- #content -->
	</div><!-- #content-wrapper -->

	<div id="footer">
		<ul>
			<li><a href="http://educalab.es/intef"><img src="<?php echo $themePath; ?>/images/exelearning/footer/intef.png" alt="INTEF" width="125" height="40" /></a></li>
			<li><a href="http://cedec.ite.educacion.es/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/cedec.png" alt="CEDEC" width="109" height="40" /></a></li>
			
			<li><a href="http://www.ulhi.hezkuntza.net/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/ulhi.png" alt="ULHI" width="164" height="40" /></a></li>
			<li><a href="http://www.tknika.net/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/tknika.png" alt="TKNIKA" width="121" height="40" /></a></li>
			<li class="epa"><a href="http://www.juntadeandalucia.es/educacion"><img src="<?php echo $themePath; ?>/images/exelearning/footer/epa.png" alt="Consejería de Educación, Cultura y Deporte (Junta de Andalucía)" width="173" height="50" /></a></li>
			<li><a href="http://www.edu.xunta.es/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/cceou.png" alt="Consellería de Cultura, Educación e Ordenación Universitaria (Xunta de Galicia)" width="215" height="40" /></a></li>            
            <li class="amtega"><a href="http://imit.xunta.es/portal/sxmit/AMTEGA/presentacion_amtega.html"><img src="<?php echo $themePath; ?>/images/exelearning/footer/amtega.png" alt="Axencia para a Modernización Tecnolóxica de Galicia (AMTEGA)" width="103" height="40" /></a></li>            
			<li class="carm"><a href="http://www.educarm.es/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/carm.png" alt="Consejer&iacute;a de Educaci&oacute;n, Formaci&oacute;n y Empleo de la Regi&oacute;n de Murcia" width="96" height="40" /></a></li>
			<li class="em"><a href="http://www.educa.madrid.org/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/educamadrid.png" alt="EducaMadrid Plataforma Tecnol&oacute;gica" width="126" height="35" /></a></li>
            <li class="op"><a href="http://www.open-phoenix.com/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/open_phoenix.png" alt="Open Phoenix" width="153" height="30" /></a></li>
            <li class="tt"><a href="http://www.toughra.com/"><img src="<?php echo $themePath; ?>/images/exelearning/footer/toughra.png" alt="Toughra Technologies" width="153" height="32" /></a></li>
		</ul>
		<p><strong><?php echo get_bloginfo( 'name' ); ?></strong>, <?php echo date('Y'); ?> - <a href="<?php echo $blogURL; ?>/sitemap/<?php echo $lang_link; ?>"<?php if ($isSiteMap) echo ' class="current"'; ?>><?php echo $my_i18n['siteMap']; ?></a> - <a href="<?php echo $blogURL; ?>/contacto/<?php echo $lang_link; ?>"<?php if ($isContact) echo ' class="current"'; ?>><?php echo $my_i18n['contact']; ?></a></p>
	</div><!-- #footer -->
</div><!-- #page -->
<?php if ($isForum && is_user_logged_in()) { ?>
<script type="text/javascript" src="<?php echo $themePath; ?>/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "fullscreen",
        valid_elements : "a[rel|rev|hreflang|name|href|target|title],strong/b,em/i,strike,u,p[style],-ol[type|compact],-ul[type|compact],-li,br,-sub,-sup,-blockquote,span[style],-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],div[style]",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,bullist,numlist,|,outdent,indent,blockquote,|,sub,sup,|,undo,redo,|,link,unlink,code,|,removeformat,|,fullscreen",
        theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo $themePath; ?>/css/tiny_mce.css"        
});
</script>
<?php } ?><?php wp_footer(); ?><script type="text/javascript">domIsLoaded('<?php echo $blogURL; ?>','<?php echo bloginfo("template_url"); ?>','<?php echo $lang; ?>')</script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59382457-1', 'auto');
  ga('send', 'pageview');

</script>



</body>
</html>