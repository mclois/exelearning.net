<?php 
	global $isDescargasEU, $isDescargasCA, $isDescargasGL, $isDownloads, $lang, $isContact, $isSiteMap, $my_i18n, $blogURL; 
	if (is_page(221) || is_page(223) || is_page(601) || is_page(1616) || is_page(1618)) $isDownloadsPage = true;
    else if (is_page(50) || is_page(1626) || is_page(603) || is_page(1628) || is_page(52)) $isDevelopersPage = true;
?>
<?php get_header(); ?>
	<?php 
		if ($isContact) {
			include("includes/contact_form.php");
		} else if ($isSiteMap) {
			include("includes/site_map.php");
		} else if ($isDevelopersPage) {
            include("includes/developers.php");
        } else {            
	?>
		<div id="primary"<?php if ($isDownloadsPage) echo " class='downloads-page'"; ?>>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php //comments_template( '', true ); ?>
			<?php endwhile; ?>
		</div><!-- #primary -->
		<?php 
			if ($isDownloadsPage) {
				echo "<div id='secondary' class='other-downloads'>";
					$default = 16;
					if ($isDownloads) $default = 17;
					else if ($isDescargasEU) $default = 42;
                    else if ($isDescargasCA) $default = 260;
                    else if ($isDescargasGL) $default = 263;
					$cur_cat_id= get_cat_id(single_cat_title( '', false ));		
					echo '<div class="sidebar-block"><h2 class="widget-title" id="d-t">'.$my_i18n["otherDownloads"].'</h2>';
					echo "<ul>";
					wp_list_categories('hide_empty=0&child_of='.$default.'&hierarchical=1&use_desc_for_title=0&orderby=name&title_li=&depth=3&show_option_none='.$my_i18n['noArticles'].'&current_category='.$cur_cat_id);
					echo "</ul></div>";
				echo "</div>";
			} else {
				get_sidebar();
			}
		?>
	<?php } //!$isContact ?>
<?php get_footer(); ?>