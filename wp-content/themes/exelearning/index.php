<?php global $blogURL, $themePath; ?>
<?php get_header(); ?>
	<div id="primary" class="autoclear">
		<?php if ($lang=="en") { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> is an Open Source authoring application to assist teachers and academics in the publishing of web content.</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> Resources authored in <strong>eXe</strong> can be exported in IMS Content Package, SCORM 1.2, or IMS Common Cartridge formats or as simple self-contained web pages.</li>
			</ul>
		</div>
        <div id="col-b">
            <p>A few months ago, we announced a new release of <strong>eXeLearning</strong>.</p>
			<p>Now, we announce the launch of a website dedicated to the <strong>new eXeLearning</strong>.</p>
			<p class="logo"><a href="http://exelearning.net/downloads/"><img src="<?php echo $themePath; ?>/images/exelearning/home/exe_2.png" width="207" height="195" alt="eXeLearning logo" /></a></p>
			<p>Different institutions collaborate in the <a href="<?php echo $blogURL; ?>/developers/">development process</a> of the new eXeLearning. Also people who kindly <a href="<?php echo $blogURL; ?>/downloads/">test the application</a>, <a href="<?php echo $blogURL; ?>/forums/?lang=en">debate</a> and share. </p>
			<p><strong>eXeLearning</strong> is Open Source. All the educational community can participate in its development. </p>
			<p>You are all welcome to <a href="<?php echo $blogURL; ?>/login/?lang=en">participate</a>.</p>
		</div>        
        <?php } else if ($lang=="eu") { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> Irakasleentzat web edukiak sortu eta argitaratzeko laguntza handiko kode irekian garatutako egiletasun tresna da.</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> <strong>eXe</strong>rekin garatutako edukiak hainbat formatutan gorde daitezke: IMS, SCORM 1.2 … Baita web orri nabigagarri moduan ere.</li>
			</ul>
		</div>
		<div id="col-b">
			<p>Orain dela hilabete batzuk,<strong>eXeLearning</strong>aren bertsio berriaren berri eman genuen.</p>
			<p>Gaur, <strong>eXeLearning berriari</strong> buruz dihardun gune honen martxa jartzearen berri ematen dugu.</p>
			<p class="logo"><a href="http://exelearning.net/jaitsi/"><img src="<?php echo $themePath; ?>/images/exelearning/home/exe_2.png" width="207" height="195" alt="eXeLearningaren logoa" /></a></p>
			<p><a href="<?php echo $blogURL; ?>/garatzaileak/">Garapen</a> honetan, erakunde desberdinek kolaboratzen dute, baita elkarlanarekiko joera duten hainbat pertsonek, <a href="<?php echo $blogURL; ?>/jaitsi/">tresna probatu</a>, <a href="<?php echo $blogURL; ?>/forums/?lang=eu">eztabaidatu</a> eta partekatzen dute.</p>
			<p><strong>eXeLearning</strong> software askea da. Parte hartzea, hezkuntza komunitate osoari irekia dago.</p>
			<p>Denak zaudete gonbidatuak <a href="<?php echo $blogURL; ?>/login/?lang=eu">gurekin bidea egiten</a>.</p>			
		</div>
		<?php } else if ($lang=="gl") { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> é unha ferramenta de autor de código aberto para axudar aos docentes na creación e publicación de contidos web.</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> Os recursos elaborados con <strong>eXe</strong> poden exportarse en diferentes formatos: IMS, SCORM 1.2... Tamén como páxinas web navegables.</li>
			</ul>
		</div>
		<div id="col-b">
			<p>Fai uns meses anunciamos o lanzamento da nova versión de <strong>eXeLearning</strong>.</p>
			<p>Hoxe, anunciamos a posta en marcha deste espazo adicado ao <strong>novo eXeLearning</strong>.</p>
			<p class="logo"><a href="http://exelearning.net/descargas-gl/"><img src="<?php echo $themePath; ?>/images/exelearning/home/exe_2.png" width="207" height="195" alt="eXeLearning" /></a></p>
			<p>No seu <a href="http://exelearning.net/desenvolvedores/">desenvolvemento</a> colaboran diferentes institucións, e tamén persoas que amablemente seguen un espíritu colaborativo, <a href="http://exelearning.net/descargas-gl/" title="Descargas">proban a ferramenta</a>, <a href="http://exelearning.net/forums/?lang=gl" title="Foros">debaten</a> e comparten.</p>
			<p><strong>eXeLearning</strong> é software libre. A participación está aberta a toda a comunidade educativa.</p>
			<p>Estades tod@s <a href="http://exelearning.net/login/?lang=gl">invitados a acompañarnos</a>.</p>			
		</div>        
		<?php } else if ($lang=="ca") { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> és una eina d'autor de codi obert per ajudar als docents a crear i publicar continguts web. </li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> Els recursos elaborats amb <strong>eXe</strong> poden exportar-se a diferents formats: IMS, SCORM 1.2… També com pàgines web navegables.</li>
			</ul>
		</div>
		<div id="col-b">
			<p>Fa uns mesos anunciàvem el llançament de la nova versió d'<strong>eXeLearning</strong>.</p>
			<p>Avui, anunciem la posada en marxa d'aquest espai dedicat al <strong>nou eXeLearning</strong>.</p>
			<p class="logo"><a href="http://exelearning.net/descarregues/"><img src="<?php echo $themePath; ?>/images/exelearning/home/exe_2.png" width="207" height="195" alt="" /></a></p>
			<p>En el seu <a href="<?php echo $blogURL; ?>/desenvolupadors/">desenvolupament</a> col·laboren diferents institucions, i també persones que amablement i seguint un esperit col·laboratiu <a href="<?php echo $blogURL; ?>/descarregues/">proven l'eina</a>, <a href="<?php echo $blogURL; ?>/forums/?lang=ca">debaten</a> i comparteixen.</p>
			<p><strong>eXeLearning</strong> és programari lliure. La participació està oberta a tota la comunitat educativa.</p>
			<p>Esteu tots/totes convidats/convidades a <a href="<?php echo $blogURL; ?>/login/?lang=ca">acompanyar-nos</a>.</p>
		</div>		
        <?php } else { ?>
		<div id="col-a">
			<ul>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/01.png" width="260" height="160" alt="" /> <strong>eXeLearning</strong> es una herramienta de autor de código abierto para ayudar a los docentes en la creación y publicación de contenidos web.
</li>
				<li><img src="<?php echo $themePath; ?>/images/exelearning/home/02.png" width="260" height="160" alt="" /> Los recursos elaborados con <strong>eXe</strong> pueden exportarse en diferentes formatos: IMS, SCORM 1.2&hellip; También como páginas web navegables.</li>
			</ul>
		</div>
		<div id="col-b">
			<p>Hace unos meses anunciamos el lanzamiento de la nueva versi&oacute;n de <strong>eXeLearning</strong>.</p>
			<p>Hoy, anunciamos la puesta en marcha de este espacio dedicado al <strong>nuevo eXeLearning</strong>.</p>
			<p class="logo"><a href="http://exelearning.net/descargas/"><img src="<?php echo $themePath; ?>/images/exelearning/home/exe_2.png" width="207" height="195" alt="Logotipo de eXeLearning" /></a></p>
			<p>En su <a href="<?php echo $blogURL; ?>/desarrolladores/">desarrollo</a> colaboran diferentes instituciones, y tambi&eacute;n personas que amablemente y siguiendo un esp&iacute;ritu colaborativo <a href="<?php echo $blogURL; ?>/descargas/">prueban la herramienta</a>, <a href="<?php echo $blogURL; ?>/forums/">debaten</a> y comparten.</p>
			<p><strong>eXeLearning</strong> es software libre. La participaci&oacute;n est&aacute; abierta a toda la comunidad educativa.</p>
			<p>Est&aacute;is tod@s <a href="<?php echo $blogURL; ?>/login/">invitados a acompa&ntilde;arnos</a>.</p>
		</div>		
		<?php } ?>		
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>