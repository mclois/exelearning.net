<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
global $wp_query, $lang_link, $lang, $my_i18n;;

get_header();

$taxonomy = get_query_var('taxonomy');
$term_slug = get_query_var($taxonomy);
$term = get_term_by('slug', $term_slug, $taxonomy);
switch ($taxonomy) {
  case 'exe_style_colour' : 
    $term_name = $my_i18n['exe_style_colours'][$term->slug];
    break;
    
  case 'exe_style_tag' :  
    $term_name = $my_i18n['exe_style_tags'][$term->slug];
    break;
    
  case 'exe_style_compatibility' :  
    $term_name = 'eXe Learning ' . $term->name;
    break;
    
  default :
    $term_name = $term->name;
}
?>

		<section id="primary">
			<div id="content" role="main">

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title">
						<?php
						if ( is_day() ) {
						  printf( __( '%s daily Archives: %s', 'twentyeleven' ), $term_name, '<span>' . get_the_date() . '</span>' );
						}
						elseif ( is_month() ) {
              printf( __( '%s monthly Archives: %s', 'twentyeleven' ), $term_name, '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyeleven' ) ) . '</span>' ); 
            }
            elseif ( is_year() ) { 
              printf( __( '%s yearly Archives: %s', 'twentyeleven' ), $term_name, '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentyeleven' ) ) . '</span>' ); 
            }
            else {
							echo $term_name;
					  } ?>
					</h1>
					
      		<?php if (substr($taxonomy, 0, 10) == 'exe_style_') { ?>
      		  <div id="term">
      		    <?php echo __($term->description); ?>
      		  </div>
      		<?php } ?>
				</header>

				<?php twentyeleven_content_nav( 'nav-above' ); ?>
				
				<?php if (have_posts()) { 
				  switch ($taxonomy) {
            case 'exe_style_license' :
            	printf('<h2>' . $my_i18n['exe_style_labels']['taxonomy_list_license'] . '</h2>', $term_name);
            	break;
            
            case 'exe_style_compatibility' :
            	printf('<h2>' . $my_i18n['exe_style_labels']['taxonomy_list_compatibility'] . '</h2>', $term_name);
            	break;
            
            case 'exe_style_tag' :
            	printf('<h2>' . $my_i18n['exe_style_labels']['taxonomy_list_tag'] . '</h2>', $term_name);
            	break;
            
            case 'exe_style_colour' :
            	printf('<h2>' . $my_i18n['exe_style_labels']['taxonomy_list_colour'] . '</h2>', $term_name);
            	break;
          } 
				} ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
		</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>