<?php global $my_i18n; ?>
<?php get_header(); ?>
<div id="primary">
	<h1 class="page-title"><?php echo $my_i18n["searchResultsFor"]; ?>: <span>'</span><strong><?php echo get_search_query(); ?></strong><span>'</span></h1>
	<?php if ( have_posts() ) : ?>
		<?php twentyeleven_content_nav( 'nav-above' ); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
		<?php twentyeleven_content_nav( 'nav-below' ); ?>
	<?php else : ?>
		<div class="post no-results not-found">
			<div class="entry-content">
				<p><?php echo $my_i18n['noSearchResults']; ?></p>
			</div>
		</div>
	<?php endif; ?>
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>