<?php global $blogURL, $my_i18n, $lang; ?>
<div id="secondary">
	<?php if (is_page(215) || is_page(217) || is_page(614)) { ?>
		<p id="rss-alternate"><?php echo $my_i18n['youCanAlsoFollowUsOnTwitter']; ?>&hellip; 
			<a href='https://twitter.com/exelearning_sp' class="js-scr-av">@exelearning_sp</a>
		</p>
	<?php } else { ?>
	<div id="rss-feeds" class="sidebar-block">
		<h2 class='widget-title'>RSS</h2>
		<p><a href="<?php echo $blogURL; ?>/rss-<?php echo $lang; ?>/"><?php echo $my_i18n['followUsViaRSS']; ?></a> &raquo;</p>		
	</div>
	<?php } ?>
    <script type="text/javascript">/*<![CDATA[*/
        document.write('<a class="twitter-timeline"  href="https://twitter.com/exelearning_sp"  data-widget-id="347612916448624640">@exelearning_sp</a>');
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    /*]]>*/</script>
</div>