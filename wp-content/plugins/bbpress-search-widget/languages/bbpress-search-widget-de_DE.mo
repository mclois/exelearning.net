��          �   %   �      0     1     5     N     U  6   Y  6   �     �  #   �                5  �   <  '   �     �     �  {   �     {     �     �     �     �  "   �  <   �  �  *               -     5  6   K  F   �  #   �  =   �     +	     B	     Y	  �   `	  $   �	     
  $   /
  �   T
               !     5     I      ^  6                                                                            	            
                                          1.1 David Decker - DECKERWEB Donate FAQ For example add some additional forum/search info etc. For example add some additional user instructions etc. Go to the Widgets settings page Just leave blank to not use at all. Optional intro text: Optional outro text: Search Search box for the bbPress 2.x forum plugin. Search in forum topics and replies only. (No mix up with regular WordPress search!) Search forum in topics and replies for: Search the forums Support This Plugin adds a search widget for the bbPress 2.x forum plugin post types independent from the regular WordPress search. Title: Widgets bbPress Forum Search bbPress Search Widget http://deckerweb.de/ http://genesisthemes.de/en/donate/ http://genesisthemes.de/en/wp-plugins/bbpress-search-widget/ Project-Id-Version: bbPress Search Widget Plugin
Report-Msgid-Bugs-To: http://wordpress.org/tags/bbpress-search-widget?forum_id=10
POT-Creation-Date: 2011-10-10 21:08+0100
PO-Revision-Date: 2012-04-15 21:28+0100
Last-Translator: David Decker <deckerweb.mobil@googlemail.com>
Language-Team: DECKERWEB <deckerweb.mobil@googlemail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 1.1 David Decker - DECKERWEB.de Spenden FAQ - Häufige Fragen Zum Beispiel für zusätzliche Forum-/ Such-Infos etc. Zum Beispiel um zusätzliche Benutzer-Instruktionen hinzuzufügen etc. Zur Widgets-Einstellungen Seite ... Einfach frei lassen, um dieses Feld <em>nicht</em> zu nutzen. Optionaler Intro-Text: Optionaler Outro-Text: Suchen Suchformular für das bbPress 2.x Forum-Plugin. Suche beschränkt auf Foren-Themen und Antworten. (Kein Vermischen mit der regulären WordPress-Suche!) Suche in Foren-Themen und Antworten: Foren durchsuchen Hilfe &amp; Unterstützung (Support) Dieses Plugin fügt ein Such-Widget für das bbPress 2.x Forum-Plugin hinzu, um dessen Inhaltstypen ( Themen, Antworten) unabhängig von der normalen WordPress-Suche durchsuchen zu können. Titel: Widgets bbPress Forum-Suche bbPress Such-Widget http://deckerweb.de/ http://genesisthemes.de/spenden/ http://genesisthemes.de/plugins/bbpress-search-widget/ 