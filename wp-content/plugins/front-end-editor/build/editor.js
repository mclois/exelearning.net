(function(){
  var extract_data_attr, get_group_button, base, input, select, textarea, image_base, image, thumbnail, rich, terminput, termselect, widget, group, createPost;
  extract_data_attr = function(el){
    var data, attr, value, _i, _ref, _len;
    data = {};
    for (_i = 0, _len = (_ref = el.attributes).length; _i < _len; ++_i) {
      attr = _ref[_i];
      if (attr.specified && 0 === attr.name.indexOf('data-')) {
        value = attr.value;
        try {
          value = jQuery.parseJSON(value);
        } catch (_e) {}
        if (null === value) {
          value = '';
        }
        data[attr.name.substr(5)] = value;
      }
    }
    return data;
  };
  FrontEndEditor.fieldTypes = {};
  FrontEndEditor.edit_lock = function($el){
    FrontEndEditor._editing = true;
    return $el.trigger('edit_start');
  };
  FrontEndEditor.edit_unlock = function($el){
    FrontEndEditor._editing = false;
    return $el.trigger('edit_stop');
  };
  FrontEndEditor.is_editing = function(){
    return FrontEndEditor._editing;
  };
  FrontEndEditor.overlay = function(){
    var $cover;
    $cover = jQuery('<div>', {
      'class': 'fee-loading'
    }).css('background-image', 'url(' + FrontEndEditor.data.spinner + ')').hide().prependTo(jQuery('body'));
    return {
      cover: function($el){
        var parent, bgcolor, _i, _ref, _len;
        for (_i = 0, _len = (_ref = $el.parents()).length; _i < _len; ++_i) {
          parent = _ref[_i];
          bgcolor = jQuery(parent).css('background-color');
          if ('transparent' !== bgcolor) {
            break;
          }
        }
        return $cover.css({
          'width': $el.width(),
          'height': $el.height(),
          'background-color': bgcolor
        }).css($el.offset()).show();
      },
      hide: function(){
        return $cover.hide();
      }
    };
  }();
  FrontEndEditor.make_editable = function(el, single){
    var $el, data, fieldType, editor;
    $el = jQuery(el);
    data = extract_data_attr(el);
    fieldType = FrontEndEditor.fieldTypes[data.type];
    if (!fieldType) {
      if (console) {
        console.warn('invalid field type', el);
      }
      return;
    }
    editor = new fieldType;
    editor.el = $el;
    editor.data = data;
    if (single) {
      FrontEndEditor.hover_init($el, __bind(editor, editor.start_editing));
      $el.data('fee-editor', editor);
    }
    return editor;
  };
  FrontEndEditor.hover_init = function(){
    var get_dims, HOVER_BORDER, HOVER_PADDING, hover_lock, hover_timeout, hover_border, hover_box, box_position_vert, hover_hide_immediately, hover_hide, hover_show;
    get_dims = function($el){
      return {
        'width': $el.width(),
        'height': $el.height()
      };
    };
    HOVER_BORDER = 2;
    HOVER_PADDING = 2;
    hover_lock = false;
    hover_timeout = void 8;
    hover_border = jQuery('<div>').addClass('fee-hover-border').css('width', HOVER_BORDER).hide().appendTo('body');
    hover_box = jQuery('<div>', {
      'class': 'fee-hover-edit',
      'html': FrontEndEditor.data.edit_text,
      'mouseover': function(){
        return hover_lock = true;
      },
      'mouseout': function(){
        hover_lock = false;
        return hover_hide();
      }
    }).hide().appendTo('body');
    box_position_vert = function(mouse_vert_pos){
      var normal_height;
      normal_height = mouse_vert_pos - hover_box.outerHeight() / 2;
      return hover_box.css('top', (normal_height - HOVER_BORDER) + 'px');
    };
    hover_hide_immediately = function(){
      hover_box.hide();
      return hover_border.hide();
    };
    hover_hide = function(){
      return hover_timeout = setTimeout(function(){
        if (hover_lock) {
          return;
        }
        return hover_hide_immediately();
      }, 300);
    };
    hover_show = function(callback){
      var $self, offset, dims;
      $self = jQuery(this);
      offset = $self.offset();
      dims = get_dims($self);
      if (dims.width > $self.parent().width()) {
        $self.css('display', 'block');
        dims = get_dims($self);
      }
      clearTimeout(hover_timeout);
      hover_box.unbind('click');
      hover_box.bind('click', hover_hide_immediately);
      hover_box.bind('click', callback);
      hover_box.css('left', (offset.left - hover_box.outerWidth() - HOVER_PADDING) + 'px');
      hover_box.show();
      return hover_border.css({
        'left': (offset.left - HOVER_PADDING - HOVER_BORDER) + 'px',
        'top': (offset.top - HOVER_PADDING - HOVER_BORDER) + 'px',
        'height': (dims.height + HOVER_PADDING * 2) + 'px'
      }).show();
    };
    return function($el, callback){
      return $el.bind({
        mouseover: function(ev){
          if (FrontEndEditor.is_editing()) {
            return;
          }
          box_position_vert(ev.pageY);
          return hover_show.call(this, callback);
        },
        mousemove: function(ev){
          return box_position_vert(ev.pageY);
        },
        mouseout: hover_hide
      });
    };
  }();
  get_group_button = function($container){
    var $button;
    $button = $container.find('.fee-edit-button');
    if ($button.length) {
      return $button;
    }
    if (FrontEndEditor.data.add_buttons) {
      $button = jQuery('<span>', {
        'class': 'fee-edit-button',
        text: FrontEndEditor.data.edit_text
      });
      $button.appendTo($container);
      return $button;
    }
    return false;
  };
  jQuery(function(){
    var el, $el, $widget, $container, $elements, editor, editors, fieldType, $button, _i, _ref, _len, _res, _j, _len2, _results = [];
    for (_i = 0, _len = (_ref = jQuery('[data-filter="widget_title"], [data-filter="widget_text"]')).length; _i < _len; ++_i) {
      el = _ref[_i];
      $el = jQuery(el);
      $widget = $el.closest('.widget_text');
      if ($widget.length) {
        $el.attr('data-widget_id', $widget.attr('id'));
        $widget.addClass('fee-group');
      } else {
        $el.unwrap();
      }
    }
    for (_i = 0, _len = (_ref = jQuery('.fee-group')).length; _i < _len; ++_i) {
      el = _ref[_i];
      $container = jQuery(el);
      $elements = $container.find('.fee-field').removeClass('fee-field');
      if (!$elements.length) {
        return;
      }
      _res = [];
      for (_j = 0, _len2 = $elements.length; _j < _len2; ++_j) {
        el = $elements[_j];
        editor = FrontEndEditor.make_editable(el);
        editor.part_of_group = true;
        _res.push(editor);
      }
      editors = _res;
      fieldType = $container.hasClass('status-auto-draft') ? 'createPost' : 'group';
      editor = new FrontEndEditor.fieldTypes[fieldType]($container, editors);
      $button = get_group_button($container);
      if ($button) {
        $button.click(__bind(editor, editor.start_editing));
        $container.bind({
          edit_start: _fn,
          edit_stop: _fn2
        });
      } else {
        FrontEndEditor.hover_init($container, __bind(editor, editor.start_editing));
      }
      $container.data('fee-editor', editor);
    }
    for (_i = 0, _len = (_ref = jQuery('.fee-field')).length; _i < _len; ++_i) {
      el = _ref[_i];
      _results.push(FrontEndEditor.make_editable(el, true));
    }
    return _results;
    function _fn(ev){
      $button.addClass('fee-disabled');
      return ev.stopPropagation();
    }
    function _fn2(ev){
      $button.removeClass('fee-disabled');
      return ev.stopPropagation();
    }
  });
  jQuery(window).load(function(){
    var _ref;
    return (_ref = jQuery('.fee-group.status-auto-draft').data('fee-editor')) != null ? _ref.start_editing() : void 8;
  });
  FrontEndEditor.fieldTypes.base = base = (function(){
    base.displayName = 'base';
    var prototype = base.prototype, constructor = base;
    prototype.get_type = function(){
      return this.constructor.displayName;
    };
    prototype.start_editing = null;
    prototype.ajax_get = function(){
      FrontEndEditor.edit_lock(this.el);
      return this._ajax_request({
        data: this.ajax_get_args.apply(this, arguments),
        success: __bind(this, this.ajax_get_handler)
      });
    };
    prototype.ajax_set = function(){
      return this._ajax_request({
        data: this.ajax_set_args.apply(this, arguments),
        success: __bind(this, this.ajax_set_handler)
      });
    };
    prototype._ajax_request = function(args){
      args.url = FrontEndEditor.data.ajax_url;
      args.type = 'POST';
      args.dataType = 'json';
      return jQuery.ajax(args);
    };
    prototype.ajax_get_handler = null;
    prototype.ajax_set_handler = null;
    prototype.ajax_get_args = function(){
      var args;
      args = this.ajax_args();
      args.callback = 'get';
      return args;
    };
    prototype.ajax_set_args = function(content){
      var args;
      args = this.ajax_args();
      args.callback = 'save';
      args.content = content;
      return args;
    };
    prototype.ajax_args = function(){
      return {
        action: 'front-end-editor',
        nonce: FrontEndEditor.data.nonce,
        data: this.data
      };
    };
    function base(){}
    return base;
  }());
  FrontEndEditor.fieldTypes.input = input = (function(_super){
    input.displayName = 'input';
    var prototype = __extends(input, _super).prototype, constructor = input;
    prototype.input_tag = '<input type="text">';
    prototype.start_editing = function(){
      this.create_form();
      this.create_buttons();
      this.create_input();
      this.ajax_get();
      return false;
    };
    prototype.create_buttons = function(){
      this.save_button = jQuery('<button>', {
        'class': 'fee-form-save',
        'text': FrontEndEditor.data.save_text,
        'click': __bind(this, this.submit_form)
      });
      this.cancel_button = jQuery('<button>', {
        'class': 'fee-form-cancel',
        'text': FrontEndEditor.data.cancel_text,
        'click': __bind(this, this.remove_form)
      });
      return this.form.append(this.save_button).append(this.cancel_button);
    };
    prototype.create_form = function(){
      this.form = this.el.is('span')
        ? jQuery('<span>')
        : jQuery('<div>');
      this.form.addClass('fee-form').addClass('fee-type-' + this.get_type());
      return this.form.keypress(__bind(this, this.keypress));
    };
    prototype.remove_form = function(){
      this.form.remove();
      this.el.show();
      FrontEndEditor.edit_unlock(this.el);
      return false;
    };
    prototype.submit_form = function(ev){
      this.ajax_set();
      return false;
    };
    prototype.keypress = function(ev){
      var keys, code;
      keys = {
        ENTER: 13,
        ESCAPE: 27
      };
      code = ev.keyCode || ev.which || ev.charCode || 0;
      if (code === keys.ENTER && 'input' === this.get_type()) {
        this.save_button.click();
      }
      if (code === keys.ESCAPE) {
        return this.cancel_button.click();
      }
    };
    prototype.create_input = function(){
      this.input = jQuery(this.input_tag).attr({
        'id': 'fee-' + new Date().getTime(),
        'class': 'fee-form-content'
      });
      return this.input.prependTo(this.form);
    };
    prototype.content_to_input = function(content){
      return this.input.val(content);
    };
    prototype.content_from_input = function(){
      return this.input.val();
    };
    prototype.content_to_front = function(content){
      return this.el.html(content);
    };
    prototype.ajax_get = function(){
      FrontEndEditor.overlay.cover(this.el);
      return input.superclass.prototype.ajax_get.apply(this, arguments);
    };
    prototype.ajax_set_args = function(contentData){
      FrontEndEditor.overlay.cover(this.form);
      if (0 == arguments.length) {
        contentData = this.content_from_input();
      }
      return input.superclass.prototype.ajax_set_args.call(this, contentData);
    };
    prototype.ajax_get_handler = function(response){
      var $el;
      $el = this.error_handler(response);
      if (!$el) {
        return;
      }
      this.el.hide();
      $el.after(this.form);
      this.content_to_input(response.content);
      return this.input.focus();
    };
    prototype.ajax_set_handler = function(response){
      var $el;
      $el = this.error_handler(response);
      if (!$el) {
        return;
      }
      this.content_to_front(response.content);
      return this.remove_form();
    };
    prototype.error_handler = function(response){
      var $parent, $el;
      $parent = this.el.closest('a');
      $el = $parent.length
        ? $parent
        : this.el;
      FrontEndEditor.overlay.hide();
      if (response.error) {
        jQuery('<div class="fee-error">').append(jQuery('<span class="fee-message">').html(response.error)).append(jQuery('<span class="fee-dismiss">x</span>').click(function(){
          return $error_box.remove();
        })).insertBefore($el);
        return false;
      }
      return $el;
    };
    function input(){}
    return input;
  }(FrontEndEditor.fieldTypes.base));
  FrontEndEditor.fieldTypes.select = select = (function(_super){
    select.displayName = 'select';
    var prototype = __extends(select, _super).prototype, constructor = select;
    prototype.input_tag = '<select>';
    prototype.content_to_input = function(content){
      var value, title, _ref, _results = [];
      for (value in _ref = this.data.values) {
        title = _ref[value];
        _results.push(this.input.append(jQuery('<option>', {
          value: value,
          html: title,
          selected: content === value
        })));
      }
      return _results;
    };
    prototype.content_from_input = function(){
      return this.input.find(':selected').val();
    };
    function select(){}
    return select;
  }(FrontEndEditor.fieldTypes.input));
  FrontEndEditor.fieldTypes.textarea = textarea = (function(_super){
    textarea.displayName = 'textarea';
    var prototype = __extends(textarea, _super).prototype, constructor = textarea;
    prototype.input_tag = '<textarea rows="10">';
    function textarea(){}
    return textarea;
  }(FrontEndEditor.fieldTypes.input));
  FrontEndEditor.fieldTypes.image_base = image_base = (function(_super){
    image_base.displayName = 'image_base';
    var _ref, prototype = __extends(image_base, _super).prototype, constructor = image_base;
    prototype.button_text = (_ref = FrontEndEditor.data.image) != null ? _ref.change : void 8;
    prototype.start_editing = function(){
      var _this = this;
      tb_show(this.button_text, FrontEndEditor.data.image.url);
      jQuery('#TB_closeWindowButton img').attr('src', FrontEndEditor.data.image.tb_close);
      return jQuery('#TB_iframeContent').load(function(ev){
        var iframe, $thickbox;
        iframe = ev.currentTarget.contentWindow;
        $thickbox = iframe.jQuery(iframe.document);
        _this.thickbox_load($thickbox);
        if (jQuery.noop !== _this.media_item_manipulation) {
          $thickbox.find('.media-item').each(function(i, el){
            return _this.media_item_manipulation(iframe.jQuery(el));
          });
          return $thickbox.ajaxComplete(function(event, request){
            var item_id;
            item_id = jQuery(request.responseText).find('.media-item-info').attr('id');
            return _this.media_item_manipulation($thickbox.find('#' + item_id).closest('.media-item'));
          });
        }
      });
    };
    prototype.thickbox_load = function($thickbox){
      var _this = this;
      return $thickbox.delegate('.media-item :submit', 'click', function(ev){
        var $button, data;
        $button = jQuery(ev.currentTarget);
        data = $button.closest('form').serializeArray();
        data.push({
          name: $button.attr('name'),
          value: $button.attr('name')
        });
        data.push({
          name: 'action',
          value: 'fee_image_insert'
        });
        jQuery.post(FrontEndEditor.data.ajax_url, data, __bind(_this, _this.image_html_handler));
        return false;
      });
    };
    prototype.media_item_manipulation = function($item){
      $item.find('#go_button').remove();
      return $item.find(':submit').val(this.button_text);
    };
    function image_base(){}
    return image_base;
  }(FrontEndEditor.fieldTypes.base));
  FrontEndEditor.fieldTypes.image = image = (function(_super){
    image.displayName = 'image';
    var prototype = __extends(image, _super).prototype, constructor = image;
    prototype.start_editing = function(){
      var _this = this;
      image.superclass.prototype.start_editing.apply(this, arguments);
      return jQuery('<a id="fee-img-revert" href="#">').text(FrontEndEditor.data.image.revert).click(function(ev){
        _this.ajax_set(-1);
        return false;
      }).insertAfter('#TB_ajaxWindowTitle');
    };
    prototype.media_item_manipulation = function($item){
      $item.find('tbody tr').not('.image-size, .submit').hide();
      return image.superclass.prototype.media_item_manipulation.apply(this, arguments);
    };
    prototype.image_html_handler = function(html){
      var $html;
      $html = jQuery(html);
      if ($html.is('a')) {
        $html = $html.find('img');
      }
      return this.ajax_set($html.attr('src'));
    };
    prototype.ajax_set_handler = function(response){
      var url;
      url = response.content;
      if ('-1' === url) {
        return location.reload(true);
      } else {
        this.el.find('img').attr('src', url);
        return tb_remove();
      }
    };
    function image(){}
    return image;
  }(FrontEndEditor.fieldTypes.image_base));
  FrontEndEditor.fieldTypes.thumbnail = thumbnail = (function(_super){
    thumbnail.displayName = 'thumbnail';
    var prototype = __extends(thumbnail, _super).prototype, constructor = thumbnail;
    prototype.thickbox_load = function($thickbox){
      var _this = this;
      $thickbox.find('#tab-type_url').remove();
      return $thickbox.delegate('.media-item :submit', 'click', function(ev){
        var $item, attachment_id;
        $item = jQuery(ev.currentTarget).closest('.media-item');
        attachment_id = $item.attr('id').replace('media-item-', '');
        _this.ajax_set(attachment_id);
        return false;
      });
    };
    prototype.media_item_manipulation = function($item){
      $item.find('tbody tr').not('.submit').remove();
      return thumbnail.superclass.prototype.media_item_manipulation.apply(this, arguments);
    };
    function thumbnail(){}
    return thumbnail;
  }(FrontEndEditor.fieldTypes.image));
  if (typeof Aloha != 'undefined' && Aloha !== null) {
    Aloha.require(['aloha/selection'], function(Selection){
      var image_rich;
      return FrontEndEditor.fieldTypes.image_rich = image_rich = (function(_super){
        image_rich.displayName = 'image_rich';
        var _ref, prototype = __extends(image_rich, _super).prototype, constructor = image_rich;
        prototype.button_text = (_ref = FrontEndEditor.data.image) != null ? _ref.insert : void 8;
        prototype.start_editing = function(){
          jQuery('.aloha-floatingmenu, #aloha-floatingmenu-shadow').hide();
          return image_rich.superclass.prototype.start_editing.apply(this, arguments);
        };
        prototype.media_item_manipulation = jQuery.noop;
        prototype.image_html_handler = function(html){
          GENTICS.Utils.Dom.insertIntoDOM(jQuery(html), Selection.getRangeObject(), Aloha.activeEditable.obj);
          tb_remove();
          return jQuery('.aloha-floatingmenu, #aloha-floatingmenu-shadow').show();
        };
        function image_rich(){}
        return image_rich;
      }(FrontEndEditor.fieldTypes.image_base));
    });
  }
  FrontEndEditor.fieldTypes.rich = rich = (function(_super){
    rich.displayName = 'rich';
    var prototype = __extends(rich, _super).prototype, constructor = rich;
    prototype.content_from_input = function(){
      return Aloha.getEditableById(this.form.attr('id')).getContents();
    };
    prototype.create_input = jQuery.noop;
    prototype.create_form = function(){
      return this.form = Aloha.jQuery('<div class="fee-form fee-type-rich">');
    };
    prototype.remove_form = function(){
      this.form.mahalo();
      return rich.superclass.prototype.remove_form.apply(this, arguments);
    };
    prototype.start_editing = function(ev){
      rich.superclass.prototype.start_editing.apply(this, arguments);
      return FrontEndEditor.current_field = this;
    };
    prototype.ajax_get_handler = function(response){
      var $el;
      $el = this.error_handler(response);
      if (!$el) {
        return;
      }
      this.create_form();
      this.form.html(response.content);
      this.el.hide();
      this.form.insertAfter($el);
      this.form.aloha();
      if (!this.part_of_group) {
        this.form.focus();
        return this.form.dblclick();
      }
    };
    function rich(){}
    return rich;
  }(FrontEndEditor.fieldTypes.textarea));
  FrontEndEditor.fieldTypes.terminput = terminput = (function(_super){
    terminput.displayName = 'terminput';
    var prototype = __extends(terminput, _super).prototype, constructor = terminput;
    prototype.content_to_input = function(content){
      terminput.superclass.prototype.content_to_input.apply(this, arguments);
      return this.input.suggest(FrontEndEditor.data.ajax_url + '?action=ajax-tag-search&tax=' + this.data.taxonomy, {
        multiple: true,
        resultsClass: 'fee-suggest-results',
        selectClass: 'fee-suggest-over',
        matchClass: 'fee-suggest-match'
      });
    };
    function terminput(){}
    return terminput;
  }(FrontEndEditor.fieldTypes.input));
  FrontEndEditor.fieldTypes.termselect = termselect = (function(_super){
    termselect.displayName = 'termselect';
    var prototype = __extends(termselect, _super).prototype, constructor = termselect;
    prototype.content_to_input = function(content){
      var $dropdown;
      $dropdown = jQuery(content);
      this.input.replaceWith($dropdown);
      return this.input = $dropdown;
    };
    function termselect(){}
    return termselect;
  }(FrontEndEditor.fieldTypes.select));
  FrontEndEditor.fieldTypes.widget = widget = (function(_super){
    widget.displayName = 'widget';
    var prototype = __extends(widget, _super).prototype, constructor = widget;
    prototype.create_input = jQuery.noop;
    prototype.content_to_input = function(content){
      this.input = jQuery(content);
      return this.form.prepend(content);
    };
    prototype.ajax_set_args = function(){
      var args, name, value, _i, _ref, _len, _ref2;
      args = widget.superclass.prototype.ajax_set_args.apply(this, arguments);
      for (_i = 0, _len = (_ref = this.form.find(':input').serializeArray()).length; _i < _len; ++_i) {
        _ref2 = _ref[_i], name = _ref2.name, value = _ref2.value;
        args[name] = void 8 === args[name]
          ? value
          : jQuery.isArray(args[name])
            ? args[name].concat(value)
            : [args[name], value];
      }
      return args;
    };
    function widget(){}
    return widget;
  }(FrontEndEditor.fieldTypes.textarea));
  FrontEndEditor.fieldTypes.group = group = (function(_super){
    group.displayName = 'group';
    var prototype = __extends(group, _super).prototype, constructor = group;
    function group(el, editors){
      var editor, _i, _ref, _len;
      this.el = el;
      this.editors = editors;
      this.has_aloha = false;
      if (typeof Aloha != 'undefined' && Aloha !== null) {
        for (_i = 0, _len = (_ref = this.editors).length; _i < _len; ++_i) {
          editor = _ref[_i];
          if ('rich' == editor.get_type()) {
            this.has_aloha = true;
            return;
          }
        }
      }
    }
    prototype.start_editing = function(ev){
      this.create_form();
      if (this.has_aloha) {
        FrontEndEditor.current_field = this;
      } else {
        this.create_buttons();
      }
      this.ajax_get();
      return false;
    };
    prototype.create_form = function(){
      var editor, _i, _ref, _len;
      for (_i = 0, _len = (_ref = this.editors).length; _i < _len; ++_i) {
        editor = _ref[_i];
        editor.create_form();
        editor.create_input();
      }
      group.superclass.prototype.create_form.apply(this, arguments);
      return this.el.append(this.form);
    };
    prototype.remove_form = function(ev){
      var editor, _i, _ref, _len;
      for (_i = 0, _len = (_ref = this.editors).length; _i < _len; ++_i) {
        editor = _ref[_i];
        editor.remove_form();
      }
      return group.superclass.prototype.remove_form.apply(this, arguments);
    };
    prototype.content_from_input = function(){
      var editor;
      return (function(){
        var _i, _ref, _len, _results = [];
        for (_i = 0, _len = (_ref = this.editors).length; _i < _len; ++_i) {
          editor = _ref[_i];
          _results.push(editor.content_from_input());
        }
        return _results;
      }.call(this));
    };
    prototype.keypress = jQuery.noop;
    prototype.ajax_set = function(){
      group.superclass.prototype.ajax_set.apply(this, arguments);
      return FrontEndEditor.overlay.cover(this.el);
    };
    prototype.ajax_args = function(){
      var args, editor, dataArr, commonData, i, key, value, data, item, _res, _i, _ref, _len, _to, _own = {}.hasOwnProperty;
      args = group.superclass.prototype.ajax_args.apply(this, arguments);
      args.group = true;
      _res = [];
      for (_i = 0, _len = (_ref = this.editors).length; _i < _len; ++_i) {
        editor = _ref[_i];
        _res.push(editor.data);
      }
      dataArr = _res;
      if (dataArr.length == 1) {
        args.data = dataArr;
      } else {
        commonData = (__import({}, dataArr[0]));
        for (i = 1, _to = dataArr.length - 1; i <= _to; ++i) {
          for (key in commonData) if (_own.call(commonData, key)) {
            value = commonData[key];
            if (value !== dataArr[i][key]) {
              delete commonData[key];
            }
          }
        }
        _res = [];
        for (_i = 0, _len = dataArr.length; _i < _len; ++_i) {
          data = dataArr[_i];
          item = {};
          for (key in data) if (_own.call(data, key)) {
            if (!(key in commonData)) {
              item[key] = data[key];
            }
          }
          _res.push(item);
        }
        args.data = _res;
        args.commonData = commonData;
      }
      return args;
    };
    prototype.ajax_get_handler = function(response){
      var i, editor, _ref, _len;
      for (i = 0, _len = (_ref = this.editors).length; i < _len; ++i) {
        editor = _ref[i];
        editor.ajax_get_handler(response[i]);
      }
      return (_ref = this.editors[0].input) != null ? _ref.focus() : void 8;
    };
    prototype.ajax_set_handler = function(response){
      var i, editor, _ref, _len;
      for (i = 0, _len = (_ref = this.editors).length; i < _len; ++i) {
        editor = _ref[i];
        editor.ajax_set_handler(response[i]);
      }
      return this.remove_form();
    };
    return group;
  }(FrontEndEditor.fieldTypes.input));
  FrontEndEditor.fieldTypes.createPost = createPost = (function(_super){
    createPost.displayName = 'createPost';
    var prototype = __extends(createPost, _super).prototype, constructor = createPost;
    function createPost(){
      createPost.superclass.apply(this, arguments);
    }
    prototype.ajax_set_args = function(){
      var args;
      args = createPost.superclass.prototype.ajax_set_args.apply(this, arguments);
      args.createPost = true;
      return args;
    };
    prototype.ajax_set_handler = function(response){
      return window.location = response.permalink;
    };
    return createPost;
  }(FrontEndEditor.fieldTypes.group));
  function __bind(me, fn){ return function(){ return fn.apply(me, arguments) } }
  function __extends(sub, sup){
    function ctor(){} ctor.prototype = (sub.superclass = sup).prototype;
    (sub.prototype = new ctor).constructor = sub;
    if (typeof sup.extended == 'function') sup.extended(sub);
    return sub;
  }
  function __import(obj, src){
    var own = {}.hasOwnProperty;
    for (var key in src) if (own.call(src, key)) obj[key] = src[key];
    return obj;
  }
}).call(this);
