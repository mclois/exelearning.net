<?php
/*
 Plugin Name: eXe Styles repository
 Plugin URI: http://exelearning.net/
 Description: Repository of eXeLearning.net styles
 Version: 1.1
 Author: Mercedes Cotelo
 License: GPLv2 or later
*/

/**
 * eXe Styles plugin
 *
 * @author mclois
 */
class eXeStylesPlugin {
  /**
   * Actions on enabling the plugin
   */
  static function install() {
    // Register taxonomy first, and then create terms in the taxonomy 
    eXeStyleColour::init_taxonomy();
    eXeStyleColour::register_colours();

    eXeStyleTag::init_taxonomy();
    eXeStyleTag::register_tags();

    eXeStyleLicense::init_taxonomy();
    eXeStyleLicense::register_licenses();

    eXeStyleCompatibility::init_taxonomy();
    eXeStyleCompatibility::register_versions();
    
    flush_rewrite_rules();
  }

  /**
   * Intialize plugin error handling
   */
  static function init_errors() {
    global $exe_style_error;

    // Set error ID
    $exe_style_error_id = 'exe_style_error';
    if (!isset($exe_style_error)) {
      $exe_style_error = new WP_Error();
    }
  }

  /**
   * Dump found errors
   */
  static function dump_errors() {
    // Get errors from transient
    $errors = get_transient('exe_style_error');
    
    if (!empty($errors) && is_wp_error($errors)) { 
      $messages = $errors->get_error_messages();
      
      if (!empty($messages)) {
        echo '<div class="exe-style-errors error">';
        echo __('Errors');
        echo '<ul>';
        foreach (array_unique($messages) as $message) {
      	  echo '<li>' . __($message) . '</li>';
        }
        echo '</ul>';
        echo '</div>';
      }
    }
   
    // Delete transient to avoid displaying same error twice
    delete_transient('exe_style_error');
  }

  /**
   * Add an error to the error list and saves it as a transient, so that it persits through redirections
   *
	 * @param string|int $code    Error code.
	 * @param string     $message Error message.
   * 
   * @see http://codex.wordpress.org/Class_Reference/WP_Error
   * @see http://codex.wordpress.org/Function_Reference/set_transient
   */
  public static function add_error($code, $message) {
    global $exe_style_error;
    $exe_style_error->add($code, $message);
    set_transient('exe_style_error', $exe_style_error, 60 * 60 * 10);
  }
  
  /**
   * Define custom metabox for eXe styles
   */
  public static function add_meta_box() {
    // Remove default metabox for custom taxonomy terms,
    // 'exe_style_meta' will provide custom widgets to input terms
    remove_meta_box('tagsdiv-exe_style_license', 'exe_style', 'side');
    remove_meta_box('tagsdiv-exe_style_compatibility', 'exe_style', 'side');
    remove_meta_box('tagsdiv-exe_style_tag', 'exe_style', 'side');
    remove_meta_box('tagsdiv-exe_style_colour', 'exe_style', 'side');
    add_meta_box(
      'exe_style_meta',
      __('eXe Style'),
      array('eXeStylesPlugin', 'render_meta_box'),
      'exe_style',
      'normal',
      'high'
    );
  }
  
  /**
   * Add `enctype="multipart/form-data"` to post form, to allow file uploading
   * 
   * @see http://code.tutsplus.com/articles/attaching-files-to-your-posts-using-wordpress-custom-meta-boxes-part-1--wp-22291
   */
  public static function update_edit_form() {
    echo ' enctype="multipart/form-data"';
  }
  
  /**
   * Render form to edit meta data (meta box)
   * 
   * @param Object  $post     Post data      
   * @param Array   $metabox  Associative array with metabox id, title, callback, and args elements. 
   * 
   * @see http://codex.wordpress.org/Function_Reference/add_meta_box#Class
   */
  public static function render_meta_box($post, $metabox) {

    global $wp_meta_boxes;
    
    // Add an nonce field so we can check for it later.
    wp_nonce_field(basename(__FILE__), 'exe_style_nonce' );

    // Get current values of metadata fields from database
    $version_stored_meta = get_post_meta($post->ID, '_exe_style_version_meta_key', TRUE);
    $author_stored_meta = get_post_meta($post->ID, '_exe_style_author_meta_key', TRUE);
    $author_url_stored_meta = get_post_meta($post->ID, '_exe_style_author_url_meta_key', TRUE);
    $demo_url_stored_meta = get_post_meta($post->ID, '_exe_style_demo_url_meta_key', TRUE);
    
    // Get current terms associated with this style
    $license = wp_get_object_terms($post->ID, 'exe_style_license');
    $compatibility = wp_get_object_terms($post->ID, 'exe_style_compatibility');
    $tags = wp_get_object_terms($post->ID, 'exe_style_tag');
    $colours = wp_get_object_terms($post->ID, 'exe_style_colour');
    
    // Get post id of the attached zip file
    $download_zip_stored_meta = get_post_meta($post->ID, '_exe_style_download_zip_meta_key', TRUE);

    // Singular values, take just the first element from the array of terms
    $license = array_shift($license);
    $compatibility = array_shift($compatibility);
    
    // Multiple values, build list of terms id's
    $tags_ids = array();
    foreach ($tags as $tag) {
      $tags_ids[] = $tag->term_id;
    }
    $colours_ids = array();
    foreach ($colours as $colour) {
      $colours_ids[] = $colour->term_id;
    }
    
    // Display custom form fields, using the current values.
    echo '<div class="exe_style_demo_url">';
    echo '<label for="exe_style_demo_url">' . __('Demo') . '</label> ';
    echo '<input type="text" id="exe_style_demo_url" name="exe_style_demo_url" value="' . esc_attr($demo_url_stored_meta) . '" size="128" />';
    echo '</div>';
    
    echo '<div class="exe_style_version">';
    echo '<label for="exe_style_version">' . __('Style version') . '</label> ';
    echo '<input type="text" id="exe_style_version" name="exe_style_version" value="' . esc_attr($version_stored_meta) . '" size="5" />';
    echo '</div>';

    echo '<div class="exe_style_author">';
    echo '<label for="exe_style_author">' . __('Author') . '</label> ';
    echo '<input type="text" id="exe_style_author" name="exe_style_author" value="' . esc_attr($author_stored_meta) . '" size="50" />';
    echo '</div>';

    echo '<div class="exe_style_author_url">';
    echo '<label for="exe_style_author_url">' . __('Author URL') . '</label> ';
    echo '<input type="text" id="exe_style_author_url" name="exe_style_author_url" value="' . esc_attr($author_url_stored_meta) . '" size="128" />';
    echo '</div>';
    
    // Display custom widgets to associate taxonomy terms to this post
    // Original idea from: http://shibashake.com/wordpress-theme/wordpress-custom-taxonomy-input-panels
    
    // Compatibility as a dropdown select element, with one option for each term in 'exe_style_compatibility' taxonomy,
    // and mark as selected the current term (defaults to empty option)
    echo '<div class="exe_style_compatibility">';
    echo '<label for="exe_style_compatibility">' . __('eXe target version (compatibility)') . '</label> ';
    echo '<select type="text" id="exe_style_compatibility" name="exe_style_compatibility">';
    echo '<option value="">' . __('Please select') . '</option>';
    // 'hide_empty=0' => show unused terms too
    foreach (get_terms('exe_style_compatibility', 'hide_empty=0') as $term) {
      $selected = ($term->term_id == $compatibility->term_id)? ' selected="selected"' : '';
      echo '<option value="' . $term->term_id . '"' . $selected . '>' . $term->name . '</option>';
    }
    echo '</select>';
    echo '</div>';
    

    // License as a dropdown select element, with one option for each term in 'exe_style_license' taxonomy,
    // and mark as selected the current term (defaults to empty option)
    echo '<div class="exe_style_license">';
    echo '<label for="exe_style_license">' . __('License') . '</label> ';
    echo '<select type="text" id="exe_style_license" name="exe_style_license">';
    echo '<option value="">' . __('Please select') . '</option>';
    // 'hide_empty=0' => show unused terms too
    foreach (get_terms('exe_style_license', 'hide_empty=0') as $term) {
    	$selected = ($term->term_id == $license->term_id)? ' selected="selected"' : '';
    	echo '<option value="' . $term->term_id . '"' . $selected . '>' . $term->name . '</option>';
    }
    echo '</select>';
    echo '</div>';
    
    // Tags as a group of checkboxes, named as exe_style_tags[$term_id] => will be collected in $_POST as an associative array
    echo '<div class="exe_style_tags">';
    echo '<fieldset><legend>' . __('Tags') . '</legend> ';
    // 'hide_empty=0' => show unused terms too
    foreach (get_terms('exe_style_tag', 'hide_empty=0') as $term) {
    	$checked = (in_array($term->term_id, $tags_ids))? ' checked="checked"' : '';
    	echo '<label><input type="checkbox" name="exe_style_tags[' . $term->term_id . ']"' . $checked . '/>' . $term->name . '</label>';
    }
    echo '</fieldset>';
    echo '</div>';

    // Tags as a group of checkboxes, named as exe_style_colours[$term_id] => will be collected in $_POST as an associative array
    echo '<div class="exe_style_colours">';
    echo '<fieldset><legend>' . __('Colours') . '</legend> ';
    // 'hide_empty=0' => show unused terms too
    foreach (get_terms('exe_style_colour', 'hide_empty=0') as $term) {
    	$checked = (in_array($term->term_id, $colours_ids))? ' checked="checked"' : '';
    	echo '<label><input type="checkbox" name="exe_style_colours[' . $term->term_id . ']"' . $checked . '/>' . $term->name . '</label>';
    }
    echo '</fieldset>';
    echo '</div>';
    
    // Show link to current ZIP file and an upload file input to choose new ZIP file
    echo '<div class="exe_style_download_zip">';
    $download_zip_stored_meta = (int) get_post_meta($post->ID, '_exe_style_download_zip_meta_key', TRUE);
    $download = get_post($download_zip_stored_meta);
    if (!empty($download)) {
      echo '<div class="exe-style exe-style-download-zip">';
    	echo '<span class="label">' . __('Download ZIP file') . ': </span>';
    	echo '<span class="value"><a href="' . $download->guid . '" class="zip">' . $download->post_title . '</a></span>';
    	echo '</div>';
    }
    
    echo '<label for="exe_style_download_zip">' . __('Upload ZIP file') . '</label> ';
    echo '<input type="file" name="exe_style_download_zip" id="exe_style_download_zip" />';
    echo '</div>';
		
  }

  /**
   * Save the meta when the post is saved.
   *
   * @param int $post_id The ID of the post being saved.
   * 
   * @see http://codex.wordpress.org/Function_Reference/add_meta_box#Class
   */
  public static function save_meta_box_data($post_id) {
    /*
     * We need to verify this came from our screen and with proper authorization,
     * because save_post can be triggered at other times.
     */
    /**
     * @todo Add error handling as suggested in http://wordpress.stackexchange.com/questions/715/objective-best-practices-for-plugin-development/16246#16246
     * @todo Author, license and compatibility are required, check they have non empty value
     */
    // Check if our nonce is set and that it is valid
    if ($_POST['post_type'] != 'exe_style') return $post_id;
    if (!isset( $_POST['exe_style_nonce'])) {
      self::add_error('exe_style_save_meta', 'Form expired or not valid. ');
      return $post_id;
    };
    $nonce = $_POST['exe_style_nonce'];
    if (!wp_verify_nonce($nonce, basename(__FILE__))) {
      self::add_error('exe_style_save_meta', 'Form expired or not valid. ');
      return $post_id;
    }
    
    // If this is an autosave, do nothing
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
    
    // Check user's permissions.
    if (!current_user_can('edit_exe_style', $post_id)) {
      self::add_error('exe_style_save_meta', 'Insuficient permissions. ');
      return $post_id;
    };
    
    // Collect, sanitize and validate user input
    $data_error = FALSE;
    $version_posted_data = sanitize_text_field($_POST['exe_style_version']);
    if (is_null($version_posted_data) || $version_posted_data === '' || $version_posted_data === FALSE) {
      self::add_error('exe_style_save_meta', 'Style version is required. ');
      $data_error = TRUE;
    }
    else {
      $version_valid_format = (boolean)preg_match('/^\d+(\.\d+)*$/', $version_posted_data);
      if (!$version_valid_format) {
        self::add_error('exe_style_save_meta', 'Style version is not valid: must be numeric digits separated by dots. ');
        $data_error = TRUE;
      }
    }
    
    $demo_url_posted_data = sanitize_text_field($_POST['exe_style_demo_url']);
    $demo_url_posted_data = esc_url_raw($demo_url_posted_data, array('http', 'https'));
    if (!empty($_POST['exe_style_demo_url']) && empty($demo_url_posted_data)) {
      self::add_error('exe_style_save_meta', 'Demo URL is not valid. ');
      $data_error = TRUE;
    }
    
    $author_posted_data = sanitize_text_field($_POST['exe_style_author']);
    if (empty($author_posted_data)) {
      self::add_error('exe_style_save_meta', 'Author name is required. ');
      $data_error = TRUE;
    }
    
    $author_url_posted_data = sanitize_text_field($_POST['exe_style_author_url']);
    $author_url_posted_data = esc_url_raw($author_url_posted_data, array('http', 'https'));
    if (!empty($_POST['exe_style_author_url']) && empty($author_url_posted_data)) {
      self::add_error('exe_style_save_meta', 'Author URL is not valid. ');
      $data_error = TRUE;
    }
    
    $license_posted_data = sanitize_term_field('term_id', $_POST['exe_style_license'], (int)$_POST['exe_style_license'], 'exe_style_license', 'db');
    if (empty($license_posted_data)) {
      self::add_error('exe_style_save_meta', 'License is required. ');
      $data_error = TRUE;
    }
    
    $compatibility_posted_data = sanitize_term_field('term_id', $_POST['exe_style_compatibility'], (int)$_POST['exe_style_compatibility'], 'exe_style_compatibility', 'db');
    if (empty($compatibility_posted_data)) {
      self::add_error('exe_style_save_meta', 'Target version is required. ');
      $data_error = TRUE;
    }
    
    // Make sure user uploaded a ZIP file
    $post_zip_id = (int) get_post_meta($_POST['ID'], '_exe_style_download_zip_meta_key', TRUE);
    if(!empty($_FILES['exe_style_download_zip']['name'])) {
      // Get the file type of the upload
      $arr_file_type = wp_check_filetype(basename($_FILES['exe_style_download_zip']['name']));
      $uploaded_type = $arr_file_type['type'];
       
      // Check if the type is supported. If not, throw an error.
      $supported_types = array('application/zip');
      if(!in_array($uploaded_type, $supported_types)) {
        self::add_error('exe_style_save_meta', 'eXe Style file must be a ZIP. ');
        $data_error = TRUE;
      }
    }
    else if (empty($post_zip_id)) {
      // Unless the eXe Style already has its ZIP file, this field is required
      self::add_error('exe_style_save_meta', 'ZIP file is required. ');
      $data_error = TRUE;
    }
    
    $tags_posted_data = array();
    if (!empty($_POST['exe_style_tags'])) {
      foreach ($_POST['exe_style_tags'] as $tag_id => $value) {
        if (!empty($value)) {
          $tags_posted_data[] = sanitize_term_field('term_id', $tag_id, (int)$tag_id, 'exe_style_tag', 'db');
        }
      }
    }
    
    $colours_posted_data = array();
    if (!empty($_POST['exe_style_colours'])) {
      foreach ($_POST['exe_style_colours'] as $tag_id => $value) {
        if (!empty($value)) {
          $colours_posted_data[] = sanitize_term_field('term_id', $tag_id, (int)$tag_id, 'exe_style_colour', 'db');
        }
      }
    }
    
    if ($data_error) {
      return $post_id;
    }
    
    // Update meta fields
    update_post_meta($post_id, '_exe_style_version_meta_key', $version_posted_data);
    update_post_meta($post_id, '_exe_style_demo_url_meta_key', $demo_url_posted_data);
    update_post_meta($post_id, '_exe_style_author_meta_key', $author_posted_data);
    update_post_meta($post_id, '_exe_style_author_url_meta_key', $author_url_posted_data);
    
    // Update taxonomy terms
    wp_set_object_terms($post_id, $license_posted_data, 'exe_style_license' );
    wp_set_object_terms($post_id, $compatibility_posted_data, 'exe_style_compatibility' );
    wp_set_object_terms($post_id, $tags_posted_data, 'exe_style_tag' );
    wp_set_object_terms($post_id, $colours_posted_data, 'exe_style_colour' );

    // Update de Post ID of the uploaded ZIP attachment
    // @see http://stackoverflow.com/questions/8931870/how-do-i-upload-a-file-to-wordpress-using-a-custom-upload-form
    if (!empty($_FILES['exe_style_download_zip']['name'])) {
      require_once(ABSPATH . "wp-admin" . '/includes/image.php');
      require_once(ABSPATH . "wp-admin" . '/includes/file.php');
      require_once(ABSPATH . "wp-admin" . '/includes/media.php');
	
      $attachment_id = (int) media_handle_upload('exe_style_download_zip', $post_id);
      /**
       * For some unknown reason, after being updated with the attachment ID,
       * this meta key is overwritten with a value of 1, this workaround avoids that
       */
      if (!empty($attachment_id) && $attachment_id > 1) {
        update_post_meta($post_id, '_exe_style_download_zip_meta_key', $attachment_id);
      }
    }
  }
  
  /**
   * Add custom methods to WP XML-RPC server
   * 
   * @param Array  $methods List of methods available in WP server before calling this filter
   *  
   * @return Array, list of methods in WP server with eXe Sytles Repository custom methods added
   */
  public static function xmlrpc_methods($methods) {
    $methods['exe_styles.listStyles'] = array('eXeStylesPlugin', 'xmlrpc_listStyles');
    return $methods;
  }

  /**
   * Gets and returns the full list of published eXe Styles in the repository
   *
   * @param Array $filter Ignored
   *
   * @see https://forja.cenatic.es/plugins/mediawiki/wiki/iteexe/index.php/ThemesRepository#xmlrpc-listStyles
   *
   * @return Array of objects, complying with the XML-RPC API
   */
  public static function xmlrpc_listStyles($filter = array()) {
  	// Get posts that match the filters
  	$style_posts = self::get_styles($filter);
  
  	// Load data from those posts into objects ready to be sent by XML-RPC
  	$style_list = array();
  	foreach ($style_posts as $style_post) {
  		$style = new eXeStyle($style_post);
  		$style_list[] = $style;
  	}
  
  	return $style_list;
  }
  
  /**
   * Gets the full list of published eXe Styles Posts in the site
   *
   * @param Array $filter Ignored
   *
   * @return Array of post objects
   */
  protected static function get_styles($filter = array()) {
  	$posts = array();
  
  	// Get published eXeStyle posts
  	$args = array();
  	$args['post_type'] = 'exe_style';
  	$args['post_status'] = 'publish';
  	$args['nopaging'] = TRUE;
  	$args['orderby'] = 'title';
  	$args['order'] = 'asc';
  	$query = new WP_Query($args);
  
  	while ($query->have_posts()) {
  		$posts[] = $query->next_post();
  	}
  
  	return $posts;
  }
}

/**
 * eXeStyle custom post type
 *
 * @author mclois
 */
class eXeStyle {
  /* Class attributes must be public, otherwise XML-RPC server would not be able to include this data in the response,
   * even if __get() and __isset() magic methods are defined.
   */
  public $id;
  public $name;
  public $demo_url;
  public $version;
  public $compatibility;
  public $author;
  public $author_url;
  public $license;
  public $license_url;
  public $title;
  public $description;
  public $download_url;
  public $link_url;
  public $tags = array();
  public $colours = array();
  
  /**
   * Loads eXeStyle attributes from its WP Post data
   * 
   * @see https://forja.cenatic.es/plugins/mediawiki/wiki/iteexe/index.php/ThemesRepository#XML-RPC_API_Data_dictionary
   * 
   * @param Object $post Wordpress post data
   */
  public function __construct($post) {
    // Load object attributes from Post data
    $this->id = $post->ID;
    $this->name = $post->post_name;
    $this->version = get_post_meta($post->ID, '_exe_style_version_meta_key', TRUE);
    $this->demo_url  = get_post_meta($post->ID, '_exe_style_demo_url_meta_key', TRUE);
    $this->author = get_post_meta($post->ID, '_exe_style_author_meta_key', TRUE);
    $this->author_url  = get_post_meta($post->ID, '_exe_style_author_url_meta_key', TRUE);
    $this->link_url = get_permalink($post->ID);
    
    // Array of localized strings, in this release just one language 
    $this->title = (object) array(
      'und' => $post->post_title,
      'en' => $post->post_title, 
    );

    // Load attributes from WP terms associated to post
    $license_term = wp_get_object_terms($post->ID, 'exe_style_license');
    $license_term = array_shift($license_term);
    $this->license = $license_term->name;
    $this->license_url = get_term_link($license_term);
    
    $compatibility_term = wp_get_object_terms($post->ID, 'exe_style_compatibility');
    $compatibility_term = array_shift($compatibility_term);
    $this->compatibility = $compatibility_term->name;
    
    // Download URL is the GUID of the attached ZIP file
    $download_zip_stored_meta = (int) get_post_meta($post->ID, '_exe_style_download_zip_meta_key', TRUE);
    $download = get_post($download_zip_stored_meta);
    $this->download_url = $download->guid;
    
    // Description is the post_content plus the main image thumbnail
    $post_thumbnail_id = get_post_thumbnail_id($post->ID);
    if ( $post_thumbnail_id ) {
    	do_action('begin_fetch_post_thumbnail_html', $post->ID, $post_thumbnail_id); 
    	$html = wp_get_attachment_image($post_thumbnail_id);
    	do_action('end_fetch_post_thumbnail_html', $post->ID, $post_thumbnail_id);
    } else {
    	$html = '';
    }
    $image_html = apply_filters( 'post_thumbnail_html', $html, $post->ID, $post_thumbnail_id);
    // Localized, one string per language.
    // HTML special chars will be replaced by its HTML entities, client must use 
    // html_entity_decode() on this field to get the original HTML content
    $description = '';
    if (!empty($image_html)) { 
      $description .= '<div class="exe-style-main-image">' . $image_html . '</div>';
    }
    if (!empty($post->post_content)) {
      $description .= '<div class="exe-style-post-content">' . $post->post_content . '</div>';
    }
    $this->description = (object) array(
      'und' => $description, // Default/untranslated language
      'en' => $description,
    );
    
    // Tags and Colours are complex data, managed in their own classes
    $tags_terms = wp_get_object_terms($post->ID, 'exe_style_tag');
    foreach ($tags_terms as $tag_term) {
      $this->tags[] = new eXeStyleTag($tag_term);
    }
    
    $colours_terms = wp_get_object_terms($post->ID, 'exe_style_colour');
    foreach ($colours_terms as $colour_term) {
      $this->colours[] = new eXeStyleColour($colour_term);
    }
  }
  
  /**
   * Define custom post type for eXe Styles and assign permissions to roles
   */
  public static function init_post_type() {
    register_post_type(
      'exe_style',
      array(
        'labels' => array(
          'name' => __( 'eXe Styles' ),
          'singular_name' => __( 'eXe Style' )
        ),
        'description' => __('Every post represents an eXe Style, uploaded by its author into the eXe Styles repository'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true, 
        'hierarchical' => false,
        'has_archive' => false,
        'capability_type' => 'exe_style',
        'supports' => array(
          'title',
          'editor',
          'comments',
          'revisions',
          'thumbnail',
          'custom-fields',
        ),
        'rewrite' => array('slug' => 'exe-style', 'with_front' => false,),
        'taxonomies' => array(
          'category',
          'exe_style_colour',
          'exe_style_tag',
          'exe_style_license',
          'exe_style_compatibility',
        ),
      )
    );
    
    register_taxonomy_for_object_type( 'category', 'exe_style' );

    // Allow authors and contributos to post new eXe Styles, and edit their own
    // allow editors and admins to publish, and edit all eXe Styles
    $author = get_role('administrator');

    // Add caps for Administrator role
    $role = get_role('administrator');
    $role->add_cap('edit_exe_style');
    $role->add_cap('edit_exe_styles');
    $role->add_cap('edit_others_exe_styles');
    $role->add_cap('publish_exe_styles');
    $role->add_cap('read_exe_style');
    $role->add_cap('read_private_exe_styles');
    $role->add_cap('delete_exe_style');

    // Add caps for Editor role
    $role = get_role('editor');
    $role->add_cap('edit_exe_style');
    $role->add_cap('edit_exe_styles');
    $role->add_cap('edit_others_exe_styles');
    $role->add_cap('publish_exe_styles');
    $role->add_cap('read_exe_style');
    $role->add_cap('read_private_exe_styles');
    $role->add_cap('delete_exe_style');

    // Add caps for Author role
    $role = get_role('author');
    $role->add_cap('edit_exe_style');
    $role->add_cap('edit_exe_styles');
    $role->add_cap('read_exe_style');

    // Add caps for Contributor role
    $role = get_role('contributor');
    $role->add_cap('edit_exe_style');
    $role->add_cap('edit_exe_styles');
    $role->add_cap('read_exe_style');

    // Add caps for Subscriber role
    $role = get_role('subscriber');
    $role->add_cap('edit_exe_style');
    $role->add_cap('edit_exe_styles');
    $role->add_cap('read_exe_style');
  }
  
  /**
   * Add posts of type eXe Style to taxonomy or tag search queries
   * 
   * @param WP_Query $query  Query to be executed
   *   
   * @return WP_Query, query to be executed
   */
  public static function query_post_type($query) {
  	if($query->is_category() || $query->is_tag()) {
  	  $post_types = $query->get('post_type');

  	  if ($post_types == 'any') {
  	    // Already searching for all kind of post types, do nothing
  	    return $query;
  	  }
  	  else {
  	    // Add 'exe_style' to the list of types to search on
  	    if (empty($post_types)) {
  	      // By default WP searchs only 'post' type
    	    $post_types = array(
  	        'post',
    	    );
  	    }
  	    else if (!is_array($post_types)) {
  	      $post_types = array($post_types);
  	    }
  	    
  	    if (!in_array('exe_style', $post_types)) {
  	      $post_types[] = 'exe_style';
  	    }

  	    $query->set('post_type', $post_types);
  	    return $query;
  	  }
  	}
  }
}

/**
 * eXeStyleColour custom taxonomy
 *
 * @author mclois
 */
class eXeStyleColour {
  /* Class attributes must be public, otherwise XML-RPC server would not be able to include this data in the response,
   * even if __get() and __isset() magic methods are defined.
   */
  public $id;
  public $title;
  public $url;
  
  /**
   * Load eXeStyleColour attributes from WP taxonomy term
   * 
   * @param Object $term WP taxonomy term
   */
  public function __construct($term) {
    $this->id = $term->term_id;

    // Array of localized strings, in this release just one language
    $this->title = (object) array(
      'und' => $term->name, // Default/untranslated content
      'en' => $term->name, 
    );
    
    $this->url = get_term_link($term);
  }
  
  /**
   * Define custom taxonomy for eXe style's colours
   */
  public static function init_taxonomy() {
    register_taxonomy(
      'exe_style_colour',
      'exe_style',
      array(
        'labels' => array(
          'name' => __( 'eXe Styles Colours' ),
          'singular_name' => __( 'eXe Style Colour' )
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'hierarchical' => false,
        'show_admin_column' => true,
      )
    );
  }
  
  /**
   * Create some colours in 'exe_style_colour' taxonomy
   */
  public static function register_colours() {
    wp_insert_term(
      'Red', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'red',
      )
    );

    wp_insert_term(
      'Orange', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'orange',
      )
    );

    wp_insert_term(
      'Yellow', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'yellow',
      )
    );

    wp_insert_term(
      'Green', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'green',
      )
    );

    wp_insert_term(
      'Blue', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'blue',
      )
    );

    wp_insert_term(
      'Violet', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'violet',
      )
    );

    wp_insert_term(
      'Black', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'black',
      )
    );

    wp_insert_term(
      'Grey', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'grey',
      )
    );

    wp_insert_term(
      'White', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'white',
      )
    );

    wp_insert_term(
      'Brown', // the term
      'exe_style_colour', // the taxonomy
      array(
        'slug' => 'brown',
      )
    );
  }
}

/**
 * eXeStyleTag custom taxonomy
 *
 * @author mclois
 */
class eXeStyleTag {
  /* Class attributes must be public, otherwise XML-RPC server would not be able to include this data in the response,
   * even if __get() and __isset() magic methods are defined.
   */
  public $id;
  public $title;
  public $url;
  
  /**
   * Load eXeStyleTag attributes from WP taxonomy term
   * 
   * @param Object $term WP taxonomy term
   */
  public function __construct($term) {
    $this->id = $term->term_id;

    // Array of localized strings, in this release just one language
    $this->title = (object) array(
      'und' => $term->name, // Default/untranslated content
      'en' => $term->name, 
    );
    
    $this->url = get_term_link($term);
  }
  
  /**
   * Define custom taxonomy for eXe style's tags
   */
  public static function init_taxonomy() {
    register_taxonomy(
      'exe_style_tag',
      'exe_style',
      array(
        'labels' => array(
          'name' => __( 'eXe Styles Tags' ),
          'singular_name' => __( 'eXe Style Tag' )
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'hierarchical' => false,
        'show_admin_column' => true,
      )
    );
  }

  /**
   * Create some tags in 'exe_style_tag' taxonomy
   */
  public static function register_tags() {
    wp_insert_term(
      'Adaptive', // the term
      'exe_style_tag', // the taxonomy
      array(
        'slug' => 'adaptive',
      )
    );

    wp_insert_term(
      'Mobile', // the term
      'exe_style_tag', // the taxonomy
      array(
        'slug' => 'mobile',
      )
    );

    wp_insert_term(
      'Fluid width', // the term
      'exe_style_tag', // the taxonomy
      array(
        'slug' => 'fluid-width',
      )
    );

    wp_insert_term(
      'Fixed width', // the term
      'exe_style_tag', // the taxonomy
      array(
        'slug' => 'fixed-width',
      )
    );

    wp_insert_term(
      'High contrast', // the term
      'exe_style_tag', // the taxonomy
      array(
        'slug' => 'high-contrast',
      )
    );
  }
}

/**
 * eXeStyleLicense custom taxonomy
 *
 * @author mclois
 */
class eXeStyleLicense {
  
  /**
   * Load eXeStyelLicense attributes from WP taxonomy term
   * 
   * @param Object $term WP taxonomy term
   */
  public function __construct($term) {
    $this->id = $term->term_id;
    $this->title = $term->name;
    $this->url = get_term_link($term);
  }
  
  /**
   * Define custom taxonomy for eXe style's licenses
   */
  public static function init_taxonomy() {
    register_taxonomy(
      'exe_style_license',
      'exe_style',
      array(
        'labels' => array(
          'name' => __( 'eXe Styles Licenses' ),
          'singular_name' => __( 'eXe Style License' )
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'hierarchical' => false,
        'show_admin_column' => true,
      )
    );
  }

  /**
   * Create some licenses in 'exe_style_license' taxonomy
   */
  public static function register_licenses() {
    wp_insert_term(
      'CC-BY', // the term
      'exe_style_license', // the taxonomy
      array(
        'slug' => 'cc-by',
      )
    );
    
    wp_insert_term(
      'CC-BY-SA', // the term
      'exe_style_license', // the taxonomy
      array(
        'slug' => 'cc-by-sa',
      )
    );
    
    wp_insert_term(
      'GPL', // the term
      'exe_style_license', // the taxonomy
      array(
        'slug' => 'gnu-gpl',
      )
    );
    
    wp_insert_term(
      'GNU FDL', // the term
      'exe_style_license', // the taxonomy
      array(
        'slug' => 'gnu-fdl',
      )
    );
  }
}

/**
 * eXeStyleCompatibility custom taxonomy
 *
 * @author mclois
 */
class eXeStyleCompatibility {
  
  /**
   * Define custom taxonomy for eXe style's compatibilty
   */
  public static function init_taxonomy() {
    register_taxonomy(
      'exe_style_compatibility',
      'exe_style',
      array(
        'labels' => array(
          'name' => __( 'eXe Target versions' ),
          'singular_name' => __( 'eXe Target versions' )
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'hierarchical' => false,
        'show_admin_column' => true,
      )
    );
  }

  /**
   * Create some eXe versions in 'exe_style_compatibility' taxonomy
   */
  public static function register_versions() {
    wp_insert_term(
      '>= 2.0', // the term
      'exe_style_compatibility', // the taxonomy
      array(
        'slug' => 'v200',
      )
    );
  }
}

// Actions on activating the plugin
register_activation_hook( __FILE__, array('eXeStylesPlugin', 'install') );

// WordPress hooks: define custom post types and taxonomies
add_action('init', array('eXeStyle', 'init_post_type'));
add_filter('pre_get_posts', array('eXeStyle', 'query_post_type'));
add_action('init', array('eXeStyleColour', 'init_taxonomy'));
add_action('init', array('eXeStyleTag', 'init_taxonomy'));
add_action('init', array('eXeStyleLicense', 'init_taxonomy'));
add_action('init', array('eXeStyleCompatibility', 'init_taxonomy'));

// Add custom metabox to eXe Styles and saved custom meta data
add_action('add_meta_boxes', array('eXeStylesPlugin', 'add_meta_box'));
add_action('save_post', array('eXeStylesPlugin', 'save_meta_box_data'));
add_action('post_edit_form_tag', array('eXeStylesPlugin', 'update_edit_form'));

// Plugin error hanling
add_action('after_setup_theme', array('eXeStylesPlugin', 'init_errors'));
add_action('admin_notices', array('eXeStylesPlugin', 'dump_errors'));

// Custom XML-RPC methods
add_filter( 'xmlrpc_methods', array('eXeStylesPlugin', 'xmlrpc_methods'));