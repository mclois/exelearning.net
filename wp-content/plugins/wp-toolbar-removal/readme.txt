=== ToolBar Removal completely disable ===
Contributors: sLaNGjI
Donate link: http://lcsn.net/donate/
Tags: sLa, sLaNGjI, toolbar, admin, bar, removal, hide, disable, remove, frontend, backend, code
Stable tag: 2012.0319.2012
Requires at least: 3.3
Tested up to: 3.4
License: GPLv2
Completely Disable new WordPress 3.3+ and 3.4+ (only) ToolBar Frontend, Backend, related User Profile Settings, without loosing logout functionality! Thanks to olyma.
== Description ==
Completely Disable new WordPress 3.3+ and 3.4+ (only) ToolBar Frontend, Backend, related User Profile Settings, without loosing logout functionality! Completely <code>Remove Code</code> for minimal memory load, with new approach.

All in One Removal: ToolBar - Node - Frontend - Backend - Profile Personal Options - Code.

The first and only Plugin (for now) that really work on WordPress 3.4+ / 3.3+

ToolBar Removal is [Admin Bar Removal](http://wordpress.org/extend/plugins/wp-admin-bar-removal/) Enhancement based on [ToolBar Node Removal](http://wordpress.org/extend/plugins/wp-toolbar-node-removal/) and [One Click Logout Barless](http://wordpress.org/extend/plugins/one-click-logout-barless/)

Linked and reviewed at [softpedia.com](http://webscripts.softpedia.com/script/Modules/WordPress-Plugins/ToolBar-Removal-completely-disable-73548.html)

Thanks to [olyma](http://wordpress.org/extend/plugins/one-click-logout-barless/)

* Try also:
 * [WP Overview (lite)](http://wordpress.org/extend/plugins/wp-overview-lite/)
 * [WP Missed Schedule](http://wordpress.org/extend/plugins/wp-missed-schedule/)
 * [WP Admin Bar Removal](http://wordpress.org/extend/plugins/wp-admin-bar-removal/) Linked and reviewed at [softpedia.com](http://webscripts.softpedia.com/script/Modules/WordPress-Plugins/Admin-Bar-Removal-completely-disable-73547.html)
 * [WP Admin Bar Node Removal](http://wordpress.org/extend/plugins/wp-admin-bar-node-removal/)
 * [WP Toolbar Removal](http://wordpress.org/extend/plugins/wp-toolbar-removal/) Linked and reviewed at [softpedia.com](http://webscripts.softpedia.com/script/Modules/WordPress-Plugins/ToolBar-Removal-completely-disable-73548.html)
 * [WP Toolbar Node Removal](http://wordpress.org/extend/plugins/wp-toolbar-node-removal/)
 * [Noindex (login) WordPress Deindexing](http://wordpress.org/extend/plugins/wp-login-deindexing/)
 * [Noindex (total) WordPress Deindexing](http://wordpress.org/extend/plugins/wp-total-deindexing/)
 * [IE Enhancer and Modernizer](http://wordpress.org/extend/plugins/wp-ie-enhancer-and-modernizer/) Linked and reviewed at [softpedia.com](http://webscripts.softpedia.com/script/Modules/WordPress-Plugins/IE-Enhancer-and-Modernizer-73546.html)

Work under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html) License.

Author URL [sLa](http://wordpress.org/extend/plugins/profile/sla) is moved to [sLaNGjI](http://wordpress.org/extend/plugins/profile/slangji). Please update your bookmarks!
== Changelog ==
`All previous release, prior of latest stable, are on fact
deprecated, and no longer supported on this project:
is very suggested upgrade to the latest build always!
This plugin work under GPLv2 license.`
= Development Release =
[Version 2012.0325-BUGFIX.0144-DEVELOPMENTAL](http://downloads.wordpress.org/plugin/wp-toolbar-removal.zip)
= 2012.0319.2012 =
* Major Update [CERTIFIED] WP 3.3+ / 3.4+ [Admin Bar Removal](http://wordpress.org/extend/plugins/wp-admin-bar-removal/) Enhancement!
 * Please update as soon possible!
 * First 2012 Major Release Certified
 * Enhancement of [Admin Bar Removal](http://wordpress.org/extend/plugins/wp-admin-bar-removal/)
 * Derived from [ToolBar Node Removal](http://wordpress.org/extend/plugins/wp-toolbar-node-removal/)
 * Thanks to [olyma](http://wordpress.org/extend/plugins/one-click-logout-barless/)
 * All in One Removal (ToolBar Node Frontend Backend Profile Personal Options Code)
 * State of the Art Plugin?
 * NEW Logout functionality is not removed
 * NEW Faster Dasboard Speedup
 * NEW Maximun Free Memory
 * Full WordPress 3.3+ Support
 * Full WordPress 3.4+ Support
 * DEPRECATED all pre 3.3 WordPress Versions
 * Full Strict Security Rules Applied
 * Code Cleanup for faster loading
 * Only one unique php plugin file
 * Less Plugin Memory Consumption (1KB or no more)
 * Nothing is written into your space disk
 * Nothing is written into your wp_option table
 * Work with single and multisite installation of WordPress
 * Work with Shared, VPS and Dedicated Hosting
 * Work under GPLv2 license
 * Bump Version 2012 Build 0319 Revision 2012
= 2012.0314.1152 =
* Release Candidate [STABLE] Full WordPress 3.3+ Compatible.
 * Please update as soon possible!
 * NEW Preemptive support WP 3.4-alpha
 * Work under GPLv2 license
 * Bump Version 2012 Build 0314 Revision 1152
== Upgrade Notice ==
= 2012.0319.2012 =
* Major Update [CERTIFIED] Full WP 3.3+ / 3.4+ Compatible. [Admin Bar Removal](http://wordpress.org/extend/plugins/wp-admin-bar-removal/) Enhancement!
= 2012.0314.1152 =
* Release Candidate [STABLE] Full WordPress 3.3+ Compatible. Preemptive support WP 3.4-alpha